��          �   %   �      P     Q     X     w     }     �     �     �     �     �     �     �  
   �     �     
               '     7     ?     H     ^     d     p     v     |  _  �     �  -   �            
   ,  "   7  #   Z  #   ~     �     �     �  
   �     �     �     �     �  
   �  	   �     �          %     ,     8     =     B        
                                               	                                                                      Center Click on the eye to turn it on Color Default Enable Enable left click coloring Enable middle click coloring Enable right click coloring Eye Eyelid Left Left click Line Thickness Location Margin Middle click Mouse indicator Opacity Position Refresh interval (ms) Right Right click Round Shape Size Project-Id-Version: EyeExtendedExtension
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2022-11-28 10:26-0500
Last-Translator: Philippe-André Akue <pakueamb@gmail.com>
Language-Team: 
Language: de
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
X-Generator: Poedit 3.1.1
 Zentrum Klicken Sie auf das Auge, um es einzuschalten Farbe Voreingestellt Aktivieren Färbung bei Linksklick aktivieren Färbung bei Mittelklick aktivieren Färbung bei Rechtsklick aktivieren Auge Augenlid Links Linksklick Dicke der Linie Stelle Rand Mittelklick Mauszeiger Deckkraft Position Aktualisierungsintervall (ms) Rechts Rechtsklick Rund Form Größe 