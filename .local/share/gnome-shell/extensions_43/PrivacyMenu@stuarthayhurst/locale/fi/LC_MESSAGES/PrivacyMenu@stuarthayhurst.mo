��          �      <      �     �     �  7   �  W   �     P     c     t  
   }     �     �     �     �     �     �            ;   )  ~  e     �     �  7   �  _   /     �     �     �  	   �     �     �  $   �     "     ?     Q  	   c     m  ?   �                                                                      
                       	        Camera Enabled Force the icon to move to right side of the status area Found this useful?
<a href="https://paypal.me/stuartahayhurst">Consider donating</a> :) GNOME 43+ settings General settings Location Microphone Move status icon right Privacy Settings Privacy Settings Menu Indicator Reset privacy settings Reset settings Reset to defaults Settings Use quick settings menu Use the system quick settings area, instead of an indicator Project-Id-Version: privacy-menu-extension
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2023-03-09 20:37+0200
Last-Translator: Samu Lumio <samu.lumio@outlook.com>
Language-Team: Finnish <samu.lumio@outlook.com>
Language: fi
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1)
X-Generator: Gtranslator 42.0
 Kamera Käytössä Pakota kuvake siirtymään tilapalkin oikealle puolelle Hyödyitkö tästä?
<a href="https://paypal.me/stuartahayhurst">Harkitse lahjoittamista</a> :) Asetukset GNOME 43+:lle Yleiset asetukset Sijainti Mikrofoni Siirrä tilakuvake oikealle Yksityisyysasetukset Yksityisyysvalintojen Pika-asetukset Palauta yksityisyysasetukset Palauta asetukset Palauta oletukset Asetukset Käytä pika-asetusvalikkoa Käytä järjestelmän pika-asetusvalikkoa tilakuvakkeen sijaan 