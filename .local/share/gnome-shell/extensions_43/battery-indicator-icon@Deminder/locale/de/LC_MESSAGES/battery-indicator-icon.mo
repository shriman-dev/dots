��          �      �           	     !  =   ;     y     ~     �  
   �     �  
   �     �  '   �  '   �               6     G     S  L   Y     �     �     �     �    �     �     �  G   �     :     C     I     P  
   \  
   g     r  .   �  :   �  	   �     �          #     /  S   5     �     �     �     �                                     
              	                                                     Battery percentage text Battery status icon style Battery status icon style (bold, slim, plain, circle, hidden) Bold Circle Default Extra wide Hidden Horizontal Horizontal scale Icon orientation (vertical, horizontal) Icon scale as aspect ratio width/height Inside the icon Inside the icon (vertical) Next to the icon Orientation Plain Show battery percentage text in icon (hidden: 0, horizontal: 1, vertical: 2) Slim Text Vertical Wide Project-Id-Version: 
Report-Msgid-Bugs-To: 
PO-Revision-Date: 
Last-Translator: Philipp Kiemle <philipp.kiemle@gmail.com>
Language-Team: 
Language: de
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Poedit 3.1.1
 Prozentangabe Art des Akkustand-Symbols Art des Akkustand-Symbols (kräftig, schlank, glatt, Kreis, unsichtbar) Kräftig Kreis Normal Extra breit Unsichtbar Horizontal Horizontale Skalierung Ausrichtung des Symbols (vertikal, horizontal) Symbolgröße als Seitenverhältnis des Icons Breite/Höhe Im Symbol Im Symbol (vertikal) Neben dem Symbol Ausrichtung Glatt Akkustand in Prozent im Symbol anzeigen (unsichtbar: 0, horizontal: 1, vertikal: 2) Schlank Text Vertikal Breit 