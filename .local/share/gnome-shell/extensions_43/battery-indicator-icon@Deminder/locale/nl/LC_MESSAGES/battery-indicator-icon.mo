��          �      �           	     !  =   ;     y     ~     �  
   �     �  
   �     �  '   �  '   �               6     G     S  L   Y     �     �     �     �  h  �     '     ;  <   T     �     �  	   �     �  	   �     �     �  -   �  -        9     J     g     {     �  U   �     �     �  	   �     �                                     
              	                                                     Battery percentage text Battery status icon style Battery status icon style (bold, slim, plain, circle, hidden) Bold Circle Default Extra wide Hidden Horizontal Horizontal scale Icon orientation (vertical, horizontal) Icon scale as aspect ratio width/height Inside the icon Inside the icon (vertical) Next to the icon Orientation Plain Show battery percentage text in icon (hidden: 0, horizontal: 1, vertical: 2) Slim Text Vertical Wide Project-Id-Version: battery-indicator-icon v1
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2023-02-04 20:29+0100
Last-Translator: Heimen Stoffels <vistausss@fastmail.com>
Language-Team: Dutch
Language: nl
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
X-Generator: Poedit 3.2.2
 Accupercentagetekst Accustatuspictogramstijl Accustatuspictogramstijl (bold, slim, plain, circle, hidden) Dik Cirkel Standaard Extra breed Verborgen Horizontaal Horizontale weergave Pictogramoriëntatie (verticaal, horizontaal) Pictogramgrootte op basis van beeldverhouding Op het pictogram Op het pictogram (verticaal) Naast het pictogram Oriëntatie Kaal Accupercentagetekst op pictogram tonen (verborgen: 0 - horizontaal: 1 - verticaal: 2) Dun Tekst Verticaal Breed 