��          �      �           	     !  =   ;     y     ~     �  
   �     �  
   �     �  '   �  '   �               6     G     S  L   Y     �     �     �     �  >  �     �  (     [   A  	   �  	   �     �     �     �     �     �  1   �  0   "     S  "   j     �     �     �  X   �            	        %                                     
              	                                                     Battery percentage text Battery status icon style Battery status icon style (bold, slim, plain, circle, hidden) Bold Circle Default Extra wide Hidden Horizontal Horizontal scale Icon orientation (vertical, horizontal) Icon scale as aspect ratio width/height Inside the icon Inside the icon (vertical) Next to the icon Orientation Plain Show battery percentage text in icon (hidden: 0, horizontal: 1, vertical: 2) Slim Text Vertical Wide Project-Id-Version: battery-indicator-icon v2
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2023-02-04 14:13+0100
Last-Translator: Albano Battistella <albano_battistella@hotmail.com>
Language-Team: Italian <LL@li.org>
Language: it
MIME-Version: 1.0
Content-Type: text/plain; charset=CHARSET
Content-Transfer-Encoding: 8bit
 Testo percentuale batteria Stile dell'icona di stato della batteria Stile dell'icona di stato della batteria (grassetto, sottile, normale, circolare, nascosta) Grassetto Cercolare Predefinito Extra larga Nascosta Orrizontale Scala orizzontale Orientamento delle icone (verticale, orizzontale) Scala dell'icona come rapporto larghezza/altezza All'interno dell'icona All'interno dell'icona (verticale) Accanto all'icona Orientamento Normale Mostra testo percentuale batteria nell'icona (nascosto: 0, orizzontale: 1, verticale: 2) Sottile Testo Verticale Larga 