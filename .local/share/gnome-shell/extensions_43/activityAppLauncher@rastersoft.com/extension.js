const GLib = imports.gi.GLib;
const Gio = imports.gi.Gio;
const St = imports.gi.St;
const GMenu = imports.gi.GMenu;
const GObject = imports.gi.GObject;
const Clutter = imports.gi.Clutter;
const Shell = imports.gi.Shell;
const Gtk = imports.gi.Gtk;
const Pango = imports.gi.Pango;

const Main = imports.ui.main;
const OverviewControls = imports.ui.overviewControls;
const WorkspacesView = imports.ui.workspacesView;
const Favorites = imports.ui.appFavorites;
const AppDisplay = imports.ui.appDisplay;
const ExtensionUtils = imports.misc.extensionUtils;

const Lang = imports.lang;
const Gettext = imports.gettext;
const Mainloop = imports.mainloop;

const Tweener = imports.tweener.tweener;

const SCHEMA = 'org.gnome.shell.extensions.activityAppLauncher';

var _ = Gettext.domain("activityAppLauncher").gettext;
Gettext.bindtextdomain("activityAppLauncher", ExtensionUtils.getCurrentExtension().path + "/locale");

// Version 11

var init = function() {
    return new ActivityAppLauncher();
}

class ActivityAppLauncher {

    constructor() {
        this.isEnabled = false;
        this._startupPreparedId = 0;
        this._applicationsButtons = null;
        this._appsInnerContainer = null;
        this.selected = null;
        this._iconsScroll = null;
        this.visibility = "atexttoensurethateverythingworks";
        this._stageWidth = 0;
        this._stageHeight = 0;

        this._LABELSHOWTIME	= 15/100;
        this._LABELHIDETIME = 10/100;
        this._SLIDETIME = 15/100;
        this._HOVERDELAY = 300;
        this._TITLE = true;
        this._APPDESCRIPTION = true;
        this._BORDERS = true;
    }

    _onChangedSetting() {
        this.icon_size = this._settings.get_int("icon-size");
        this.show_favorites = this._settings.get_boolean("show-favorites");
        this.show_frequent = this._settings.get_boolean("show-frequent");
        this.icon_width = Math.floor(this.icon_size * 2);
        this.icon_height = Math.floor(this.icon_size * 1.8);
        this._TITLE = this._settings.get_boolean("show-tooltips");
        this._APPDESCRIPTION = this._settings.get_boolean("show-tooltips");
        this._BORDERS = this._settings.get_boolean("show-tooltip-border");
    }

    enable() {
        // Wait until startup completed
        if (Main.layoutManager._startingUp) {
            this._startupPreparedId = Main.layoutManager.connect('startup-complete', () => {
                this._doEnable(true);
            });
        } else {
            this._doEnable(false);
        }
    }

    _doEnable(removeStartup) {
        if (removeStartup) {
            Main.layoutManager.disconnect(this._startupPreparedId);
            this._startupPreparedId = 0;
        }
        this._appSys = Shell.AppSystem.get_default();
        this._settings = ExtensionUtils.getSettings(SCHEMA);
        this._controls = Main.overview._overview._controls;
        this._settingsId = this._settings.connect('changed', () => {this._onChangedSetting();});
        this._onChangedSetting();
        this.isEnabled = true;
        this._fillElements(true);
        this._favorites = Favorites.getAppFavorites();
        this._usage = Shell.AppUsage.get_default();
        this.showingId = Main.overview.connect('showing', () => {this._show();});
        this.hidingId = Main.overview.connect('hiding', () => {this._hide();});
    }

    disable() {
        if (this.isEnabled) {
            this._fillElements(false);
            Main.overview.disconnect(this.showingId);
            Main.overview.disconnect(this.hidingId);
            this._settings.disconnect(this._settingsId);
        }
        this.isEnabled = false;
    }

    _hide() {
        this._appsLaunchContainer.hide();
        this.selected = null;
        this._setVisibility();
    }

    _show() {
        this.visibility = "atexttoensurethateverythingworks";
        this.selected = null;
        this._setVisibility();
        this._appsLaunchContainer.hide();
        this._fillCategories();
    }


    _allocateWorkspaces() {
        if ((this._stageWidth != 0) && (this._stageHeight != 0) && (this._appsWidth != 0)) {
            const childBox = new Clutter.ActorBox();
            childBox.set_origin(this._appsWidth,0);
            childBox.set_size(this._stageWidth - this._appsWidth, this._stageHeight);
            this._iconsContainer.allocate(childBox);
            this._workspacesDisplay.allocate(childBox);
        }
    }

    _allocationChangedActivities() {
        let [stageX, stageY] = this._appsContainer.get_transformed_position();
        let [stageWidth, stageHeight] = this._appsContainer.get_transformed_size();
        this._stageX = stageX;
        this._stageY = stageY;
        this._stageWidth = stageWidth;
        this._stageHeight = stageHeight;
    }

    _allocationChangedApplicationList() {
        let [X, Y] = this._appsInnerContainer.get_transformed_position();
        let [Width, Height] = this._appsInnerContainer.get_transformed_size();
        this._appsX = X;
        this._appsY = Y;
        this._appsWidth = Width;
        this._appsHeight = Height;
        const childBox = new Clutter.ActorBox();
        childBox.set_origin(0,0);
        childBox.set_size(Width, Height);
        this._appsInnerContainer.allocate(childBox);
    }

    // Modifies the Activities view to add our stage with the application categories
    _fillElements(desired_mode) {

        if (desired_mode) {
            this._appsContainer = new St.BoxLayout({x_expand: true, y_expand: true});

            this._controls.remove_child(this._controls._workspacesDisplay);

            this._workspacesDisplay = this._controls._workspacesDisplay;
            this._controls.layout_manager._workspacesDisplay = this._appsContainer;
            this._appsContainer.add_child(this._workspacesDisplay);

            this._appsInnerContainer = new St.BoxLayout({ vertical: true,
                                                         x_align: Clutter.ActorAlign.FILL,
                                                         y_align: Clutter.ActorAlign.START,
                                                         x_expand: false,
                                                         y_expand: false });
            this._appsContainer.add_child(this._appsInnerContainer);
            this._appsLaunchContainer = new St.Bin({});
            this._alloc = this._appsContainer.connect_after("notify::allocation", () => {
                this._allocationChangedActivities();
            });
            this._alloc2 = this._appsInnerContainer.connect_after("notify::allocation", () => {
                this._allocationChangedApplicationList();
            });
            this._iconsScroll = new St.ScrollView({hscrollbar_policy: Gtk.PolicyType.NEVER});
            this._iconsScroll.x_expand = true;
            this._iconsScroll.y_expand = true;
            this._appsLaunchContainer.add_actor(this._iconsScroll);
            this._iconsContainer = new St.BoxLayout({ vertical: true, x_expand: true});
            this._iconsScroll.add_actor(this._iconsContainer);

            this._controls.add_child(this._appsContainer);

            this._appsContainer.add_child(this._appsLaunchContainer);
            this._appsContainer.show();
            this._show();
            this._search = this._controls._searchController;
            this._searchID = this._search.connect("notify::search-active", () => {
                if (!this._search.search_active) {
                    this.selected = null;
                }
                this._setVisibility();
            })
        } else {
            this._appsContainer.remove_child(this._workspacesDisplay);
            this._controls.add_child(this._workspacesDisplay);
            this._controls.layout_manager._workspacesDisplay = this._workspacesDisplay;
            this._workspacesDisplay.ease({
                opacity: 255,
                duration: 100,
                mode: Clutter.AnimationMode.EASE_OUT_QUAD,
                onComplete: () => {
                    this._workspacesDisplay.reactive = true;
                    this._workspacesDisplay.setPrimaryWorkspaceVisible(true);
                },
            });
            this._controls.remove_child(this._appsContainer);
            this._appsContainer.disconnect(this._alloc);
            this._appsInnerContainer.disconnect(this._alloc2);
            this._search.disconnect(this._searchID);
            this._alloc = 0;
            this._alloc2 = 0;
            this._searchID = 0;
            this._appsContainer = null;
            this._appsLaunchContainer = null;
            this._appsInnerContainer = null;
            this._search = null;
        }
    }

    _fillCategories() {
        this.selected = null;
        this._appsInnerContainer.destroy_all_children();

        this._appsInnerContainer.buttons = [];
        this._appsInnerContainer.appList=[];
        this._appsInnerContainer.appClass=[];

        let tree = new GMenu.Tree({ menu_basename: 'applications.menu' });
        tree.load_sync();
        let root = tree.get_root_directory();
        let categoryMenuItem = new AALCathegory_Menu_Item(this,1,_("Windows"), null);
        this._appsInnerContainer.add_child(categoryMenuItem);
        this._appsInnerContainer.buttons.push(categoryMenuItem);

        if (this.show_favorites) {
            let favoritesMenuItem = new AALCathegory_Menu_Item(this,2,_("Favorites"), null);
            this._appsInnerContainer.add_child(favoritesMenuItem);
            this._appsInnerContainer.buttons.push(favoritesMenuItem);
        }

        if (this.show_frequent) {
            let mostUsedMenuItem = new AALCathegory_Menu_Item(this,3,_("Frequent"), null);
            this._appsInnerContainer.add_child(mostUsedMenuItem);
            this._appsInnerContainer.buttons.push(mostUsedMenuItem);
        }

        let iter = root.iter();
        let nextType;
        while ((nextType = iter.next()) != GMenu.TreeItemType.INVALID) {
            if (nextType == GMenu.TreeItemType.DIRECTORY) {
                let dir = iter.get_directory();
                if (!dir.get_is_nodisplay()) {
                    let childrens = this._fillCategories2(dir,[]);
                    if (childrens.length != 0) {
                        childrens.sort(this._sortApps);
                        let item = { dirItem: dir, dirChilds: childrens };
                        this._appsInnerContainer.appClass.push(item);
                    }
                }
            }
        }
        this._appsInnerContainer.appList.sort(this._sortApps);
        for (var i = 0; i < this._appsInnerContainer.appClass.length; i++) {
            let categoryMenuItem = new AALCathegory_Menu_Item(this,0,this._appsInnerContainer.appClass[i].dirItem.get_name(), this._appsInnerContainer.appClass[i].dirChilds);
            this._appsInnerContainer.add_child(categoryMenuItem);
            this._appsInnerContainer.buttons.push(categoryMenuItem);
        }
    }

    _sortApps(param1, param2) {
        if (!param1) {
            return 1;
        }
        if (!param2) {
            return -1;
        }
        if (param1.get_name().toUpperCase()<param2.get_name().toUpperCase()) {
            return -1;
        } else {
            return 1;
        }
    }

    _fillCategories2(dir, childrens) {
        let iter = dir.iter();
        let nextType;

        while ((nextType = iter.next()) != GMenu.TreeItemType.INVALID) {
            if (nextType == GMenu.TreeItemType.ENTRY) {
                let entry = iter.get_entry();
                if (!entry.get_app_info().get_nodisplay()) {
                    let app = this._appSys.lookup_app(entry.get_desktop_file_id());
                    childrens.push(app);
                    this._appsInnerContainer.appList.push(app);
                }
            } else if (nextType == GMenu.TreeItemType.DIRECTORY) {
                childrens = this._fillCategories2(iter.get_directory(), childrens);
            }
        }
        return childrens;
    }

    _refreshIcons(appButton) {

        let sizex = this._stageWidth - this._appsWidth;

        let iconx = Math.floor(sizex / this.icon_width);
        let element_width = Math.floor(sizex / iconx);

        this._iconsContainer.destroy_all_children();
        var position = 0;
        var currentContainer = null;
        var launcherList = null;

        switch (appButton.launcherType) {
            case 0:
                launcherList = appButton.launchers;
                break;
            case 2:
                launcherList = this._favorites.getFavorites();
                break;
            case 3:
                launcherList = this._usage.get_most_used();
                break;
            default:
                launcherList = null;
                break;
        }
        if (launcherList !== null) {
            for(let i = 0;i < launcherList.length; i++) {
                let element = launcherList[i];
                if (position == 0) {
                    currentContainer = new St.BoxLayout({vertical: false, x_expand: true});
                    currentContainer.x_expand = true;
                    currentContainer.y_expand = false;
                    this._iconsContainer.add_child(currentContainer);
                }
                let tmpContainer = new St.BoxLayout({width: element_width, height: this.icon_height});
                tmpContainer.set_x_align(Clutter.ActorAlign.CENTER);
                tmpContainer.set_y_align(Clutter.ActorAlign.START);
                tmpContainer.set_x_expand(false);
                tmpContainer.set_y_expand(false);

                let button = new AppDisplay.AppIcon(element);
                button.set_x_align(Clutter.ActorAlign.CENTER);
                button.set_y_align(Clutter.ActorAlign.START);
                button.set_x_expand(true);
                button.set_y_expand(false);
                tmpContainer.add_child(button);
                currentContainer.add_child(tmpContainer);
                // contains tooltip data
                button.ttdata = {};
                button.ttdata._labelTimeoutId = 0;
                button.ttdata._alreadyClicked = false;
                button.connect('enter-event', (actor) => {
                    this._onHover(actor);
                });
                button.connect('leave-event', (actor) => {
                    this._onLeave(actor);
                });
                button.connect('destroy', (actor) => {
                    this._onLeave(actor);
                });
                button.connect('clicked', (actor) => {
                    actor.ttdata._alreadyClicked = true;
                    this._onLeave(actor);
                });

                position++;
                if (position == iconx) {
                    position = 0;
                }
            }
        }
        this._appsLaunchContainer.show();
    }

    _clickedCathegory(button) {
        for(var i = 0; i < this._appsInnerContainer.buttons.length; i++) {
            var tmpbutton = this._appsInnerContainer.buttons[i];
            if (button == tmpbutton) {
                tmpbutton.checked = true;
            } else {
                tmpbutton.checked = false;
            }
        }
        if (button.launcherType == 1) {
            this.selected = null; // for Windows list, the selected button must be null
        } else {
            this.selected = button.cat;
        }
        this._setVisibility();
        if (this.selected !== null) {
            this._refreshIcons(button);
        }
    }

    _setVisibilityWorkspaces(visible) {
        if (((this.visibility === null) && (this.selected !== null)) ||
            ((this.visibility !== null) && (this.selected === null))) {
                this.visibility = this.selected;
                if (visible) {
                    this._workspacesDisplay.reactive = true;
                    this._workspacesDisplay.setPrimaryWorkspaceVisible(true);
                }
                this._workspacesDisplay.ease({
                    opacity: visible ? 255 : 0,
                    duration: 100,
                    mode: Clutter.AnimationMode.EASE_OUT_QUAD,
                    onComplete: () => {
                        this._workspacesDisplay.reactive = visible;
                        this._workspacesDisplay.setPrimaryWorkspaceVisible(visible);
                    },
                });
        }
    }

    _setVisibilityLauncher(visible) {
        if (visible && !this._search.search_active) {
            this._appsLaunchContainer.show();
        } else {
            this._appsLaunchContainer.hide();
        }
    }

    _setVisibility() {
        if (this.selected === null) {
            this._setVisibilityWorkspaces(true);
            this._setVisibilityLauncher(false);
        } else {
            this._setVisibilityWorkspaces(false);
            this._setVisibilityLauncher(true);
        }
    }

    _onHover(actor){

        // checks if cursor is over the icon
        // it is : let's setup a toolip display
        // unless it's already set
        if (actor.ttdata._labelTimeoutId == 0) {
            // if the tooltip is already displayed (on another icon)
            // we update it, else we delay it
            if (actor.ttdata._labelShowing) {
                this._showTooltip(actor);
            } else {
                actor.ttdata._labelTimeoutId = Mainloop.timeout_add(this._HOVERDELAY, () => {
                    this._showTooltip(actor);
                    actor.ttdata._labelTimeoutId = 0;
                    return false;
                } );
            }
        }
    }

    _onLeave(actor) {
        // unset label display timer if needed
        if (actor.ttdata._labelTimeoutId > 0){
            Mainloop.source_remove(actor.ttdata._labelTimeoutId);
            actor.ttdata._labelTimeoutId = 0;
        }
        if (actor.ttdata._labelShowing) {
            this._hideTooltip(actor);
            actor.ttdata._labelShowing = false;
        }
    }

    _hideTooltip(actor) {
        if (actor.ttdata._ttbox) {
            Tweener.addTween(actor.ttdata._ttbox, {
                opacity: 0,
                time: this._LABELHIDETIME,
                transition: 'easeOutQuad',
                onComplete: function() {
                    Main.uiGroup.remove_child(actor.ttdata._ttbox);
                    actor.ttdata._ttbox = null;
                }
            });
        }
    }

    _showTooltip(actor) {
        if (actor.ttdata._alreadyClicked) {
            this._onLeave(actor);
            return;
        }
        if (!this._TITLE && !this._APPDESCRIPTION) {
            this._onLeave(actor);
            return;
        }
        let icontext = '';
        let titletext = '';
        let detailtext = '';
        let should_display = false;

        //applications overview
        icontext = actor.app.get_name();

        if (this._APPDESCRIPTION) {
            let appDescription = actor.app.get_description();
            if (appDescription) {
                detailtext = appDescription;
                should_display = true;
            }
        }

        // Decide wether to show title
        if ( this._TITLE && icontext ) {
            titletext = icontext;
            should_display = true;
        }

        // If there's something to show ..
        if ( titletext && should_display ) {

            // Create a new tooltip if needed
            if (!actor.ttdata._ttbox) {
                let css_class = this._BORDERS ? 'activityAppLauncher-tooltip-borders' : 'activityAppLauncher-tooltip';
                actor.ttdata._ttbox = new St.Bin({ style_class: css_class });
                actor.ttdata._ttlayout = new St.BoxLayout({ vertical: true });
                actor.ttdata._ttlabel = new St.Label({ style_class: 'activityAppLauncher-tooltip-title', text: titletext });
                actor.ttdata._ttdetail = new St.Label({ style_class: 'activityAppLauncher-tooltip-detail', text: detailtext });
                actor.ttdata._ttlayout.add_child(actor.ttdata._ttlabel);
                actor.ttdata._ttlayout.add_child(actor.ttdata._ttdetail);
                actor.ttdata._ttbox.set_child(actor.ttdata._ttlayout);

                // we force text wrap on both labels
                actor.ttdata._ttlabel.clutter_text.line_wrap = true;
                actor.ttdata._ttlabel.clutter_text.line_wrap_mode = Pango.WrapMode.WORD;
                actor.ttdata._ttlabel.clutter_text.ellipsize = Pango.EllipsizeMode.NONE;
                actor.ttdata._ttdetail.clutter_text.line_wrap = true;
                actor.ttdata._ttdetail.clutter_text.line_wrap_mode = Pango.WrapMode.WORD;
                actor.ttdata._ttdetail.clutter_text.ellipsize = Pango.EllipsizeMode.NONE;

                Main.uiGroup.add_child(actor.ttdata._ttbox);
            } else {
                actor.ttdata._ttlabel.text = titletext;
                actor.ttdata._ttdetail.text = detailtext;
            }

            if (!titletext) {
                actor.ttdata._ttlabel.hide();
            } else {
                actor.ttdata._ttlabel.show();
            }
            if (!detailtext) {
                actor.ttdata._ttdetail.hide();
            } else {
                actor.ttdata._ttdetail.show();
            }

            let [stageX, stageY] = actor.get_transformed_position();
            let [iconWidth, iconHeight] = actor.get_transformed_size();
            let actorWidth = actor.ttdata._ttbox.get_width();
            let actorHeight = actor.ttdata._ttbox.get_height();
            let y = stageY + iconHeight + 5;
            let x = stageX - Math.round((actorWidth - iconWidth)/2);
            if (x < this._stageX) {
                x = this._stageX;
            }
            if (y < this._stageY) {
                y = this._stageY;
            }
            if ((x + actorWidth) > (this._stageX + this._stageWidth)) {
                x = this._stageX + this._stageWidth - actorWidth;
            }
            if ((y + actorHeight) > (this._stageY + this._stageHeight)) {
                y = this._stageY + this._stageHeight - actorHeight;
            }

            // do not show label move if not in showing mode
            if (actor.ttdata._labelShowing) {
                Tweener.addTween(actor.ttdata._ttbox, {
                    x: x,
                    y: y,
                    time: this._SLIDETIME,
                    transition: 'easeOutQuad',
                });
            } else {
                actor.ttdata._ttbox.set_position(x, y);
                Tweener.addTween(actor.ttdata._ttbox, {
                    opacity: 255,
                    time: this._LABELSHOWTIME,
                    transition: 'easeOutQuad',
                });
                actor.ttdata._labelShowing = true;
            }
        } else {
            // No tooltip to show : act like we're leaving an icon
            this._onLeave(actor);
        }
    }
}

const AALCathegory_Menu_Item = GObject.registerClass({
    GTypeName: 'AALCathegory_Menu_Item',
}, class AALCathegory_Menu_Item extends St.Button {

    _init(topClass, type, cathegory, launchers) {
        this.topClass = topClass;
        this.cat = cathegory;
        this.launchers = launchers;
        this.launcherType = type;
        super._init({label: GLib.markup_escape_text(cathegory,-1), style_class: "world-clocks-button button activityAppLauncherButton", toggle_mode: true, can_focus: true, track_hover: true});
        this.connect("clicked", () => {
            this.topClass._clickedCathegory(this);
        });
        this.show();
    }
});
