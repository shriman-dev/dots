��    B      ,  Y   <      �     �     �     �  
   �     �     �     �  N   �  A   3     u     {     �     �  #   �  F   �            %   2     X     d     q     �     �     �     �  	   �     �     �     �     �     �                    !     '     8     H  S   U     �     �  ?   �     �     �     	     	     3	     ;	  P   Z	     �	     �	     �	     �	  0   �	  	   
     
     &
     C
     V
     p
     w
     �
  	   �
     �
     �
  M  �
          $     *  
   .     9  	   O     Y  Y   e  V   �            
   1     <  3   A  _   u     �     �  C   �     C     O     ]     }     �     �  +   �     �     �       	               	   "     ,     9     >     J     \  
   p  �   {             3     
   N  	   Y     c      |  	   �  /   �  8   �            	     ,   )  >   V  
   �     �  $   �     �     �     	          $     2     ;  "   O     4                 %   /   B              ;       0                 @   $            '   A   *   >   &   	              1                    8       
             ?       "       5             6   :   9      <      2   !      ,       7      -           3                         =            +      .   )   #             (    About Action Alt Appearance Apply Changes Border Color Border Size CAUTION: These settings when enabled are buggy or can cause the shell to crash Change the modifier for <b>tiling</b> windows via mouse/drag-drop Color Container Shortcuts Control key Ctrl Default Drag-and-Drop Center Layout Delete text to unset. Press Return key to accept. Focus out to ignore. Development Development in Progress... Drag-Drop Tiling Modifier Key Options Editor Mode Experimental Floated Focus Hint Focus Shortcuts Forge Panel Settings Gaps Gaps Hidden when Single Gaps Size Gaps Size Increments Home Keyboard Layout Legend Logger Level Modifier Keys None Notes Open Preferences Other Shortcuts Palette Mode Provide workspace indices to skip. E.g. 0,1. Empty text to disable. Enter to accept Reset Resets Select <i>None</i> to <u>always tile immediately</u> by default Settings Shortcut Skip Workspace Tiling Split Direction Hint Stacked Stacked Focus Hint and Preview Stacked Tiling Mode (Stack windows on top of each other while still being tiled) Super Syntax Tabbed Tabbed Focus Hint and Preview Tabbed Tiling Mode (Group tiled windows as tabs) Tile Mode Tile Modifier Tiled Focus Hint and Preview Update Keybindings Update Workspace Settings Window Window Shortcuts Windows key Workspace Workspace Shortcuts to previous value when invalid Project-Id-Version: Forge
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2021-12-29 19:04+0100
Last-Translator: Heimen Stoffels <vistausss@fastmail.com>
Language-Team: 
Language: nl
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
X-Generator: Poedit 3.0
 Over Actie Alt Vormgeving Wijzigingen toepassen Randkleur Randbreedte LET OP: deze voorkeuren kunnen instabiliteit veroorzaken of zelfs de shell laten crashen! Wijzig de actietoets voor het <b>tegelen</b> van vensters middels slepen-en-neerzetten Kleur Containersneltoetsen Ctrl-toets Ctrl Standaardindeling: gecentreerd slepen-en-neerzetten Wis de tekst om de toets wijzigen. Druk op enter om toe te passen. Klik erbuiten om te negeren. Ontwikkeling De ontwikkeling is gaande… Opties omtrent actietoets voor tegelen middels slepen-en-neerzetten Bewerkmodus Experimenteel Focushint bij zwevende vensters Focussneltoetsen Forge-paneelvoorkeuren Ruimtes Ruimtes verbergen bij één geopend venster Ruimte tussen vensters Ruimteverhogingen Home Sneltoets Indeling Legenda Logniveau Actietoetsen Geen Opmerkingen Voorkeuren openen Overige sneltoetsen Paletmodus Geef op op welke werkbladen er niet dient te worden getegeld, bijv. 0,1. Laat leeg om uit te schakelen. Druk Enter om toe te passen. Standaardwaarden Herstelt Kies <i>Geen</i> om <u>altijd direct te tegelen</u> Voorkeuren Sneltoets Niet tegelen op werkblad Hint bij veranderen van richting Gestapeld Hint bij gestapelde tegelfocus en voorvertoning Gestapelde tegelmodus (stapel vensters in de tegelmodus) Super Syntaxis Tabbladen Hint bij tabblad-tegelfocus en voorvertoning Tabblad-tegelmodus (groepeer getegelde vensters als tabbladen) Tegelmodus Tegel-actietoets Hint bij tegelfocus en voorvertoning Sneltoetsen bijwerken Werkbladvoorkeuren bijwerken Venster Venstersneltoetsen Windows-toets Werkblad Werkbladsneltoetsen de standaardwaarde indien ongeldig 