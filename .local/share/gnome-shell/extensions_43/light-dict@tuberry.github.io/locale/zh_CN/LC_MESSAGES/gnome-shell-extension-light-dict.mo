��    N      �  k   �      �     �     �  	   �     �     �     �     �  	   �     �  &   �     "     ?     L     Z     i     n  /   z     �  	   �     �     �     �  
   �  	   �     �       6        M     _  u   l     �  6   �     	     "	     @	     [	  	   a	     k	  	   q	  
   {	     �	  *   �	     �	     �	     �	     �	  	   �	     �	     
     
  !   
     ?
     M
     Y
     g
     p
     y
  B   �
  =   �
                    $     8     @     N  
   _     j  	   o     y     �     �     �  >   �  @   +  7   l     �    �     �     �  	   �     �                  	        %     ,     K     d     q     ~     �     �  .   �     �     �     �     �     �                     -  6   D     {     �  <   �     �  0   �     
          -     F     M     Z     a     h     o     v     �     �     �     �     �     �     �     �     �     
          $     1  	   8     B  6   O  -   �     �     �     �     �     �     �     �  	   
               (     A     Y     x  9   �  6   �  7        <        4             
           '   I      ,      A           J            >   ;      &         =           8              $             B              "      3          G   1                  E   -   +              M   L           ?              D               	   9       0      )   /   *   #   !   C   7   .            K   :   (       <   @       5   N   F   %              6   H            2    About Add Allowlist Application list Area Autohide interval Basic Blocklist Clear Click or press ENTER to commit changes Click the app icon to remove Command type Commit result Content copied Copy Copy result Depends on python-opencv and python-pytesseract Disable Dwell OCR Enable systray Enable tooltip Help Hide title Icon name Icon tooltip LD DBus error. (~_~) Leave RegExp/application list blank for no restriction Left click to run Left command Lightweight extension for on-the-fly manipulation to primary selections, especially optimized for Dictionary lookups. Line Middle click the panel to copy the result to clipboard OCR OCR preprocess failed. (-_-;) OCR process failed. (-_-;) Other Page size Panel Paragraph Parameters Passive Passive means that pressing Alt to trigger Passive mode Paste Paste content parsing failed Popup Proactive RegExp filter RegExp matcher Remove Right click to run and hide panel Right command Run command Select result Settings Shortcut Show result Simulate keyboard input in JS statement: <i>key("Control_L+c")</i> Substitute <b>LDWORD</b> for the selected text in the command Swift Switch to %s style Tips Too marginal. (>_<) Trigger Trigger style Trim blank lines Version %d Word Work mode flash on the detected area invoke LD around the cursor show this help message and exit specify LD swift style name specify LD trigger style: [%(choices)s] (default: %(default)s) specify language(s) used by Tesseract OCR (default: %(default)s) specify work mode: [%(choices)s] (default: %(default)s) suppress error messages Project-Id-Version: gnome-shell-extension-light-dict 55
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2021-08-18 13:20+0800
Last-Translator: Automatically generated
Language-Team: none
Language: zh_CN
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
 关于 添加 白名单 应用名单 区域 隐藏延迟 基本 黑名单 清除 单击或按回车提交更改 点击应用图标移除 命令类型 提交结果 内容已复制 复制 复制结果 依赖于 python-opencv 和 python-pytesseract 禁用 悬停取词 托盘图标 启用提示 帮助 隐藏标题 图标名称 图标提示 LD DBus 出错。(~_~) 正则表达式或应用列表留空则无相应限制 左键运行 左键命令 即时操作所选文本的轻量扩展，为查词而生。 单行 中键点击气泡复制结果到系统剪切板 取词 OCR预处理失败。(-_-;) OCR处理失败。(-_-;) 其它 页面容量 气泡 段落 参数 被动 被动意为按住Alt触发 被动模式 粘贴 粘贴内容解析失败 弹出 主动 正则过滤 正则匹配 移除 右键运行并关闭气泡 右键命令 运行命令 选取结果 设置 快捷键 显示结果 JS 语句模拟键盘输入: <i>key('Control_L+c')</i> 以 <b>LDWORD</b> 代替命令中所选文本 即时 切换至%s风格 提示 太靠边了。(>_<) 触发 触发风格 去除空行 版本 %d 单词 工作模式 在目标区域上闪烁 在光标附近唤起LD 显示此帮助信息并退出 指定LD即时风格名称 指定LD触发风格: [%(choices)s] (默认: %(default)s) 指定 Tesseract OCR 所用语言(默认: %(default)s) 指定工作模式: [%(choices)s] (默认: %(default)s) 禁言报错消息 