��    @        Y         �     �  B   �     �  	   �     �     �            	        #     @     M     [     b  N   n     �  8   �     �       
     J   '  	   r     |  6   �     �     �  j   �     J  6   O     �     �     �  -   �  	   �     �  	   �  
   �     	     	     	  	   	     (	     6	  !   E	     g	     u	     �	     �	     �	     �	  B   �	  =   �	     .
     4
     <
  :   O
     �
     �
  	   �
     �
  
   �
     �
  	   �
  l  �
     ?  M   D  (   �     �     �     �     �  	   �       2        C     R     g     p  U   �     �  9   �     !     ;     L  ]   [     �     �  5   �          0  n   ?     �  6   �  <   �     (     ,  A   2     t     �  	   �     �     �     �     �  
   �     �     �  5   �     '     7     J  
   _  	   j     t  F   �  >   �     
            C   .     r     w     �     �  	   �     �  	   �                      9       .       #   3       6   @       
          /      5                   "   $      <         ,      -           +                                 8   *   ;   !   =                                  	   7   )          2   >      (         0   4   %   &   ?           '                1          :        About Add the icon to <i>~/.local/share/icons/hicolor/symbolic/apps/</i> Add/remove current app Allowlist Application list Area Autohide interval Basic Blocklist Click the app icon to remove Command type Commit result Common Copy result Depends on python-opencv, python-pytesseract and python-googletrans (optional) Disable Double click a list item on the left to change the name. Enable systray Enable tooltip Hide title Hold <b>Alt/Shift</b> to function when highlighting in <b>Passive mode</b> Icon name Icon tooltip Leave RegExp/application list blank for no restriction Left click to run Left command Lightweight extension for instant action to primary selection, especially optimized for Dictionary lookup. Line Middle click the panel to copy the result to clipboard Need modifier to trigger or not OCR OCR:  Only one item can be enabled in swift style.
 Page size Panel Paragraph Parameters Passive Passive mode Popup Proactive RegExp filter RegExp matcher Right click to run and hide panel Right command Run command Select result Settings Shortcut Show result Simulate keyboard input in JS statement: <i>key("Control_L+c")</i> Substitute <b>LDWORD</b> for the selected text in the command Swift Swift:  Switch to %s style The first one will be used by default if none is enabled.
 Tips Trigger style Trigger:  Trim blank lines Version %d Word Work mode Project-Id-Version: gnome-shell-extension-light-dict
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2021-09-15 20:14+0200
Last-Translator: Heimen Stoffels <vistausss@fastmail.com>
Language-Team: none
Language: nl
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
X-Generator: Poedit 3.0
 Over Voeg het pictogram toe aan <i>~/.local/share/icons/hicolor/symbolic/apps/</i> Huidige toepassing toevoegen/verwijderen Witte lijst Toepassingenlijst Gebied Automatisch verbergen na Eenvoudig Zwarte lijst Klik op een toepassingspictogram om te verwijderen Soort opdracht Resultaat vastleggen Algemeen Resultaat kopiëren Afhankelijkheden: python-opencv, python-pytesseract en python-googletrans (optioneel) Uitschakelen Dubbelklik op een lijstitem om de naam ervan te wijzigen. Systeemvakpictogram tonen Hulpballon tonen Naam verbergen Houd <b>Alt/Shift</b> ingedrukt om uit te voeren tijdens markeren in de <b>passieve modus</b> Pictogramnaam Pictogram-hulpballon Laat de lijsten leeg om geen beperkingen in te voeren Linksklikken om uit te voeren Linkeropdracht Een lichte uitbreiding voor het uitvoeren van directie acties op selecties, gericht op woordenboekopzoekingen. Regel Middelklik op het paneel om het resultaat te kopiëren Of er een sneltoets moet worden gebruikt bij deze aanroeping OCR OCR:  In de swift-stijl kan er slechts één item worden ingeschakeld.
 Paginagrootte Paneel Paragraaf Aanvullende opties Passief Passieve modus Pop-up Pro-actief RegExp-filter RegExp-overeenkomst Rechtsklikken om uit te voeren en paneel te verbergen Rechteropdracht Opdracht uitvoeren Resultaat selecteren Voorkeuren Sneltoets Resultaat tonen Simuleer toetsenbordinvoer in de JS-opdracht <i>key("Control_L+c")</i> Vervang <b>LDWORD</b> in de geselecteerde tekst in de opdracht Swift Swift:  Overschakelen naar %s Als er niks is ingeschakeld, wordt de eerste op de lijst gebruikt.
 Tips Aanroepingsstijl Aanroeping:  Aantal witregels beperken Versie %d Woord Werkmodus 