��    O      �  k         �     �     �  	   �     �     �     �          1     8     ?     M     S     a     s     �  
   �     �     �  3   �       	     
   "  6   -  	   d     n     �     �     �  
   �     �     �  -   �     �     	  	    	     *	  1   2	  3   d	     �	     �	     �	  (   �	     �	     �	     
     
     
     +
     0
     >
     L
     [
     d
     {
     �
  "   �
     �
     �
     �
     �
       1   *     \     e  %   v  %   �  !   �     �     �       	        (     ,     ;     P     g     ~     �  $  �     �     �     �     �     �          /     I     O     U     k     y     �  *   �     �  
   �  -   �       K   .     z     �  	   �  I   �  	   �     �          #     =  
   C     N     T  5   h     �  #   �     �     �  :   �  G   )     q     w     �  4   �     �     �     �     �  '     	   *     4     L     Y     p     ~     �  "   �  0   �          #  &   6     ]  -   {  ;   �  
   �     �  :   	  4   D  -   y     �  '   �     �  
                "   "     E     e     �     �     N   <       #   8       C   +       &   ,                J      E      $      @          6   0             5                 =           /   (   7                ?   >                    B   9                  2           !       L   
   M   I             %           '      )      K   H   .       3   4       O   *      	   D       F   "              ;       :       G   1   A               -    About Actions App Icons App Icons Taskbar App Icons Taskbar GitLab App Icons Taskbar Settings App Icons Taskbar Version Bottom Center Click Actions Cycle Cycle Windows Donate via PayPal Focused Indicator Color GNOME Version Git Commit Hide Window Previews Delay Hover Actions Hovering a window preview will focus desired window Icon Desaturate Factor Icon Size Icon Style Icon themes may not have a symbolic icon for every app Indicator Indicator Location Isolate Monitors Isolate Workspaces Left Left Click Load Load Settings Modify Left Click Action of Running App Icons Multi-Dashes Multi-Window Indicator Style No Action OS Name Offset the position within the above selected box Opacity of non-focused windows during a window peek Panel Panel Height Panel Location Panel menu button that shows focused app Position Offset Position in Panel Regular Right Running Indicator Color Save Save Settings Scroll Action Scroll Actions Settings Show Activities Button Show All Apps Show App Menu Button Show App Title on Focused App Icon Show Apps Button Show Favorites Show Panels on All Monitors Show Running Apps Show Window Previews Delay Show running apps and favorites on the main panel Symbolic Taskbar Behavior Time in ms to hide the window preview Time in ms to show the window preview Time in ms to trigger window peek Toggle / Cycle Toggle / Cycle + Minimize Toggle / Preview Tool-Tips Top Window Peeking Window Peeking Delay Window Peeking Opacity Window Preview Options Window Previews Windowing System Project-Id-Version: aztaskbar
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2023-02-10 13:41+0100
Last-Translator: Onno Giesmann <nutzer3105@gmail.com>
Language-Team: 
Language: de
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Poedit 3.2.2
 Info Aktionen Anwendungssymbole App Icons Taskbar App Icons Taskbar GitLab App Icons Taskbar-Einstellungen App Icons Taskbar-Version Unten Mitte Aktionen beim Klicken Durchwechseln Fenster durchwechseln Spenden über PayPal Indikator-Farbe für fokussierte Anwendung GNOME-Version Git-Commit Ausblende-Verzögerung der Fenster-Vorschauen Aktionen beim Berühren Beim Berühren der Fenster-Vorschau wird das fokussierte Fenster angedeutet Symbolentsättigung Symbolgröße Symbolart Nicht alle Symbolthemen enthalten stilisierte Symbole für jede Anwendung Indikator Indikator-Position Bildschirme isolieren Arbeitsflächen isolieren Links Linksklick Laden Einstellungen laden Aktion bei Linksklick auf laufende Anwendung anpassen Mehrere Striche Indikatorstil bei mehreren Fenstern Keine Aktion Betriebssystem Position innerhalb des oben gewählten Bereichs verrücken Deckkraft der nicht-fokussierten Fenster während der Fenster-Andeutung Panel Höhe des Panels Position des Panels Zeigt den Menüknopf für das fokussierte Fenster an Positionsversatz Position im Panel Normal Rechts Indikator-Farbe für laufende Anwendung Speichern Einstellungen speichern Sroll-Aktion Aktionen beim Scrollen Einstellungen Aktivitäten-Knopf anzeigen Alle Anwendungen anzeigen Knopf mit Anwendungsmenü anzeigen Fenstertitel der fokussierten Anwendung anzeigen Anwendungen-Knopf anzeigen Favoriten anzeigen Panels auf allen Bildschirmen anzeigen Laufende Anwendungen anzeigen Einblende-Verzögerung der Fenster-Vorschauen Laufende Anwendungen und Favoriten im oberen Panel anzeigen Stilisiert Verhalten der Taskleiste Zeit in ms, nach der die Fenstervorschau ausgeblendet wird Zeit in ms, bevor die Fenstervorschau angezeigt wird Zeit in ms, bevor das Fenster angedeutet wird Umschalten / Durchwechseln Umschalten / Durchwechseln + Minimieren Umschalten / Vorschau Minihilfen Oben Fenster-Andeutung Verzögerung der Fenster-Andeutung Deckkraft der Fenster-Andeutung Optionen für Fenster-Vorschau Fenster-Vorschauen Fenstersystem 