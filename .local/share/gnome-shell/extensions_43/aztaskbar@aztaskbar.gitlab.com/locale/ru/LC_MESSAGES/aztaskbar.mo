��    O      �  k         �     �     �  	   �     �     �     �          1     8     ?     M     S     a     s     �  
   �     �     �  3   �       	     
   "  6   -  	   d     n     �     �     �  
   �     �     �  -   �     �     	  	    	     *	  1   2	  3   d	     �	     �	     �	  (   �	     �	     �	     
     
     
     +
     0
     >
     L
     [
     d
     {
     �
  "   �
     �
     �
     �
     �
       1   *     \     e  %   v  %   �  !   �     �     �       	        (     ,     ;     P     g     ~     �  �  �     +     C  !   T     v     �  $   �     �  
   �     �  /        5     E  *   ^  \   �     �  
   �  a     *   f  y   �  @        L     f  �   �       -     '   I  >   q  
   �  /   �     �  %   �  �   $     �  :   �          %  h   ;  |   �     !     .  %   H  T   n  #   �  *   �          !  I   .     x  %   �  #   �  #   �     �  0     0   =  ?   n  v   �  :   %  <   `  B   �  >   �  a     k   �     �  *     e   -  m   �  U     *   W  =   �  +   �     �     �  (     Q   5  N   �  O   �  <   &     c     N   <       #   8       C   +       &   ,                J      E      $      @          6   0             5                 =           /   (   7                ?   >                    B   9                  2           !       L   
   M   I             %           '      )      K   H   .       3   4       O   *      	   D       F   "              ;       :       G   1   A               -    About Actions App Icons App Icons Taskbar App Icons Taskbar GitLab App Icons Taskbar Settings App Icons Taskbar Version Bottom Center Click Actions Cycle Cycle Windows Donate via PayPal Focused Indicator Color GNOME Version Git Commit Hide Window Previews Delay Hover Actions Hovering a window preview will focus desired window Icon Desaturate Factor Icon Size Icon Style Icon themes may not have a symbolic icon for every app Indicator Indicator Location Isolate Monitors Isolate Workspaces Left Left Click Load Load Settings Modify Left Click Action of Running App Icons Multi-Dashes Multi-Window Indicator Style No Action OS Name Offset the position within the above selected box Opacity of non-focused windows during a window peek Panel Panel Height Panel Location Panel menu button that shows focused app Position Offset Position in Panel Regular Right Running Indicator Color Save Save Settings Scroll Action Scroll Actions Settings Show Activities Button Show All Apps Show App Menu Button Show App Title on Focused App Icon Show Apps Button Show Favorites Show Panels on All Monitors Show Running Apps Show Window Previews Delay Show running apps and favorites on the main panel Symbolic Taskbar Behavior Time in ms to hide the window preview Time in ms to show the window preview Time in ms to trigger window peek Toggle / Cycle Toggle / Cycle + Minimize Toggle / Preview Tool-Tips Top Window Peeking Window Peeking Delay Window Peeking Opacity Window Preview Options Window Previews Windowing System Project-Id-Version: 
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2023-01-23 00:03+1000
Last-Translator: Ser82-png <asvmail.as@gmail.com>
Language-Team: 
Language: ru
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=3; plural=(n%10==1 && n%100!=11 ? 0 : n%10>=2 && n%10<=4 && (n%100<12 || n%100>14) ? 1 : 2);
X-Generator: Poedit 3.0.1
 О расширении Действия Значки приложений App Icons Taskbar App Icons Taskbar на GitLab Настройки App Icons Taskbar Версия App Icons Taskbar Снизу В центре Действия при щелчке мышью По кругу Окна по кругу Пожертвовать через PayPal Цвет индикатора приложения, находящегося в фокусе Версия GNOME Git Commit Задержка до скрытия окна предварительного просмотра Действия при наведении Наведение на окно предварительного просмотра выведет нужное окно Коэффициент обесцвечивания значка Размер значка Стиль значков Темы значков могут содержать символические значки не для всех приложений Индикатор Расположение индикатора Изолировать мониторы Изолировать рабочие пространства Слева Щелчок левой кнопкой мыши Загрузить Загрузить настройки Изменить действие щелчка левой кнопкой мыши на значках запущенных приложений Мульти-чёрточки Стиль многооконного индикатора Нет действий Название ОС Сместить положение в пределах диапазона выбранного выше Непрозрачность несфокусированных окон во время быстрого просмотра Панель Высота панели Расположение панели Кнопка меню приложения, находящегося в фокусе Смещение положения Расположение на панели Обычные Справа Цвет индикатора запущенного приложения Сохранить Сохранить настройки Действие прокрутки Действия прокрутки Настройки Показывать кнопку «Обзор» Показывать все приложения Показывать кнопку меню приложения Показывать на значке название приложения, находящегося в фокусе Показывать кнопку «Приложения» Показывать избранные приложения Показывать панель на всех мониторах Показывать запущенные приложения Задержка при показе окна предварительного просмотра Показ запущенных и избранных приложений на главной панели Символьные Поведение панели задач Время в мс для скрытия окна предварительного просмотра Время в мс для отображения окна предварительного просмотра Время в мс для запуска быстрого просмотра окна Переключение / По кругу Переключение / По кругу + Свернуть Переключение / Просмотр Подсказки Сверху Быстрый просмотр окна Задержка при выводе быстрого просмотра окна Непрозрачность при быстром просмотре окна Параметры окна предварительного просмотра Окна предварительного просмотра Оконная система 