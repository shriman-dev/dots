��    D      <  a   \      �     �     �     
  
        )     7  
   O     Z  !   g     �     �     �     �     �     �     �     �               "     )  
   .  
   9     D     I     O     V  	   ^     h     p     ~  
   �     �     �  
   �     �     �  
   �     �  
   �     �  
                  1     B  
   V     a     n     �     �     �     �     �     �     �     �  	   �  	   	  S   	  	   e	     o	  	   {	     �	     �	     �	     �	  �  �	     i     �     �  
   �     �     �     �     �     
     *     G  
   V     a     e     u     �     �     �     �     �     �     �     �     �            	          	   -     7     G  	   Y     c     j  
   z     �     �     �     �     �  
   �     �     �  !   �          2     I     W      i     �     �     �     �  
   �     �     �                  W   .     �     �     �     �     �     �     �     <               	                                 >      2   B              -              3       0   /       D   &       
         *           @   6       C   !                "   4          8   #           5             )      (   .   %      ?                      :   7                         $       ,   ;                 =   1   9   A       +   '    About Burn-My-Windows Add New Profile... Animation Time [ms] Apparition Balanced Mode Balanced or Performance Blow Force Broken Glass Burn-My-Windows has been updated! Claw Scratch Scale Closing Windows Cold Breeze Color Create an Effect Dark Color Scheme Dark and Smutty Default Color Scheme Default Fire Dialog Windows Donate Doom Energize A Energize B Fire Glide Glitch Gravity Hell Fire Hexagon High Priority Horizontal Scale Incinerate Matrix Normal Windows On Battery Opening Windows Performance Mode Pixel Size Pixel Wheel Pixel Wipe Pixelate Plugged In Portal Power Saver or Balanced Power-Saver Mode Preview this effect Randomness Report a Bug Reset to Default Value Santa is Coming Scale Shatter From Pointer Location Snap of Disintegration Speed Standard Profile Strength T-Rex Attack TV Effect TV Glitch The shards will fly away from where your mouse pointer was when closing the window. Tip Color Trail Color Translate Vertical Overshooting Vertical Scale View Changelog Wisps Project-Id-Version: burn-my-windows
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2023-02-21 15:39+0000
Last-Translator: Isaac Dias <mr-isaac@ig.com.br>
Language-Team: Portuguese (Brazil) <https://hosted.weblate.org/projects/burn-my-windows/core/pt_BR/>
Language: pt_BR
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n > 1;
X-Generator: Weblate 4.16-dev
 Sobre o Burn-My-Windows Adicionar Novo Perfil... Período de Animação [ms] Aparição Modo balanceado Balanceado ou performance Força do Golpe Vidro Quebrado Burn-My-Windows foi atualizada! Escala do Arranhão da Garra Fechar Janelas Brisa Fria Cor Criar um efeito esquema de cores escuras Escuro e enferrujado Esquema de cores padrão Fogo Padrão Dialago de Janelas Doe Doom Energizar A Energizar B Fogo Deslizar Defeito Gravidade Fogo Do Inferno Hexágono Alta prioridade Escala Horizontal Incinerar Matrix Janelas Normais na bateria Abrir Janelas Modo performance Tamanho do Pixel Roda pixelizada Limpeza de pixels Pixel izar No carregador Portal Economia de energia ou balanceado Modo economia de energia Visualizar esse efeito Aleatoriedade Reportar um erro. Resetar para os Valores Padrões Papai Noel está chegando. Escala Partir do ponteiro do mouse Estalo da Desintegração Velocidade Perfil padrão Força Ataque do T-Rex Efeito de TV Defeito na TV Os fragmentos voarão para longe de onde estava o ponteiro do mouse ao fechar a janela. Cor da ponta Cor da Trilha Traduzir Ultrapassagem Vertical Escala Vertical Veja as alterações Fagulhas 