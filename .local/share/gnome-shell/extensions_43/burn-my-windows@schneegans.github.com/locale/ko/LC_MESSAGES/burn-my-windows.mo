��    V      �     |      x     y     �     �     �  
   �  
   �     �     �     �            (   '     P     \     b  8   s     �     �     �     �     �  
   �  
   �     �     �     	     
	  
   	     	     *	     3	  	   ;	     E	     M	  .   ^	  1   �	  
   �	  
   �	     �	     �	     �	     �	  
    
     
  
   
     "
     +
     2
     :
     I
  
   ]
     h
     u
     �
     �
     �
     �
     �
     �
     �
     �
                    %     6     ?     Q  	   ^  	   h  S   r     �  	   �     �  	   �  
   �     �               +     :     I     U     ^  3   d  �  �     4     C     Z     h     �     �     �     �     �     �     �  /         0     A     H  U   Y     �     �     �     �     �     �     �                    &     4     B     S     c     q  	        �  6   �  J   �  	     
   #     .     ;     M     T     e     s     �     �     �     �     �     �     �               *     8     I     W     e     �     �     �     �     �     �     �     �     �       	           M   1  	        �     �     �     �     �     �     �     �               -     :  8   G         5   U           B   (           @          6   F   )   P   M   ;                  :                    +   <   1   K   I       S   H   G                C   !   ,       *                     N          0   L   	   Q   3           9          =   O          &          .          "                   
   J   $         8           >   4                                #                   /   A       V       E   D   R         -          2   %   ?   '                  7   T    3D Noise About Burn-My-Windows Additive Blending Animation Time [ms] Apparition Blow Force Broken Glass Bronze Sponsors Choose a Preset Claw Scratch Count Claw Scratch Scale Close this Window to Preview the Effect! Cold Breeze Color Create an Effect Creates a more dynamic fire but requires more GPU power. Dark and Smutty Default Fire Details Donate Doom Energize A Energize B Fire Flash Color Glide Glitch Glow Color Gold Sponsors Gradient Gravity Hell Fire Hexagon Horizontal Scale If disabled, a random location will be chosen. If multiple are selected, one is chosen randomly. Incinerate Line Color Matrix Mesh Line Width Noise Past Sponsors Pixel Size Pixel Wheel Pixel Wipe Pixelate Portal Presets Preview for %s Preview this effect Randomness Report a Bug Reset to Default Value Rotation Speed Santa is Coming Scale Shake Intensity Shatter From Pointer Location Shift Silver Sponsors Snap of Disintegration Speed Spoke Count Squish Start at Pointer Strength Suction Intensity T-Rex Attack TV Effect TV Glitch The shards will fly away from where your mouse pointer was when closing the window. Tilt Tip Color Trail Color Translate Turbulence Twirl Intensity Vertical Overshooting Vertical Scale View Changelog Visit Homepage Warp Effect Whirling Wisps You may want to enable this for dark window themes. Project-Id-Version: burn-my-windows
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2023-02-11 06:35+0000
Last-Translator: Seong-ho Cho <darkcircle.0426@gmail.com>
Language-Team: Korean <https://hosted.weblate.org/projects/burn-my-windows/core/ko/>
Language: ko
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=1; plural=0;
X-Generator: Weblate 4.16-dev
 3차원 잡티 Burn-My-Windows 정보 혼합 효과 애니메이션 시간 [ms] 혼령 타격 강도 유리 깨짐 브론즈 스폰서 사전설정 선택 발톱 자국 갯수 발톱 자국 크기 비율 이 창을 닫아 효과를 확인해보세요! 냉랭한 바람 색상 효과 만들기 좀 더 역동적인 화염을 만들지만 더 많은 GPU 성능이 필요합니다. 어둡고 찝찝함 기본 화염 밀도 기부 둠 에너자이즈 A 에너자이즈 B 화염 번쩍임 색상 활공 잡티 화면 불빛 색상 골드 스폰서 그레디언트 중력 계수 지옥 화염 육각형 수평 규모 설정을 끄면, 임의 위치에서 시작합니다. 여러 항목을 선택하면 그 중 하나를 임의로 선택합니다. 불태움 선 색상 매트릭스 도형 선 굵기 잡티 이전 스폰서 픽셀 크기 픽셀 바퀴회전 픽셀 쓸어내기 픽셀 파편 차원 관문 사전설정 %s 미리 보기 이 효과를 미리 봅니다 임의 상수 버그 보고 기본값으로 초기화 회전 속도 산타의 방문 비율 조정 떨림 강도 포인터 위치에서 깨짐 위치 이동 실버 스폰서 해체 영상 속도 바퀴살 수 찌그러짐 포인터 위치에서 시작 강도 흡입 강도 티라노 공격 TV 효과 TV 잡티 화면 창을 닫을 때 마우스 포인터 위치에서 조각이 날아갑니다. 기울임 끄트머리 색상 끌림 자국 색상 번역 소용돌이 회전 강도 수직 오버슈팅 수직 규모 변경 이력 보기 홈페이지 방문 왜곡 효과 소용돌이 도깨비불 어두운 창 테마에서 사용하시면 좋습니다. 