��    V      �     |      x     y     �     �     �  
   �  
   �     �     �     �            (   '     P     \     b  8   s     �     �     �     �     �  
   �  
   �     �     �     	     
	  
   	     	     *	     3	  	   ;	     E	     M	  .   ^	  1   �	  
   �	  
   �	     �	     �	     �	     �	  
    
     
  
   
     "
     +
     2
     :
     I
  
   ]
     h
     u
     �
     �
     �
     �
     �
     �
     �
     �
                    %     6     ?     Q  	   ^  	   h  S   r     �  	   �     �  	   �  
   �     �               +     :     I     U     ^  3   d  �  �  	   w     �  !   �     �     �     �     �  !        4  2   P  (   �  K   �  !   �  
        %  p   A      �     �     �                     (     :     G     \     m     v     �     �     �     �     �  +     Q   7  ^   �     �     �       "        ?     F     f     �     �     �     �     �  5   �  =   +     i  *   �  ;   �  %   �          %  +   2  E   ^     �     �     �     �          $  5   5     k  /   t  !   �     �     �  �   �  
   �  !   �     �     �     �  -     -   6  %   d     �      �     �     �     �  a            5   U           B   (           @          6   F   )   P   M   ;                  :                    +   <   1   K   I       S   H   G                C   !   ,       *                     N          0   L   	   Q   3           9          =   O          &          .          "                   
   J   $         8           >   4                                #                   /   A       V       E   D   R         -          2   %   ?   '                  7   T    3D Noise About Burn-My-Windows Additive Blending Animation Time [ms] Apparition Blow Force Broken Glass Bronze Sponsors Choose a Preset Claw Scratch Count Claw Scratch Scale Close this Window to Preview the Effect! Cold Breeze Color Create an Effect Creates a more dynamic fire but requires more GPU power. Dark and Smutty Default Fire Details Donate Doom Energize A Energize B Fire Flash Color Glide Glitch Glow Color Gold Sponsors Gradient Gravity Hell Fire Hexagon Horizontal Scale If disabled, a random location will be chosen. If multiple are selected, one is chosen randomly. Incinerate Line Color Matrix Mesh Line Width Noise Past Sponsors Pixel Size Pixel Wheel Pixel Wipe Pixelate Portal Presets Preview for %s Preview this effect Randomness Report a Bug Reset to Default Value Rotation Speed Santa is Coming Scale Shake Intensity Shatter From Pointer Location Shift Silver Sponsors Snap of Disintegration Speed Spoke Count Squish Start at Pointer Strength Suction Intensity T-Rex Attack TV Effect TV Glitch The shards will fly away from where your mouse pointer was when closing the window. Tilt Tip Color Trail Color Translate Turbulence Twirl Intensity Vertical Overshooting Vertical Scale View Changelog Visit Homepage Warp Effect Whirling Wisps You may want to enable this for dark window themes. Project-Id-Version: burn-my-windows
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2023-01-25 20:52+0000
Last-Translator: Petro S. <911us@duck.com>
Language-Team: Ukrainian <https://hosted.weblate.org/projects/burn-my-windows/core/uk/>
Language: uk
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=3; plural=n%10==1 && n%100!=11 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2;
X-Generator: Weblate 4.16-dev
 3D шум Про Burn-My-Windows Адивне змішування Час анімації (мс) Привид Сила удару Розбите скло Бронзові спонсори Вибрати Пресет Кількість подряпин кігтями Шкала подряпин кігтів Закрийте це вікно, щоб переглянути ефект! Холодний вітерець Колір Створити ефект Створює динамічніший вогонь, але потребує більше ресурсів GPU. Темний та брудний Звичайний вогонь Деталі Пожертва Doom Енергія А Енергія Б Вогонь Колір м'яса Ковзання Глюк Цвіт свічіння Золоті спонсори Градієнт Гравітація Пекельний вогонь Шестикутник Горизонтальний масштаб Якщо вимкнено, буде вибрано випадкове місце. Якщо вибрано декілька, то один обереться випадково. Горіння Колір лінії Матриця Ширина лінії сітки Шум Колишні спонсори Розмір пікселя Піксельне колесо Витерти пікселі Пікселізація Портал Пресети Попередній перегляд ефекту %s Попередній перегляд цього ефекту Випадковість Повідомити про помилку Скинути до початкового значення Швидкість обертання Миколай іде Маштаб Інтенсивність трясіння Розбити від місцезнаходження курсору Зрушення Срібні спонсори Клацання розпаду Швидкість Кількість спиць Хлюпання Починати від вказівника миші Сила Інтенсивність втягування Напад тиранозавра Телевізор ТВ Глюк Осколки розлетяться в сторону того місця, де знаходився курсор мишки під час зачинення вікна. Нахил Колір наконечника Колір сліду Перекласти Турбулентність Інтенсивність обертання Вертекальне перевищення Вертикальний маштаб Журнал змін Завітайте на сайт Ефект деформації Кружляння Вогники Ви можете захотіти увімкнути це для темних тем вікон. 