��    V      �     |      x     y     �     �     �  
   �  
   �     �     �     �            (   '     P     \     b  8   s     �     �     �     �     �  
   �  
   �     �     �     	     
	  
   	     	     *	     3	  	   ;	     E	     M	  .   ^	  1   �	  
   �	  
   �	     �	     �	     �	     �	  
    
     
  
   
     "
     +
     2
     :
     I
  
   ]
     h
     u
     �
     �
     �
     �
     �
     �
     �
     �
                    %     6     ?     Q  	   ^  	   h  S   r     �  	   �     �  	   �  
   �     �               +     :     I     U     ^  3   d  �  �     k     s     �     �     �     �     �     �     �          !  %   ;     a     m     s  A   �     �     �     �  	   �     �     �               &     4     =     J     Z     k  
   t          �     �  ,   �  2   �               #     *     C     H     W     g     w     �     �     �     �     �  
   �     �      �     
          9     ?     [     y     �     �  
   �     �  
   �     �     �     �                 ]   ,     �     �     �     �     �     �     �     �          "     =  	   N     X  B   _         5   U           B   (           @          6   F   )   P   M   ;                  :                    +   <   1   K   I       S   H   G                C   !   ,       *                     N          0   L   	   Q   3           9          =   O          &          .          "                   
   J   $         8           >   4                                #                   /   A       V       E   D   R         -          2   %   ?   '                  7   T    3D Noise About Burn-My-Windows Additive Blending Animation Time [ms] Apparition Blow Force Broken Glass Bronze Sponsors Choose a Preset Claw Scratch Count Claw Scratch Scale Close this Window to Preview the Effect! Cold Breeze Color Create an Effect Creates a more dynamic fire but requires more GPU power. Dark and Smutty Default Fire Details Donate Doom Energize A Energize B Fire Flash Color Glide Glitch Glow Color Gold Sponsors Gradient Gravity Hell Fire Hexagon Horizontal Scale If disabled, a random location will be chosen. If multiple are selected, one is chosen randomly. Incinerate Line Color Matrix Mesh Line Width Noise Past Sponsors Pixel Size Pixel Wheel Pixel Wipe Pixelate Portal Presets Preview for %s Preview this effect Randomness Report a Bug Reset to Default Value Rotation Speed Santa is Coming Scale Shake Intensity Shatter From Pointer Location Shift Silver Sponsors Snap of Disintegration Speed Spoke Count Squish Start at Pointer Strength Suction Intensity T-Rex Attack TV Effect TV Glitch The shards will fly away from where your mouse pointer was when closing the window. Tilt Tip Color Trail Color Translate Turbulence Twirl Intensity Vertical Overshooting Vertical Scale View Changelog Visit Homepage Warp Effect Whirling Wisps You may want to enable this for dark window themes. Project-Id-Version: burn-my-windows
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2023-01-25 20:52+0000
Last-Translator: gnu-ewm <gnu.ewm@protonmail.com>
Language-Team: Polish <https://hosted.weblate.org/projects/burn-my-windows/core/pl/>
Language: pl
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=3; plural=n==1 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2;
X-Generator: Weblate 4.16-dev
 Szum 3D Informacje o Burn-My-Windows Mieszanie Addytywne Czas Animacji [ms] Teleportacja Siła Uderzenia Rozbicie Szkła Brązowi Sponsorzy Wybierz Obecny Liczba Drapnięć Pazurów Skala Drapnięć Pazurów Zamknij to Okno, aby zobaczyć Efekt! Zimna Bryza Kolor Stwórz Efekt Tworzy bardziej dynamiczny ogień, ale wymaga większej mocy GPU. Ciemny i Brudny Domyślny Ogień Szczegóły Wspomóż Doom Energetyzacja A Energetyzacja B Ogień Kolor Błysku Poślizg Zakłócenia Kolor Poświaty Złoci Sponsorzy Gradient Grawitacja Ogień Piekielny Sześciokąt Skala Pozioma Jeśli wyłączone, lokalizacja jest losowa. Jeśli wybrano kilka, efekt jest wybierany losowo. Spopielenie Kolor Linii Matrix Szerokość Linii Siatki Szum Byli Sponsorzy Rozmiar Pikseli Pikselowe Koło Pikselowe Przetarcie Pikselowanie Portal Predefiniowane Podgląd dla %s Podgląd efektu Losowość Zgłoś Błąd Zresetuj do Domyślnej Wartości Prędkość obrotu Nadchodzi Święty Mikołaj Skala Intensywność Wstrząsania Roztrzaskaj z Pozycji Kursora Zmiana Srebrni Sponsorzy Pstryknięcie Dezintegracji Szybkość Liczba szprych Zgniatanie Start od Kursora Natężenie Intensywność Ssania Atak T-Rex-a Efekt TV Zakłócenia TV Odłamki odlecą z miejsca, w którym znajdował się wskaźnik myszy podczas zamykania okna. Przechylenie Kolor Końcówki Kolor Śladu Przetłumacz Turbulencja Intensywność Wirowania Przestrzelenie Pionowe Skala Pionowa Pokaż Dziennik Zmian Odwiedź Stronę Główną Efekt Wypaczenia Wirowanie Ogniki Możesz chcieć to włączyć w przypadku ciemnych motywów okien. 