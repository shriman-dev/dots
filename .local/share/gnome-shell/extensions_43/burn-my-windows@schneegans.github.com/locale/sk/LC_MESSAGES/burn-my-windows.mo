��    J      l  e   �      P     Q     Z     p     �  
   �  
   �     �     �     �     �     �  (   �     (     4     :  8   K     �     �     �     �  
   �  
   �     �     �  
   �     �     �     �  	   �            .   !  1   P  
   �  
   �     �     �     �     �  
   �     �  
   �     �     �     �  
   	     	     	     4	     D	     J	     Z	     x	     �	     �	     �	     �	     �	     �	  	   �	  S   �	  	   ?
     I
  	   U
  
   _
     j
     z
     �
     �
     �
     �
     �
  3   �
  �       �     �     �     �     �                    0     C  !   V  ,   x     �     �     �  B   �          #  	   5     ?     D     P     \     b     o     |     �     �     �  
   �     �  ,   �  3   �  	   -     7     C     J     `     e     w     �     �     �  
   �     �     �     �     �          *     8      J     k     �  
   �     �     �     �     �  
   �  E   �     2     ?  
   M     X     d     w     �     �     �     �  
   �  C   �            %   7                  8   @          +   9      <   5   3          E      	      >   $       F   D       &       /   ;      H          I      0   =       A   ?             !       #       
   ,            :                  4       .       2                        G   (   C   1      J          "       )      6      B                      '       *       -        3D Noise About Burn-My-Windows Additive Blending Animation Time [ms] Apparition Blow Force Broken Glass Bronze Sponsors Choose a Preset Claw Scratch Count Claw Scratch Scale Close this Window to Preview the Effect! Cold Breeze Color Create an Effect Creates a more dynamic fire but requires more GPU power. Dark and Smutty Default Fire Donate Doom Energize A Energize B Fire Flash Color Glow Color Gold Sponsors Gradient Gravity Hell Fire Hexagon Horizontal Scale If disabled, a random location will be chosen. If multiple are selected, one is chosen randomly. Incinerate Line Color Matrix Mesh Line Width Noise Past Sponsors Pixel Size Pixel Wheel Pixel Wipe Pixelate Presets Preview for %s Randomness Report a Bug Reset to Default Value Santa is Coming Scale Shake Intensity Shatter From Pointer Location Silver Sponsors Snap of Disintegration Speed Spoke Count Start at Pointer Suction Intensity T-Rex Attack TV Effect The shards will fly away from where your mouse pointer was when closing the window. Tip Color Trail Color Translate Turbulence Twirl Intensity Vertical Overshooting Vertical Scale View Changelog Visit Homepage Warp Effect Wisps You may want to enable this for dark window themes. Project-Id-Version: burn-my-windows
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2022-09-11 10:17+0000
Last-Translator: Jose Riha <jose1711@gmail.com>
Language-Team: Slovak <https://hosted.weblate.org/projects/burn-my-windows/core/sk/>
Language: sk
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=3; plural=(n==1) ? 0 : (n>=2 && n<=4) ? 1 : 2;
X-Generator: Weblate 4.14.1-dev
 3D šum O Burn-My-Windows Prídavné miešanie Trvanie animácie [ms] Prízrak Sila úderu Črepiny Bronzoví sponzori Vyberte predvoľbu Počet škrabancov Veľkosť škrabancov od pazúrov Zatvorte toto okno, ak chcete vidieť efekt! Studený vánok Farba Vytvoriť efekt Vytvorí dynamickejší efekt ohňa, ale vyžaduje na to viac GPU. Tmavý a zafúľaný Predvolený oheň Prispieť Doom Nabudenie A Nabudenie B Oheň Farba blesku Farba žiary Zlatí sponzori Prechod Gravitácia Pekelný oheň Šesťhran Vodorovná mierka Pri vypnutí bude vybrané náhodné miesto. Ak ich vyberiete viac, budú sa náhodne striedať. Spálenie Farba čiar Matrix Šírka čiar v sieti Šum Bývalí sponzori Veľkosť štvorčeka Kolečko so štvorčekmi Zotrenie štvorčekmi Štvorčeky Predvoľby Náhľad %s Náhodnosť Nahlásiť chybu Obnoviť predvolené hodnoty Santa prichádza Priblíženie Intenzita otrasov Roztrieštenie od polohy kurzora Strieborní sponzori Thanosove lusknutie Rýchlosť Počet lúčov Začať od kurzora Intenzita odsatia Tyrannosaurus Televízor Črepiny odletia z miesta, kde bol kurzor myši pri zatváraní okna. Farba špicu Farba chvosta Preložiť Turbulencia Intenzita vírenia Zvislé prestrelenie Vodorovná mierka Zobraziť históriu zmien Navštíviť webovú stránku Efekt zakrivenia Obláčiky Možno túto voľbu budete chcieť zapnúť pri tmavých motívoch. 