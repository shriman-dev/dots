��    F      L  a   |                 
        
   4  
   ?     J     W     g     w     �  (   �     �     �     �  8   �     "     2     ?     G     N  
   S  
   ^     i     n     z     �     �     �     �  	   �     �  .   �  1   �  
        $     +  
   9     D  
   P     [     d     k     s     �  
   �     �     �     �     �     �     �     	     	     	     5	     ;	     L	  	   Y	  S   c	     �	  	   �	     �	  	   �	  
   �	     �	     �	     
     
     '
  �  -
     �     �     �  
          
   )     4     D     Q     ^  (   j     �     �  	   �  @   �     �     �          	               %     5     9     ?     H     O     \  	   d     n  	   }  1   �  =   �     �     �               "     .  	   ?     I     ]     d     q     �     �     �     �     �     �     �          
       	   ,     6     P     _  P   g     �     �     �     �  
   �     �               (     6         +          "          ;   ,                 >      0   <   9   D   4           3       2   =   .              ?   
              )          @   7       C   #            /   $   !          6       E                 A   (      '                 F                     :                              8   %   *   1             5      	      B       -   &    3D Noise About Burn-My-Windows Animation Time [ms] Apparition Blow Force Broken Glass Bronze Sponsors Choose a Preset Claw Scratch Count Claw Scratch Scale Close this Window to Preview the Effect! Cold Breeze Color Create an Effect Creates a more dynamic fire but requires more GPU power. Dark and Smutty Default Fire Details Donate Doom Energize A Energize B Fire Flash Color Glide Glitch Gold Sponsors Gradient Gravity Hell Fire Hexagon If disabled, a random location will be chosen. If multiple are selected, one is chosen randomly. Incinerate Matrix Past Sponsors Pixel Size Pixel Wheel Pixel Wipe Pixelate Portal Presets Preview for %s Preview this effect Randomness Report a Bug Reset to Default Value Rotation Speed Santa is Coming Scale Shatter From Pointer Location Shift Silver Sponsors Snap of Disintegration Speed Start at Pointer T-Rex Attack TV Effect The shards will fly away from where your mouse pointer was when closing the window. Tilt Tip Color Trail Color Translate Turbulence Vertical Overshooting View Changelog Visit Homepage Warp Effect Wisps Project-Id-Version: burn-my-windows
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2023-01-09 16:48+0000
Last-Translator: Sir-Ivysaur <randompsyduck@duck.com>
Language-Team: Indonesian <https://hosted.weblate.org/projects/burn-my-windows/core/id/>
Language: id
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=1; plural=0;
X-Generator: Weblate 4.15.1-dev
 Kebisingan 3D Tentang Burn-My-Windows Panjang Animasi [milidetik] Penyedotan Kekuatan Pecahan Kaca Pecah Sponsor Tembaga Pilih Preset Jumlah Cakar Skala Cakar Tutup jendela ini untuk melihat efeknya! Angin Sejuk Warna Buat Efek Menciptakan api lebih dinamis tapi membutuhkan tenaga GPU lebih. Redup Api (Bawaan) Detil Donasi Doom Memberdayakan A Memberdayakan B Api Warna Meluncur Glitch Sponsor Emas Gradien Gravitasi Api Amat Panas Segi Enam Jiki dimatikan, akan terpilih lokasi secara acak. Jika lebih dari satu terpilih, satu akan dipilih secara acak. Bakar Matriks Mantan Sponsor Ukuran Piksel Roda Piksel Gelombang Piksel Pikselasi Pintu Gerbang Ajaib Preset Pratinjau %s Pratinjau efek ini Keserampangan Laporkan Bug Kembalikan ke Pengaturan Awal Kecepatan Rotasi Sinterklas Datang Skala Pecahkan dari Lokasi Pointer Pindah Sponsor Perak Jentikan Kehancuran Kecepatan Bakar dari Lokasi Pointer Serangan T-Rex Efek TV Pecahan kaca akan terbang menjauh dari lokasi pointer anda saat menutup jendela. Miring Warna Ujung Warna Jejak Terjemahkan Pergolakan Lewat Batas Vertikal Jendela Lihat Perubahan Buka Website Efek Distorsi Cahaya 