��    C      4  Y   L      �  :   �  B   �  ,   /  1   \     �     �     �     �     �  5   �          #     8     >     P  $   b     �     �     �     �     �     �     �     �     �  	   �            b   #     �  #   �  $   �     �     �  
   �  2   �     -	     0	  "   5	     X	     d	     �	     �	  
   �	     �	     �	  	   �	     �	     �	  
   �	     �	  G   �	  
   5
     @
     I
  #   [
     
  "   �
     �
  T   �
  �     �   �     9     >     F     X  �  q  H     S   ^  .   �  3   �          *     =     R     _  F   y     �     �     �     �     �  '        7     ?     H     Z     q     �     �     �     �     �     �     �  ~   �     q  '   u     �     �     �     �  -   �                %     F     U     n  
   �  
   �     �     �     �     �     �     �      �  G   �     6     E     T  $   f     �  %   �     �  h   �  d   ;  �   �     9  	   @     J  &   d     ?   <      "      8       !   3      5               >           4                /   0         )       '       =   A      +      #          &   B      C      @                    ;   -   .   :      	      $           
   (          ,          %   6   *                     2   1            9             7                   
<b>Enable option, "Allow Executing File as a Program"</b> 
<b>Set Permissions, in "Others Access", "Read Only" or "None"</b> "${programName}" is needed for Desktop Icons Add new drives to the opposite side of the screen Allow Launching Arrange Icons Broken Desktop File Broken Link Can not email a Directory Can not open this File because it is a Broken Symlink Cancel Change Background… Close Command not found Common Properties Compress {0} file Compress {0} files Copy Cut Delete permanently Display Settings Don't Allow Launching Eject Empty Trash Extract Here Extract To... File name Find Files on Desktop Folder name For this functionality to work in Desktop Icons, you must install "${programName}" in your system. Home Invalid Permissions on Desktop File Launch using Dedicated Graphics Card Move to Trash New Document New Folder New Folder with {0} item New Folder with {0} items OK Open Open All With Other Application... Open All... Open With Other Application Open in Terminal Paste Properties Redo Rename Rename… Scripts Select Select All Select Extract Destination Selection includes a Directory, compress the directory to a file first. Send to... Settings Show All in Files Show external drives in the desktop Show in Files Show network drives in the desktop Stack This Type This .desktop File has incorrect Permissions. Right Click to edit Properties, then:
 This .desktop file has errors or points to a program without permissions. It can not be executed.

	<b>Edit the file to set the correct executable Program.</b> This .desktop file is not trusted, it can not be launched. To enable launching, right-click, then:

<b>Enable "Allow Launching"</b> Undo Unmount Unstack This Type Use Nemo to open folders Project-Id-Version: gtk4-ding
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2023-03-05 15:42+0000
Last-Translator: Anonymous <noreply@weblate.org>
Language-Team: Swedish <https://hosted.weblate.org/projects/gtk4-desktop-icons-ng/gtk4-ding-pot/sv/>
Language: sv
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: Weblate 4.16.2-dev
 
<b>Aktivera alternativet, "Tillåt att filen körs som ett program"</b> 
<b>Ställ in rättigheter, i "Andras åtkomst", "Endast läsbar" eller "Inget"</b> "${programName}" behövs för skrivbordsikoner Lägg till nya enheter på motsatt sida av skärmen Tillåt programstart Arrangera ikonerna Trasig skrivbordsfil Trasig länk Kan inte maila en katalog Kan inte öppna den här filen eftersom att det är en trasig symlänk Avbryt Ändra bakgrund… Stäng Kommandot hittades inte Gemensamma egenskaper Komprimera {0} fil Komprimera {0} filer Kopiera Klipp ut Ta bort permanent Visningsinställningar Tillåt ej programstart Mata ut Töm papperskorgen Extrahera här Extrahera till… Filnamn Hitta filer på skrivbordet Mappnamn För att få den här funktionaliteten att fungera i skrivbordsikoner så måste du installera "${programName}" i ditt system. Hem Felatiga rättigheter i skrivbordsfilen Starta med dedikerat grafikkort Flytta till papperskorgen Nytt dokument Ny mapp Ny mapp med {0} objekt Ny mapp med {0} objekt OK Öppna Öppna alla med annat program… Öppna alla… Öppna med annat program Öppna i terminal Klistra in Egenskaper Gör om Byt namn Byt namn… Skript Välj Markera alla Välj målplats för extrahering Valet innehåller en katalog,  komprimera katalogen till en fil först. Skicka till… Inställningar Visa alla i Filer Visa externa enheter på skrivbordet Visa i Filer Visa nätverksenheter på skrivbordet Arrangera denna typ Den här .desktop-filen har inte korrekta rättigheter. Högerklicka för att redigera Egenskaper, sen:
 Den här .desktop-filen har fel eller pekar till ett program utan rättigheter. Den kan inte köras. Den här .desktop-filen är inte pålitlig, den kan inte startas. För att göra den startbar, högerklicka, därefter:

<b>Aktivera "Tillåt start"</b> Ångra Avmontera Sluta arrangera denna typ Använd Nemo för att öppna kataloger 