��    &      L  5   |      P     Q     a     h     }     �     �     �     �     �     �     �     �     �     �     �     �  
                       3     D  
   J     U  	   Z     d     l  
   s     ~     �  '   �  "   �     �     �          
       �       �  	   �     �  	   �                    ,  	   ?     I     Z     i     {     �     �     �  
   �     �     �     �     �               !     )     6  
   >     I     [     h  *   {  .   �  "   �     �  	   �     	     	     #      &                                       	             $   
      %                                                                                      "             !       Allow Launching Cancel Change Background… Copy Create Cut Delete permanently Display Settings Eject Empty Trash Extract Here Extract To... Home Large Move to Trash New Document New Folder OK Open Open With Other Application Open in Terminal Paste Properties Redo Rename… Scripts Select Send to... Settings Show in Files Show the personal folder in the desktop Show the trash icon in the desktop Size for the desktop icons Small Standard Undo Unmount Project-Id-Version: gtk4-ding
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2023-03-05 15:42+0000
Last-Translator: Anonymous <noreply@weblate.org>
Language-Team: Dutch <https://hosted.weblate.org/projects/gtk4-desktop-icons-ng/gtk4-ding-pot/nl/>
Language: nl
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: Weblate 4.16.2-dev
 Toepassingen starten toestaan Annuleren Achtergrond aanpassen… Kopiëren Aanmaken Knippen Definitief verwijderen Scherminstellingen Uitwerpen Prullenbak legen Hier uitpakken Uitpakken naar… Persoonlijke map Groot Verplaatsen naar prullenbak Nieuw document Nieuwe map Oké Openen Met andere toepassing openen Openen in terminalvenster Plakken Eigenschappen Opnieuw Hernoemen… Scripts Selecteren Versturen naar… Instellingen Tonen in Bestanden Toon de persoonlijke map op het bureaublad Toon het prullenbakpictogram op het bureaublad Grootte van bureaubladpictogrammen Klein Standaard Ongedaan maken Ontkoppelen 