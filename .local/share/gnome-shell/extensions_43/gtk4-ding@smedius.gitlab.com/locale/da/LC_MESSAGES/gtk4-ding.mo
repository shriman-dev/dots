��    &      L  5   |      P     Q     a     h     }     �     �     �     �     �     �     �     �     �     �     �     �  
                       3     D  
   J     U  	   Z     d     l  
   s     ~     �  '   �  "   �     �     �          
       �       �  	   �     �     �     �     �     �               $  
   5     @     M     R     X     m     z     �     �     �     �     �  
   �     �  
   �  
   �     �     �     �       )     %   <  !   b     �     �     �  	   �     #      &                                       	             $   
      %                                                                                      "             !       Allow Launching Cancel Change Background… Copy Create Cut Delete permanently Display Settings Eject Empty Trash Extract Here Extract To... Home Large Move to Trash New Document New Folder OK Open Open With Other Application Open in Terminal Paste Properties Redo Rename… Scripts Select Send to... Settings Show in Files Show the personal folder in the desktop Show the trash icon in the desktop Size for the desktop icons Small Standard Undo Unmount Project-Id-Version: gtk4-ding
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2023-03-05 15:42+0000
Last-Translator: Anonymous <noreply@weblate.org>
Language-Team: Danish <https://hosted.weblate.org/projects/gtk4-desktop-icons-ng/gtk4-ding-pot/da/>
Language: da
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: Weblate 4.16.2-dev
 Tillad opstart Annullér Skift baggrund … Kopiér Opret Klip Slet permanent Skærmindstillinger Skub ud Tøm papirkurven Pak ud her Pak ud i … Hjem Store Flyt til papirkurven Nyt dokument Ny mappe OK Åbn Åbn med et andet program Åbn i terminal Indsæt Egenskaber Omgør Omdøb … Programmer Vælg Send til … Indstillinger Vis i Filer Vis den personlige mappe på skrivebordet Vis papirkurvsikonet på skrivebordet Størrelsen på skrivebordsikoner Små Standard Fortryd Afmontér 