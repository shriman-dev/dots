��    Q      �  m   ,      �  6   �  1        J     Z     a     o     ~     �     �     �     �     �     �     �       $        D     I     P     T     g     x     �     �     �     �     �     �  	   �     �     	  +   	     @	     E	     K	  $   [	     �	     �	     �	     �	  
   �	  2   �	     �	     
     
  "   
     /
     ;
     W
     h
  
   n
     y
     ~
  	   �
     �
     �
  
   �
     �
  G   �
  
                   >  .   P  #        �     �     �  "   �  '   �  "   $     G     T     o     u     ~     �     �     �     �  �  �  <   X  5   �     �     �     �               /  '   C  	   k     u  )   �     �     �     �  ,   �          %     .     3     E  "   U     x     �     �     �     �     �     �     �  
   �  N        V     ]     b  (   v     �     �  
   �     �     �  2   �     '     ?     B  #   K     o          �     �     �     �     �     �  	   �     �       (     D   ;     �     �     �     �  7   �  %        '     A     Z  '   n  &   �  !   �  
   �     �            	             3  	   G     Q                  $      N              
   4       G   @                    	   9   P   K      "           ?              5   -          B   !                   0   +   1      %   A   7   ;   I      L   /                      6               8                    D       H      #       ,   (      :      =   '   *         .      M   &       Q   <      2      O      F   J   )      3   C       >      E        Action to do when launching a program from the desktop Add new drives to the opposite side of the screen Allow Launching Always Arrange Icons Ask what to do Bottom-left corner Bottom-right corner Can not email a Directory Cancel Change Background… Click type for open files Close Command not found Common Properties Compress {0} file Compress {0} files Copy Create Cut Delete permanently Display Settings Display the content of the file Don't Allow Launching Double click Eject Empty Trash Extract Here Extract To... File name Find Files on Desktop Folder name Highlight the drop place during Drag'n'Drop Home Large Launch the file Launch using Dedicated Graphics Card Local files only Move to Trash Never New Document New Folder New Folder with {0} item New Folder with {0} items New icons alignment OK Open Open All With Other Application... Open All... Open With Other Application Open in Terminal Paste Properties Redo Rename Rename… Scripts Select Select All Select Extract Destination Selection includes a Directory, compress the directory to a file first. Send to... Settings Settings shared with Nautilus Show All in Files Show a context menu item to delete permanently Show external drives in the desktop Show hidden files Show image thumbnails Show in Files Show network drives in the desktop Show the personal folder in the desktop Show the trash icon in the desktop Single click Size for the desktop icons Small Standard Tiny Top-left corner Top-right corner Undo Unmount Project-Id-Version: gtk4-ding
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2023-03-05 15:42+0000
Last-Translator: Anonymous <noreply@weblate.org>
Language-Team: Romanian <https://hosted.weblate.org/projects/gtk4-desktop-icons-ng/gtk4-ding-pot/ro/>
Language: ro
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
X-Generator: Weblate 4.16.2-dev
 Acțiuni de făcut la deschiderea unui program de pe desktop Adaugă unitățile noi în colțul opus al ecranului Permite lansarea Întotdeauna Aranjează pictogramele Întreabă ce e de făcut Colțul stânga-jos Colțul dreapta-jos Nu se poate trimite un dosar prin email Anulează Schimbă fundalul... Tipul de click pentru deschidere fișiere Închide Comanda nu a fost găsită Proprietăți generale Comprimă {0} fișier Comprimă {0} fișiere Copiază Creează Taie Șterge definitiv Setări afișaj Afișează conținutul fișierului Nu permite lansarea Dublu click Scoate Golește coșul de gunoi Extrage aici Extrage în... Nume fișier Caută fișiere pe Desktop Nume dosar Evidențiază punctul de plasare în timpul operațiunii de trage si plasează Acasă Mari Lansează fișierul Lanseaza folosind Placă Video Dedicată Doar fișierele locale Mută la coșul de gunoi Niciodată Document nou Director nou Dosar nou cu {0} element Dosar nou cu {0} elemente Aliniere pictograme noi Ok Deschide Deschide tot cu altă aplicație... Deschide tot... Deschide cu altă aplicație Deschide în terminal Lipește Proprietăți Refă Redenumește Redenumește… Scripturi Selectează Selectează tot Selectează destinația pentru extragere Selecția include un dosar, mai întâi arhiveză-l într-un fișier Trimite la... Setări Setări comune cu Nautilus Arată toate fișierele Arată un meniu de context pentru ștergere definitivă Arată unitățile externe pe desktop Arată fișierele ascunse Arată miniaturi imagini Arată în fișiere Arată unitățile de rețea pe desktop Afișează dosarul personal pe desktop Arată coșul de gunoi pe desktop Click unic Dimensiune pictograme desktop Mici Normale Minuscule Colțul stânga-sus Colțul dreapta-sus Anulează Demontează 