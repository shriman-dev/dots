��    &      L  5   |      P     Q     a     h     }     �     �     �     �     �     �     �     �     �     �     �     �     	  
        !     $     )     E     V  
   \     g  	   l     v  
   }     �     �  '   �  "   �     �                        !     ?     R     [     q     �     �     �     �     �     �     �     �     �     �                 	   ,     6     =     D     `     s     |     �     �     �     �     �     �  (   �  (   �      	     @	  
   E	     P	  
   Y	     #      &                                       
             $         %                                                                                      "          	   !       Allow Launching Cancel Change Background… Command not found Copy Create Cut Delete permanently Display Settings Eject Empty Trash Extract Here Extract To... Home Large Move to Trash New Document New Folder OK Open Open With Other Application Open in Terminal Paste Properties Redo Rename… Select Send to... Settings Show in Files Show the personal folder in the desktop Show the trash icon in the desktop Size for the desktop icons Small Standard Undo Unmount Project-Id-Version: gtk4-ding
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2023-03-05 15:42+0000
Last-Translator: Anonymous <noreply@weblate.org>
Language-Team: Croatian <https://hosted.weblate.org/projects/gtk4-desktop-icons-ng/gtk4-ding-pot/hr/>
Language: hr
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=3; plural=n%10==1 && n%100!=11 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2;
X-Generator: Weblate 4.16.2-dev
X-Launchpad-Export-Date: 2019-03-27 09:36+0000
 Dopusti pokretanje Odustani Promijeni pozadinu… Naredba nije pronađena Kopiraj Stvori Izreži Obriši trajno Postavke zaslona Izbaci Isprazni smeće Raspakiraj ovdje Raspakiraj u… Osobna mapa Velike Premjesti u smeće Novi dokument Nova mapa U redu Otvori Otvori s drugom aplikacijom Otvori u Terminalu Zalijepi Svojstva Ponovi Preimenuj… Odaberi Pošalji u… Postavke Prikaži u Datotekama Prikaži osobnu mapu na radnoj površini Prikaži mapu smeća na radnoj površini Veličina ikona radne površine Male Standardne Poništi Odmontiraj 