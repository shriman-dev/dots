��    C      4  Y   L      �  6   �  1   �          *     1     ?     N     a     u     �     �     �     �     �  $   �                         %     6     V     c     i     u     �  +   �     �     �     �  $   �     �               !  
   .  2   9     l     o     t     �     �     �  
   �     �  	   �     �     �  G   �  
   8	     C	     L	  .   j	     �	     �	  '   �	  "   �	     
     
     0
     6
     ?
     D
     T
     e
     j
  �  r
  7   '  1   _     �     �     �     �     �     �  0   �     /     8  "   R     u     |  ,   �     �     �     �     �     �          ,     9     @     Q     ^  A   n     �     �     �  %   �     �          *     0  
   ?  0   J     {     ~     �     �     �     �     �     �     �     �      �  L        ]     l  +   |  @   �     �     �  ,     0   G     x  ,   �     �     �     �     �     �        	   	         >      "         5          /   2   :   6          A       1       $                      )   @   (                   +          8       '   #                          B      9   -   .                 %                         ,       !   &   3   *   ?   7          ;   
   0            C          =   	   4      <       Action to do when launching a program from the desktop Add new drives to the opposite side of the screen Allow Launching Always Arrange Icons Ask what to do Bottom-left corner Bottom-right corner Can not email a Directory Cancel Change Background… Click type for open files Close Command not found Compress {0} file Compress {0} files Copy Create Cut Delete permanently Display Settings Display the content of the file Double click Eject Empty Trash Extract Here Extract To... Highlight the drop place during Drag'n'Drop Home Large Launch the file Launch using Dedicated Graphics Card Local files only Move to Trash Never New Document New Folder New Folder with {0} item New Folder with {0} items OK Open Open All... Open With Other Application Open in Terminal Paste Properties Redo Rename… Scripts Select Extract Destination Selection includes a Directory, compress the directory to a file first. Send to... Settings Settings shared with Nautilus Show a context menu item to delete permanently Show image thumbnails Show in Files Show the personal folder in the desktop Show the trash icon in the desktop Single click Size for the desktop icons Small Standard Tiny Top-left corner Top-right corner Undo Unmount Project-Id-Version: gtk4-ding
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2023-03-05 15:42+0000
Last-Translator: Anonymous <noreply@weblate.org>
Language-Team: Portuguese (Brazil) <https://hosted.weblate.org/projects/gtk4-desktop-icons-ng/gtk4-ding-pot/pt_BR/>
Language: pt_BR
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n > 1;
X-Generator: Weblate 4.16.2-dev
 O que fazer ao clicar num programa na área de trabalho Mostrar novos dispositivos no lado oposto da tela Permitir iniciar Sempre Ordenar Ícones Perguntar o que fazer Canto inferior esquerdo Canto inferior direito Não é possível enviar um diretório por email Cancelar Alterar plano de fundo… Tipo de clique para abrir arquivos Fechar Comando não encontrado Comprimir {0} arquivo Comprimir {0} arquivos Copiar Criar Recortar Excluir permanentemente Configurações de exibição Mostrar o conteúdo do arquivo Clique duplo Ejetar Esvaziar lixeira Extrair aqui Extrair para… Destacar o local onde o ícone será alocado ao arrastar e soltar Pasta pessoal Grande Executar o arquivo Abrir usando placa de vídeo dedicada Apenas arquivos locais Mover para a lixeira Nunca Novo documento Nova pasta Nova pasta com {0} item Nova pasta com {0} itens OK Abrir Abrir todos... Abrir com outro aplicativo Abrir no terminal Colar Propriedades Refazer Renomear… Scripts Selecionar destino da extração Seleção inclui um diretório, comprima o diretório em um arquivo primeiro Enviar para... Configurações Configurações compartilhadas com Nautilus Mostrar a opção de excluir permanentemente no menu de contexto Mostrar miniaturas Mostrar no gestor de arquivos Mostrar a pasta pessoal na área de trabalho Mostrar o ícone da lixeira na área de trabalho Clique único Tamanho para os ícones da área de trabalho Pequeno Padrão Muito pequeno Canto superior esquerdo Canto superior direito Desfazer Desmontar 