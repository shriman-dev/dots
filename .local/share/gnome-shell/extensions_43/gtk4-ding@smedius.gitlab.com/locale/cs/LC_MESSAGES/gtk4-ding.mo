��    Q      �  m   ,      �  6   �  1        J     Z     a     o     ~     �     �     �     �     �     �     �       $        D     I     P     T     g     x     �     �     �     �     �     �  	   �     �     	  +   	     @	     E	     K	  $   [	     �	     �	     �	     �	  
   �	  2   �	     �	     
     
  "   
     /
     ;
     W
     h
  
   n
     y
     ~
  	   �
     �
     �
  
   �
     �
  G   �
  
                   >  .   P  #        �     �     �  "   �  '   �  "   $     G     T     o     u     ~     �     �     �     �  �  �  >   p  /   �     �     �     �          &     8  !   K     m     u  1   �     �     �  
   �  J   �  
   0  	   ;     E     M     _     s     �     �     �     �     �     �     �     �       ;   "     ^     p     w  +   �     �     �     �     �     �  [        `     y  	   �  "   �     �      �     �     �  
   �     
               -     5     A     M  M   k  	   �     �     �     �  C     *   [     �     �     �  4   �  .     *   6  
   a  !   l     �     �     �     �     �     �     �                  $      N              
   4       G   @                    	   9   P   K      "           ?              5   -          B   !                   0   +   1      %   A   7   ;   I      L   /                      6               8                    D       H      #       ,   (      :      =   '   *         .      M   &       Q   <      2      O      F   J   )      3   C       >      E        Action to do when launching a program from the desktop Add new drives to the opposite side of the screen Allow Launching Always Arrange Icons Ask what to do Bottom-left corner Bottom-right corner Can not email a Directory Cancel Change Background… Click type for open files Close Command not found Common Properties Compress {0} file Compress {0} files Copy Create Cut Delete permanently Display Settings Display the content of the file Don't Allow Launching Double click Eject Empty Trash Extract Here Extract To... File name Find Files on Desktop Folder name Highlight the drop place during Drag'n'Drop Home Large Launch the file Launch using Dedicated Graphics Card Local files only Move to Trash Never New Document New Folder New Folder with {0} item New Folder with {0} items New icons alignment OK Open Open All With Other Application... Open All... Open With Other Application Open in Terminal Paste Properties Redo Rename Rename… Scripts Select Select All Select Extract Destination Selection includes a Directory, compress the directory to a file first. Send to... Settings Settings shared with Nautilus Show All in Files Show a context menu item to delete permanently Show external drives in the desktop Show hidden files Show image thumbnails Show in Files Show network drives in the desktop Show the personal folder in the desktop Show the trash icon in the desktop Single click Size for the desktop icons Small Standard Tiny Top-left corner Top-right corner Undo Unmount Project-Id-Version: gtk4-ding
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2023-03-05 15:42+0000
Last-Translator: Anonymous <noreply@weblate.org>
Language-Team: Czech <https://hosted.weblate.org/projects/gtk4-desktop-icons-ng/gtk4-ding-pot/cs/>
Language: cs
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=3; plural=(n==1) ? 0 : (n>=2 && n<=4) ? 1 : 2;
X-Generator: Weblate 4.16.2-dev
 Co se má stát když spouštíte spustitelný soubor z plochy Dávat nové disky na opačnou stranu obrazovky Povolit spouštění Vždy Uspořádat Ikony Zeptat se co se má stát Spodní levý roh Spodní pravý roh Nemůžete poslat složku emailem Zrušit Změnit pozadí… Typ kliknutí požadovaný pro otevření souboru Zavřít Příkaz nebyl nalezen Vlastnosti Zkomprimovat {0} soubor Zkomprimovat {0} soubory Zkomprimovat {0} souborů Kopírovat Vytvořit Vyjmout Odstranit nadobro Zobrazit nastavení Zobrazit obsah souboru Nepovolit spouštění Dvojklik Vysunout Vyprázdnit koš Rozbalit sem Rozbalit do… Přejmenovat soubor Najít soubory na Ploše Název nové složky Zvýraznit místo k položení při přetahování souborů Domovská Složka Velké Spustit soubor Spustit pomocí dedikované grafické karty Pouze pro místní soubory Přesunout do koše Nikdy Nový Dokument Nová složka Nová složka s {0} položkou Nová složka s {0} položkami Nová složka s {0} položkami Zarovnání nových ikon Budiž Otevřít Otevřít vše v jiné aplikaci... Otevřít vše... Otevřít pomocí jiné aplikace Otevřít v terminálu Vložit Vlastnosti Znovu Přejmenovat Přejmenovat… Skripty Vybrat vše Vybrat vše Vybrat místo k extrahování Výběr obsahuje Složku, nejdříve musíte zkomprimovat složku do souboru. Poslat... Nastavení ikon na ploše Nastavení sdílená s Nautilem Zobrazit vše v Souborech Zobrazit položku kontextové nabídky, kterou chcete trvale smazat Zobrazovat disky připojené k počítači Zobrazit skryté soubory Zobrazit náhledové obrázky Zobrazit v Souborech Zobrazovat síťové disky připojené k počítači Zobrazovat osobní složku na pracovní ploše Zobrazovat ikonu koše na pracovní ploše Jeden klik Velikost ikon na pracovní ploše Malé Standardní Drobný Levý horní roh Pravý horní roh Zpět Odpojit 