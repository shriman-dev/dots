��    `        �         (  :   )  B   d  ,   �  6   �     	  1   '	     Y	     i	     p	     ~	     �	     �	     �	     �	     �	  5   �	     $
     +
     @
     Z
     `
     r
  $   �
     �
     �
     �
     �
     �
     �
                    +     8  	   F     P     f  b   r  +   �       #        *     0  $   @     e     v     �     �  
   �  2   �     �     �     �  "   �                <     M  
   S     ^     c  	   j     t     |  
   �     �  G   �  
   �     �          #  .   5  #   d     �     �     �  "   �  '   �  "   	     ,     9     T     Z     j  T   s  �   �  �   h     �     �                         1     J  �  g  ]   	  �   g  K   �  G   4  )   |  L   �     �               *     >     U     k     �  .   �  e   �     *     2  %   Q     w     ~     �  .   �     �     �     �     �          1     J     V     _     r          �  %   �     �  ~   �  A   S     �  .   �     �     �  '   �          5     L     S     d  F   t     �     �     �  )   �       !   !     C     V     ]     j     r     {     �     �     �  '   �  K   �     $     4  #   @     d  P   {  7   �          "     9  *   O  +   z  #   �     �     �     �     �       �     �   �  �   �  	   N     X     o     �  	   �     �  &   �  7   �     !   @   +   T   /   A   O             `             Y   [   .   L      #          ;   *       -   6      0       )   C                 
   Q       $              >   =   ,   F   U      ]   H   <            %   1       3       W       V   	   K   :                     J                         (         \       S       8   Z   7   X   E   P                  B   ^   _           I   '       2       4   N          M   G                         R   &         "              9   ?      5   D    
<b>Enable option, "Allow Executing File as a Program"</b> 
<b>Set Permissions, in "Others Access", "Read Only" or "None"</b> "${programName}" is needed for Desktop Icons Action to do when launching a program from the desktop Add an emblem to soft links Add new drives to the opposite side of the screen Allow Launching Always Arrange Icons Ask what to do Bottom-left corner Bottom-right corner Broken Desktop File Broken Link Can not email a Directory Can not open this File because it is a Broken Symlink Cancel Change Background… Click type for open files Close Command not found Common Properties Compress {0} file Compress {0} files Copy Cut Delete permanently Display Settings Display the content of the file Don't Allow Launching Double click Eject Empty Trash Extract Here Extract To... File name Find Files on Desktop Folder name For this functionality to work in Desktop Icons, you must install "${programName}" in your system. Highlight the drop place during Drag'n'Drop Home Invalid Permissions on Desktop File Large Launch the file Launch using Dedicated Graphics Card Local files only Move to Trash Never New Document New Folder New Folder with {0} item New Folder with {0} items New icons alignment OK Open Open All With Other Application... Open All... Open With Other Application Open in Terminal Paste Properties Redo Rename Rename… Scripts Select Select All Select Extract Destination Selection includes a Directory, compress the directory to a file first. Send to... Settings Settings shared with Nautilus Show All in Files Show a context menu item to delete permanently Show external drives in the desktop Show hidden files Show image thumbnails Show in Files Show network drives in the desktop Show the personal folder in the desktop Show the trash icon in the desktop Single click Size for the desktop icons Small Stack This Type Standard This .desktop File has incorrect Permissions. Right Click to edit Properties, then:
 This .desktop file has errors or points to a program without permissions. It can not be executed.

	<b>Edit the file to set the correct executable Program.</b> This .desktop file is not trusted, it can not be launched. To enable launching, right-click, then:

<b>Enable "Allow Launching"</b> Tiny Top-left corner Top-right corner Undo Unmount Unstack This Type Use Nemo to open folders Use dark text in icon labels Project-Id-Version: gtk4-ding
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2023-03-05 15:42+0000
Last-Translator: Anonymous <noreply@weblate.org>
Language-Team: French <https://hosted.weblate.org/projects/gtk4-desktop-icons-ng/gtk4-ding-pot/fr/>
Language: fr
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n > 1;
X-Generator: Weblate 4.16.2-dev
 
<b>- activez l’option, « Autoriser l’exécution du fichier comme un programme ».</b> 
<b>- définissez le droit d’accès dans les permissions pour « autres » à « lecture seule » ou « aucun » ;</b> L’extension Desktop Icons requiert l’application « ${programName} » Action à effectuer lors du lancement d’un programme depuis le bureau Ajouter un emblème aux liens symboliques Ajouter les nouveaux volumes de stockage sur le côté opposé de l’écran Autoriser l’exécution Toujours Ordonner les icônes Demander quoi faire Coin inférieur gauche Coin inférieur droit Fichier XDG Desktop incorrect Lien brisé Impossible d’envoyer un dossier par courriel Impossible d’ouvrir ce fichier car il s’agit d’un lien symbolique dont la cible n’existe plus Annuler Changer l’arrière‑plan… Type de clic pour ouvrir les fichiers Fermer Commande introuvable Préférences Compresser {0} fichier Compresser {0} fichiers Copier Couper Supprimer définitivement Paramètres d’affichage Afficher le contenu du fichier Interdire l’exécution Double clic Éjecter Vider la corbeille Extraire ici Extraire vers… Nom du fichier Rechercher des fichiers sur le bureau Nom du dossier Cette fonctionnalité de l’extension Desktop Icons nécessite l’installation de « ${programName} » sur votre système. Mettre en évidence la destination lors d’un glisser‑déposer Dossier personnel Permissions du fichier XDG Desktop incorrectes Grande Exécuter le fichier Lancer avec la carte graphique dédiée Fichiers locaux uniquement Mettre à la corbeille Jamais Nouveau document Nouveau dossier Nouveau dossier avec {0} élément Nouveau dossier avec {0} éléments Nouvel alignement des icônes Valider Ouvrir Tout ouvrir avec une autre application… Ouvrir tout… Ouvrir avec une autre application Ouvrir un terminal Coller Propriétés Refaire Renommer Renommer… Scripts Sélectionner Tout sélectionner Sélectionnez le dossier d’extraction La sélection contient un dossier, créez d’abord une archive du dossier. Envoyer vers… Paramètres Paramètres partagés avec Nautilus Afficher dans Fichiers Afficher un élément « Supprimer définitivement » dans le menu contextuel Afficher les volumes de stockage externes sur le bureau Afficher les fichiers cachés Afficher les imagettes Montrer dans Fichiers Afficher les volumes réseau sur le bureau Afficher le dossier personnel sur le bureau Afficher la corbeille sur le bureau Simple clic Taille des icônes du bureau Petite Regrouper ce type Normale Ce fichier XDG Desktop a des permissions incorrectes. Veuillez modifier ses propriétés en cliquant sur le bouton droit, puis :
 Ce fichier XDG Desktop contient des erreurs ou pointe vers un programme n’ayant pas la permission d’être exécuté.

	<b>Veuillez corriger le nom et le chemin d’accès du programme à exécuter qui est indiqué dans ce fichier XDG Desktop.</b> Ce fichier XDG Desktop n’a pas l’autorisation d’être exécuté. Afin de le rendre exécutable, cliquez sur le bouton droit, puis :

<b>Activez « Autoriser l’exécution »</b> Minuscule Coin supérieur gauche Coin supérieur droit Annuler Démonter Dégrouper ce type Utiliser Nemo pour ouvrir les dossiers Utiliser un texte sombre pour les libellés des icônes 