��    W      �     �      �  6   �     �  1   �               %     3     B     U     i     }     �  5   �     �     �     �     	     	     '	  $   9	     ^	     c	     g	     z	     �	     �	     �	     �	     �	     �	     �	  	   �	     
     
  +   '
     S
  #   X
     |
     �
  $   �
     �
     �
     �
     �
  
   �
  2   �
     '     ;     >  "   C     f     r     �     �  
   �     �     �  	   �     �     �  
   �     �  G   �  
   C     N     W     u  .   �  #   �     �     �       "     '   3  "   [     ~     �     �     �     �     �     �     �     �     �       �    7   �     �  1        E  	   [     e     w     �     �     �     �      �  B   �     >     E     _     w     }     �     �     �     �     �     �     �     �          "     *     :     I  
   \     g     �  7   �     �  %   �     �     �  )   	     3     L     Z  
   _     j     w     �     �     �  #   �     �     �     �               %     ,     >     S     \     a     p  E   �  
   �     �  "   �       1   #  *   U     �     �     �  )   �  (   �  $        D     Q     m     v          �     �     �     �  $   �  %   �            +          M   K            .               $      U   R   G   I   <       7       =   V   "   6   Q       N       2      :                  L                             T         '   	   %       J   @       8   !   4   5   /   A   H   E      *      1   (   
   B              >      ?   C          S                         3      0   9   )           ;                 P   &         O       W              -               #       ,   D              F                  Action to do when launching a program from the desktop Add an emblem to soft links Add new drives to the opposite side of the screen Allow Launching Always Arrange Icons Ask what to do Bottom-left corner Bottom-right corner Broken Desktop File Broken Link Can not email a Directory Can not open this File because it is a Broken Symlink Cancel Change Background… Click type for open files Close Command not found Common Properties Compress {0} file Compress {0} files Copy Cut Delete permanently Display Settings Display the content of the file Don't Allow Launching Double click Eject Empty Trash Extract Here Extract To... File name Find Files on Desktop Folder name Highlight the drop place during Drag'n'Drop Home Invalid Permissions on Desktop File Large Launch the file Launch using Dedicated Graphics Card Local files only Move to Trash Never New Document New Folder New Folder with {0} item New Folder with {0} items New icons alignment OK Open Open All With Other Application... Open All... Open With Other Application Open in Terminal Paste Properties Redo Rename Rename… Scripts Select Select All Select Extract Destination Selection includes a Directory, compress the directory to a file first. Send to... Settings Settings shared with Nautilus Show All in Files Show a context menu item to delete permanently Show external drives in the desktop Show hidden files Show image thumbnails Show in Files Show network drives in the desktop Show the personal folder in the desktop Show the trash icon in the desktop Single click Size for the desktop icons Small Standard Tiny Top-left corner Top-right corner Undo Unmount Use Nemo to open folders Use dark text in icon labels Project-Id-Version: gtk4-ding
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2023-03-05 15:42+0000
Last-Translator: Anonymous <noreply@weblate.org>
Language-Team: Turkish <https://hosted.weblate.org/projects/gtk4-desktop-icons-ng/gtk4-ding-pot/tr/>
Language: tr
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=1; plural=0;
X-Generator: Weblate 4.16.2-dev
 Masaüstünden program çalıştırırken ne yapılsın Yumuşak linklere amblem ekle Ekranın karşı tarafına yeni sürücüler ekle Başlatmaya İzin Ver Her zaman Simgeleri Sırala Ne yapılacağını sor Sol alt köşe Sağ alt köşe Kırık Masaüstünü Dosyası Kırık Link Bir dizine e-posta gönderilemez Bozuk bir Sembolik Bağlantı olduğu için bu Dosya açılamıyor İptal Arka Planı Değiştir… Dosyaları açma türü Kapat Komut Bulunamadı Ortak Özellikler {0} dosyayı sıkıştır Kopyala Kes Tamamen Sil Görüntü Ayarları İçeriği göster Başlatmaya İzin Verme Çift tıklama Çıkar Çöpü Boşalt Buraya Çıkar Şuraya Çıkar… Dosya adı Masaüstündeki Dosyaları Bul Klasör adı Sürükle ve Bırak sırasında bırakma yerini vurgula Ev Masaüstü Dosyası İzinleri Hatalı Büyük Çalıştır Özel Grafik Kartı kullanarak başlatın Yalnızca yerel dosyalar Çöpe Taşı Asla Yeni Belge Yeni Klasör {0} öge ile yeni klasör Yeni simge hizalaması Tamam Aç Tümünü Başka Uygulamayla Aç... Tümünü Aç... Başka Uygulamayla Aç Uçbirimde Aç Yapıştır Özellikler Yinele Yeniden Adlandır Yeniden Adlandır… Betikler Seç Tümünü Seç Çıkarma Hedefini Seç Seçim bir Dizin içeriyor, önce dizini bir dosyaya sıkıştırın. Gönder... Ayarlar Ayarlar Nautilus ile paylaşıldı Tümünü Dosyalarʼda Göster Tamamen silmek için sağ tıkta seçenek göster Harici sürücüleri masaüstünde göster Gizli dosyaları göster Resim önizlemelerini göster Dosyalarʼda Göster Ağ sürücülerini masaüstünde göster Kişisel klasörü masaüstünde göster Çöp kutusunu masaüstünde göster Tek tıklama Masaüstü simgeleri boyutu Küçük Standart Minik Sol üst köşe Sağ üst Köşe Geri Al Bağı Kaldır Klasörleri açmak için Nemo kullan Simge etiketlerinde koyu metin kullan 