��    X      �     �      �  ,   �  6   �  1   �     /     ?     F     T     c     v     �     �  5   �     �     �     	     	     "	     4	  $   F	     k	     p	     w	     {	     �	     �	     �	     �	     �	     �	     �	     
  	   
     
     /
  b   ;
  +   �
     �
     �
     �
  $   �
     
          )     /  
   <  2   G     z     �     �  "   �     �     �     �     �  
   �            	          
   !     ,  G   G  
   �     �     �     �  .   �  #        &     8     N  "   \  '     "   �     �     �     �     �       �        �     �     �     �     �     �     �  �    :   �  ?   '  -   g     �     �     �     �     �     �            H   :     �     �  %   �     �     �     �  <   �     -     5     =     D     Q     c     |     �     �     �     �     �     �     �     
  N     :   h     �     �     �  /   �     �          $     *     8  r   E     �     �     �  %   �       "        5     J     P     _     f     t     �     �  %   �  B   �       
     %          F  /   g  $   �     �     �      �  !        -     H     Y     p     �     �     �  �   �     l     t     �     �     �     �  #   �            +          L   J            .               %      V   S   F   H   <       7       O   X   "   6   R       M       2      :       $          K                             U         '   	   &       I   ?       8   !   4   5   /      G   D      *      1   (   
   A              =       >   B          T              Q          3      0   9   )          ;                 P   W         N                      -       @       #       ,   C               E                  "${programName}" is needed for Desktop Icons Action to do when launching a program from the desktop Add new drives to the opposite side of the screen Allow Launching Always Arrange Icons Ask what to do Bottom-left corner Bottom-right corner Broken Link Can not email a Directory Can not open this File because it is a Broken Symlink Cancel Change Background… Click type for open files Close Command not found Common Properties Compress {0} file Compress {0} files Copy Create Cut Delete permanently Display Settings Display the content of the file Don't Allow Launching Double click Eject Empty Trash Extract Here Extract To... File name Find Files on Desktop Folder name For this functionality to work in Desktop Icons, you must install "${programName}" in your system. Highlight the drop place during Drag'n'Drop Home Large Launch the file Launch using Dedicated Graphics Card Local files only Move to Trash Never New Document New Folder New Folder with {0} item New Folder with {0} items New icons alignment OK Open Open All With Other Application... Open All... Open With Other Application Open in Terminal Paste Properties Redo Rename Rename… Scripts Select All Select Extract Destination Selection includes a Directory, compress the directory to a file first. Send to... Settings Settings shared with Nautilus Show All in Files Show a context menu item to delete permanently Show external drives in the desktop Show hidden files Show image thumbnails Show in Files Show network drives in the desktop Show the personal folder in the desktop Show the trash icon in the desktop Single click Size for the desktop icons Small Stack This Type Standard This .desktop file has errors or points to a program without permissions. It can not be executed.

	<b>Edit the file to set the correct executable Program.</b> Tiny Top-left corner Top-right corner Undo Unmount Unstack This Type Use Nemo to open folders Project-Id-Version: gtk4-ding
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2023-03-05 15:42+0000
Last-Translator: Anonymous <noreply@weblate.org>
Language-Team: Polish <https://hosted.weblate.org/projects/gtk4-desktop-icons-ng/gtk4-ding-pot/pl/>
Language: pl
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=3; plural=n==1 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2;
X-Generator: Weblate 4.16.2-dev
 "${programName}" jest potrzebny do działania Ikon Pulpitu Czynność do wykonania podczas uruchamiania programu z pulpitu Dodaj nowe dyski po przeciwnej stronie ekranu Zezwól na uruchamianie Zawsze Rozmieść ikony Zapytaj co zrobić Do lewego dolnego rogu Do prawego dolnego rogu Wadliwy link (strót) Nie można wysłać Katalogu Nie można otworzyć pliku ponieważ jest to uszkodzony link symboliczny Anuluj Zmień tło… Wybierz sposób otwierania elementów Zamknij Nie odnaleziono polecenia Właściwości Kompresuj {0} plik Kompresuj {0} pliki Kompresuj {0} plików Skopiuj Utwórz Wytnij Usuń trwale Ustawienia ekranu Pokaż zawartość pliku Nie zezwalaj na uruchamianie Podwójne kliknięcie Wysuń Opróżnij kosz Rozpakuj tutaj Rozpakuj do… Nazwa pliku Znajdź pliki na Pulpicie Nazwa katalogu Dla tej funkcjonalności musisz zainstalować"${programName}" w swoim systemie Podświetl miejsce upuszczenia pliku podczas przeciągania Katalog domowy Duży Uruchom plik Uruchom za pomocą dedykowanej karty graficznej Tylko dla lokalnych plików Przenieś do kosza Nigdy Nowy dokument Nowy katalog Nowy folder zawierający {0} element Nowy folder zawierający {0} elementy Nowy folder zawierający {0} elementów Wyrównanie nowych ikon OK Otwórz Otwórz za pomocą innego programu... Otwórz wiele... Otwórz za pomocą innego programu Otwórz w terminalu Wklej Właściwości Ponów Zmień nazwę Zmień nazwę… Skrypty Zaznacz wszystko Wybierz miejsce rozpakowania archiwum Zaznaczenie zawiera Katalog, najpierw skompresuj Katalog do pliku. Wyślij do… Ustawienia Ustawienia współdzielone z Nautilus Wyświetl w menedżerze plików Pokaż element usuń trwale w menu kontekstowym Pokaż dyski zewnętrzne na pulpicie Pokaż ukryte pliki Pokazuj podgląd obrazów Wyświetl w menedżerze plików Pokaż dyski sieciowe na pulpicie Katalog domowy na pulpicie Kosz na pulpicie Pojedyncze kliknięcie Rozmiar ikon na pulpicie Mały Zwiń ten typ pliku Standardowy Ten plik .desktop zawiera błędy lub wskazuje na program, który nie ma odpowiednich praw dostępu. Nie może zostać wykonany.

	<b>Edytuj plik, by ustawić poprawny wykonywalny program Malutki Do lewego górnego rogu Do prawego górnego rogu Cofnij Odmontuj Rozwiń ten typ pliku Użyj Nemo do otwierania katalogów 