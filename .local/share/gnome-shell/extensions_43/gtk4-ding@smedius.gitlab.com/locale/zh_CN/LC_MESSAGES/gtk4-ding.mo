��    `        �         (  :   )  B   d  ,   �  6   �     	  1   '	     Y	     i	     p	     ~	     �	     �	     �	     �	     �	  5   �	     $
     +
     @
     Z
     `
     r
  $   �
     �
     �
     �
     �
     �
     �
                    +     8  	   F     P     f  b   r  +   �       #        *     0  $   @     e     v     �     �  
   �  2   �     �     �     �  "   �                <     M  
   S     ^     c  	   j     t     |  
   �     �  G   �  
   �     �          #  .   5  #   d     �     �     �  "   �  '   �  "   	     ,     9     T     Z     j  T   s  �   �  �   h     �     �                         1     J  �  g        ;   <  %   x  '   �  !   �  !   �     
               +  	   ;  	   E     O     h     ~  ?   �     �     �     �               !     .     >     E     L     Y     f     y     �     �     �     �     �     �     �     �  i   �  '   d  	   �     �     �     �     �     �     �     �                     9     I     P     W     p     �     �     �     �     �  	   �     �     �     �     �     �  9         :     G  )   N     x  '   �     �     �     �               6     U     t     {     �     �     �  P   �  �   �  �   �     5  	   9  	   C     M     T     [  '   q  $   �     !   @   +   T   /   A   O             `             Y   [   .   L      #          ;   *       -   6      0       )   C                 
   Q       $              >   =   ,   F   U      ]   H   <            %   1       3       W       V   	   K   :                     J                         (         \       S       8   Z   7   X   E   P                  B   ^   _           I   '       2       4   N          M   G                         R   &         "              9   ?      5   D    
<b>Enable option, "Allow Executing File as a Program"</b> 
<b>Set Permissions, in "Others Access", "Read Only" or "None"</b> "${programName}" is needed for Desktop Icons Action to do when launching a program from the desktop Add an emblem to soft links Add new drives to the opposite side of the screen Allow Launching Always Arrange Icons Ask what to do Bottom-left corner Bottom-right corner Broken Desktop File Broken Link Can not email a Directory Can not open this File because it is a Broken Symlink Cancel Change Background… Click type for open files Close Command not found Common Properties Compress {0} file Compress {0} files Copy Cut Delete permanently Display Settings Display the content of the file Don't Allow Launching Double click Eject Empty Trash Extract Here Extract To... File name Find Files on Desktop Folder name For this functionality to work in Desktop Icons, you must install "${programName}" in your system. Highlight the drop place during Drag'n'Drop Home Invalid Permissions on Desktop File Large Launch the file Launch using Dedicated Graphics Card Local files only Move to Trash Never New Document New Folder New Folder with {0} item New Folder with {0} items New icons alignment OK Open Open All With Other Application... Open All... Open With Other Application Open in Terminal Paste Properties Redo Rename Rename… Scripts Select Select All Select Extract Destination Selection includes a Directory, compress the directory to a file first. Send to... Settings Settings shared with Nautilus Show All in Files Show a context menu item to delete permanently Show external drives in the desktop Show hidden files Show image thumbnails Show in Files Show network drives in the desktop Show the personal folder in the desktop Show the trash icon in the desktop Single click Size for the desktop icons Small Stack This Type Standard This .desktop File has incorrect Permissions. Right Click to edit Properties, then:
 This .desktop file has errors or points to a program without permissions. It can not be executed.

	<b>Edit the file to set the correct executable Program.</b> This .desktop file is not trusted, it can not be launched. To enable launching, right-click, then:

<b>Enable "Allow Launching"</b> Tiny Top-left corner Top-right corner Undo Unmount Unstack This Type Use Nemo to open folders Use dark text in icon labels Project-Id-Version: gtk4-ding
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2023-03-05 15:42+0000
Last-Translator: Anonymous <noreply@weblate.org>
Language-Team: Chinese (Simplified) <https://hosted.weblate.org/projects/gtk4-desktop-icons-ng/gtk4-ding-pot/zh_Hans/>
Language: zh_CN
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=1; plural=0;
X-Generator: Weblate 4.16.2-dev
 
勾选<b>允许执行文件</b> 
<b>设置权限，在其他中，设置只读或者无</b> Desktop Icons 需要 "${programName}" 在桌面运行程序时执行的动作 为软链接添加一个标记点 在屏幕对面展示新驱动器 允许运行 始终 图标排序 询问如何做 左下角 右下角 已损坏的桌面文件 断开的符号链接 文件夹不能邮件 无法打开此文件，因为它是一个断开的符号链接 取消 更改背景… 打开文件的点击方式 关闭 命令无法找到 通用属性 压缩{0}文件 复制 剪切 永久删除 显示设置 展示文件内容 不允许运行 双击 驱逐 清空回收站 提取到此处 提取到… 文件名称 在桌面上发现文件 文件夹名称 为了使此功能能在 Desktop Icons 里运行，您必须在您的系统里安装 "${programName}" 。 拖拽图标时高亮显示放置区域 主目录 此桌面文件权限非法 大 运行文件 使用独立显卡启动 仅本地文件 放入回收站 从不 新建文档 新建文件夹 带{0}项的新文件夹 新图标排列 确定 打开 用其他方式打开。 全部打开... 用其他方式打开 在终端中打开 粘贴 属性 重做 重命名 重命名…… 脚本 选择 全选 选择提取目录 选择包括目录，首先将目录压缩到文件中。 发送到… 设置 共享 Nautilus 文件管理器 的设置 全部在文件中展示 永久删除时展示确认菜单项目 在桌面显示外置驱动器 显示隐藏文件 显示图片缩略图 在文件夹中显示 在桌面显示个人文件夹 在桌面显示个人文件夹 在桌面显示回收站图标 单击 桌面图标大小 小 堆叠此类型 中 此 .desktop 文件有不正确的权限设置。右键点击属性，然后：
 此 .desktop 文件有错误或者是指向了一个不够权限运行的程序。它不能被运行。

	<b>编辑文件以设置正确的可执行程序。.</b> 此 .desktop 文件是没有被信任的，它不能被执行。为使它能够被执行，右键点击属性，然后：

<b>勾选允许执行文件</b> 微 左上角 右上角 复原 弹出 取消堆叠此类型 用 Nemo 文件管理器打开文件夹 在标签文本中使用黑色字体 