��    a      $  �   ,      8  :   9  B   t  ,   �  6   �     	  1   7	     i	     y	     �	     �	     �	     �	     �	     �	     �	  5   �	     4
     ;
     P
     j
     p
     �
  $   �
     �
     �
     �
     �
     �
     �
          #     0     6     B     O  	   ]     g     }  b   �  +   �       #        A     G  $   W     |     �     �     �  
   �  2   �     �             "        +     7     S     d  
   j     u     z  	   �     �     �  
   �     �  G   �  
                  :  .   L  #   {     �     �     �  "   �  '   �  "         C     P     k     q     �  T   �  �   �  �                       )     .     6     H     a  �  ~  M   "  T   p  /   �  8   �  -   .  <   \     �     �     �     �     �     �     	     $  /   <  I   l     �     �     �     �     �       $   !     F     L     Q     X     p     �     �     �     �     �  
   �     �               .  r   B  7   �     �  )   �          #  %   2     X     k     ~     �     �  ?   �     �             '        0     >     ]     o  
   w     �     �     �     �  	   �     �  *   �  K   �  
   6     A  #   N     r  F   �  -   �     �  "        6  -   E  ,   s  !   �     �  &   �     �     �       _     �   z  �   "  	   �     �     �     �             !     +   ?     "   A   ,   U   0   B   P             3             Z   \   /   M      $           <   +       .   7      1       *   D                 
   R       %              ?   >   -   G   V      ^   I   =            &   2       4       X       W   	   L   ;                     K                         )         ]   !   T       9   [   8   Y   F   Q          a       C   _   `           J   (              5   O          N   H                         S   '         #              :   @      6   E    
<b>Enable option, "Allow Executing File as a Program"</b> 
<b>Set Permissions, in "Others Access", "Read Only" or "None"</b> "${programName}" is needed for Desktop Icons Action to do when launching a program from the desktop Add an emblem to soft links Add new drives to the opposite side of the screen Allow Launching Always Arrange Icons Ask what to do Bottom-left corner Bottom-right corner Broken Desktop File Broken Link Can not email a Directory Can not open this File because it is a Broken Symlink Cancel Change Background… Click type for open files Close Command not found Common Properties Compress {0} file Compress {0} files Copy Create Cut Delete permanently Display Settings Display the content of the file Don't Allow Launching Double click Eject Empty Trash Extract Here Extract To... File name Find Files on Desktop Folder name For this functionality to work in Desktop Icons, you must install "${programName}" in your system. Highlight the drop place during Drag'n'Drop Home Invalid Permissions on Desktop File Large Launch the file Launch using Dedicated Graphics Card Local files only Move to Trash Never New Document New Folder New Folder with {0} item New Folder with {0} items New icons alignment OK Open Open All With Other Application... Open All... Open With Other Application Open in Terminal Paste Properties Redo Rename Rename… Scripts Select Select All Select Extract Destination Selection includes a Directory, compress the directory to a file first. Send to... Settings Settings shared with Nautilus Show All in Files Show a context menu item to delete permanently Show external drives in the desktop Show hidden files Show image thumbnails Show in Files Show network drives in the desktop Show the personal folder in the desktop Show the trash icon in the desktop Single click Size for the desktop icons Small Stack This Type Standard This .desktop File has incorrect Permissions. Right Click to edit Properties, then:
 This .desktop file has errors or points to a program without permissions. It can not be executed.

	<b>Edit the file to set the correct executable Program.</b> This .desktop file is not trusted, it can not be launched. To enable launching, right-click, then:

<b>Enable "Allow Launching"</b> Tiny Top-left corner Top-right corner Undo Unmount Unstack This Type Use Nemo to open folders Use dark text in icon labels Project-Id-Version: gtk4-ding
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2023-03-05 15:42+0000
Last-Translator: Anonymous <noreply@weblate.org>
Language-Team: Italian <https://hosted.weblate.org/projects/gtk4-desktop-icons-ng/gtk4-ding-pot/it/>
Language: it
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: Weblate 4.16.2-dev
 
<b>Abilitare l'opzione "Consentire l'esecuzione del file come programma"</b> 
<b>Impostare i permessi in "Leggere e scrivere", "Leggere soltanto" o "Nessuno"</b> "${programName}" è richiesto per Desktop Icons Azione da eseguire avviando un programma dalla scrivania Aggiungere un'icona ai collegamenti simbolici Aggiungi le nuove periferiche sul lato opposto dello schermo Permetti l'esecuzione Sempre Disponi icone Chiedi cosa fare Angolo in basso a sinistra Angolo in basso a destra File corrotto in scrivania Collegamento interrotto Non è possibile spedire per email una cartella Non è possibile aprire questo file perché è un collegamento interrotto Annulla Cambia lo sfondo… Tipo di click per aprire i file Chiudi Comando non trovato Proprietà generali Comprimi {0} file Comprimi {0} files Copia Crea Taglia Elimina definitivamente Impostazioni dello schermo Mostra il contenuto del file Non permettere l'esecuzione Doppio click Espelli Svuota il cestino Estrai qui Estrai in... Nome del file Trova i file sulla scrivania Nome della cartella Per permettere il funzionamento di questa funzionalità in Desktop Icons, installare "${programName}" nel sistema. Evidenzia il punto di rilascio durante il trascinamento Home Permessi non validi nel file in scrivania Grande Esegui il file Esegui con la scheda grafica dedicata Solo i file locali Sposta nel cestino Mai Nuovo documento Nuova cartella Nuova cartella con {0} elemento Nuova cartella con {0} elementi Allineamento delle nuove icone Ok Apri Apri tutto con un'altra applicazione… Apri tutto... Apri con un'altra applicazione Apri in Terminale Incolla Proprietà Ripeti Rinomina Rinomina… Script Seleziona Seleziona tutto Seleziona la destinazione per l'estrazione La selezione include una cartella, comprimere prima la cartella in un file. Invia a... Impostazioni Impostazioni condivise con Nautilus Mostra tutto in File Mostra un elemento del menù contestuale per eliminare definitivamente Mostra le periferiche esterne sulla scrivania Mostra i file nascosti Mostra le anteprime delle immagini Mostra in File Mostra le periferiche di rete sulla scrivania Mostra la cartella personale sulla scrivania Mostra il cestino sulla scrivania Click singolo Dimensione delle icone della scrivania Piccola Impila questo tipo Normale Questo file .desktop non ha i permessi corretti. Click destro per modificare le Proprietà, e:
 Questo file .desktop contiene errori o punta a un programma senza permessi. Non può essere eseguito.

	<b>Modificare il file per impostare il corretto eseguibile.</b> Questo file .desktop non è affidabile, non può essere eseguito. Per permetterne l'esecuzione, click destro e:

<b>Abilitare "Permetti l'esecuzione"</b> Minuscola Angolo in alto a sinistra Angolo in alto a destra Annulla Smonta Disimpila questo tipo Usare Nemo per aprire le cartelle Usa testo scuro nelle etichette delle icone 