��    `        �         (  :   )  B   d  ,   �  6   �     	  1   '	     Y	     i	     p	     ~	     �	     �	     �	     �	     �	  5   �	     $
     +
     @
     Z
     `
     r
  $   �
     �
     �
     �
     �
     �
     �
                    +     8  	   F     P     f  b   r  +   �       #        *     0  $   @     e     v     �     �  
   �  2   �     �     �     �  "   �                <     M  
   S     ^     c  	   j     t     |  
   �     �  G   �  
   �     �          #  .   5  #   d     �     �     �  "   �  '   �  "   	     ,     9     T     Z     j  T   s  �   �  �   h     �     �                         1     J  6  g  U   �  j   �  6   _  I   �  )   �  :   
     E     W     _     t     �     �      �     �  .   �  Q        f     n  -   �     �     �     �  0   �               $     <  !   T     v  
   �     �     �     �     �     �     �       |     4   �     �  ,   �            ,   "     O     n     �     �     �  @   �  "   �  
        "  &   )     P     ^     ~     �     �     �  	   �     �     �     �     �  ,   �  L        ^     j  "   v     �  B   �  .   �      &     G     g  *   ~  '   �  -   �     �          '     /  
   C  {   N  �   �  �   �  	   ,     6     P     g  	   n     x  &   �  0   �     !   @   +   T   /   A   O             `             Y   [   .   L      #          ;   *       -   6      0       )   C                 
   Q       $              >   =   ,   F   U      ]   H   <            %   1       3       W       V   	   K   :                     J                         (         \       S       8   Z   7   X   E   P                  B   ^   _           I   '       2       4   N          M   G                         R   &         "              9   ?      5   D    
<b>Enable option, "Allow Executing File as a Program"</b> 
<b>Set Permissions, in "Others Access", "Read Only" or "None"</b> "${programName}" is needed for Desktop Icons Action to do when launching a program from the desktop Add an emblem to soft links Add new drives to the opposite side of the screen Allow Launching Always Arrange Icons Ask what to do Bottom-left corner Bottom-right corner Broken Desktop File Broken Link Can not email a Directory Can not open this File because it is a Broken Symlink Cancel Change Background… Click type for open files Close Command not found Common Properties Compress {0} file Compress {0} files Copy Cut Delete permanently Display Settings Display the content of the file Don't Allow Launching Double click Eject Empty Trash Extract Here Extract To... File name Find Files on Desktop Folder name For this functionality to work in Desktop Icons, you must install "${programName}" in your system. Highlight the drop place during Drag'n'Drop Home Invalid Permissions on Desktop File Large Launch the file Launch using Dedicated Graphics Card Local files only Move to Trash Never New Document New Folder New Folder with {0} item New Folder with {0} items New icons alignment OK Open Open All With Other Application... Open All... Open With Other Application Open in Terminal Paste Properties Redo Rename Rename… Scripts Select Select All Select Extract Destination Selection includes a Directory, compress the directory to a file first. Send to... Settings Settings shared with Nautilus Show All in Files Show a context menu item to delete permanently Show external drives in the desktop Show hidden files Show image thumbnails Show in Files Show network drives in the desktop Show the personal folder in the desktop Show the trash icon in the desktop Single click Size for the desktop icons Small Stack This Type Standard This .desktop File has incorrect Permissions. Right Click to edit Properties, then:
 This .desktop file has errors or points to a program without permissions. It can not be executed.

	<b>Edit the file to set the correct executable Program.</b> This .desktop file is not trusted, it can not be launched. To enable launching, right-click, then:

<b>Enable "Allow Launching"</b> Tiny Top-left corner Top-right corner Undo Unmount Unstack This Type Use Nemo to open folders Use dark text in icon labels Project-Id-Version: gtk4-ding
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2022-07-03 21:02+0200
Last-Translator: Quentin PAGÈS
Language-Team: 
Language: oc
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n > 1);
X-Generator: Poedit 3.1
 
<b>Activar l’opcion, « Permetre d’executar lo fichièr coma programa »</b> 
<b>Definir las autorizacions, dins « Autres accèsses », « Lectura sola » o « Cap »</b> « ${programName} » es requerit per Desktop Icons Accion de realizar pendent l’aviada d’un programa a partir del burèu Apondre una emblèma als ligams leugièrs Apondre los lectors novèls al costat opausat de l’ecran Permetre l'aviada Totjorn Arrengar las icònas Demandar de qué far Caire en bas a esquèrra Caire en bas a drecha Mostrar lo burèu dins Fichièrs Ligam copat Impossible d’enviar un email a un Repertòri Dubertura d'aqueste fichièr impossibla pr’amor qu’es un ligam simbolic copat Anullar Modificar lo rèireplan… Tipe de clic utilizat per dobrir de fichièrs Tancar Comanda pas trobada Proprietats comunas Compressar {0} fichièr Compressar {0} fichièrs Copiar Copar Suprimir definitivament Paramètres d'afichatge Afichar lo contengut del fichièr Permetre pas l’aviada Clic doble Ejectar Voidar l'escobilièr Traire aicí Extraire dins... Nom de fichièr Recercar de fichièrs al burèu Nom del dorsièr Per que foncione aquesta foncionalitat dins Desktop Icons, devètz installar « ${programName} » dins vòstre sistèma. Enlusir la zòna de depaus pendent un lisar-depausar Dossièr personal Autorizacion invalida sul fichièr de burèu Granda Executar lo fichièr Aviar en utilizant la carta grafica dedicada Unicament los fichièrs locals Desplaçar dins l'escobilhièr Jamai Document novèl Dorsièr novèl Dorsièr novèl amb {0} element Dorsièr novèl amb {0} elements Alinhament de las icònas novèlas D’acordi Dobrir Dobrir tot amb una autra aplicacion... Dobrir tot... Dobrir amb una autra aplicacion Dobrir dins un terminal Pegar Proprietats Refar Renomenar Renomenar… Scripts Causir Tot seleccionar Seleccionar una destinacion per l'extraccion La seleccion inclutz un repertòri, compressatz lo repertòri d'en primièr. Mandar a... Paramètres Paramètres partejats amb Nautilus Tot mostrar dins Fichièrs Mostrar un element de menú contextual per suprimir definitivament Mostrar los lector extèrns montats pel burèu Visualizar los fichièrs amagats Mostrar las miniaturas d'imatge Mostrar dins Fichièrs Mostrar los lectors ret montats pel burèu Mostrar lo dorsièr personal pel burèu Mostrar l'icòna de l'escobilhièr pel burèu Clic simple Talha de las icònas burèu Pichona Apilar aqueste tipe Estandarda Aqueste fichièr .desktop a d’autorizacions incorrèctas. Fasètz clicatz drech per modificar las proprietats, puèi :
 Aqueste fichièr .desktop a d’errors o mena a un programa sens autorizacion. Pòt pas èsser executat.

	<b>Modificatz lo fichièr per definir un programa executable corrèct.</b> Aqueste fichièr .desktop es pas fisable, se pòt pas lançar. Per autorizar son lançament, fasètz clic drech puèi :

<b>Activar « Autorizar lo lançament »</b> Minuscula Caire en naut a esquèrra Caire en naut a drecha Desfar Desmontar Desplegar aqueste tipe Utilizar Nemo per dobrir los dossièrs Utilizar un tèxte escur pels labèls d’icòna 