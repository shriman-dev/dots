��    P      �  k         �  6   �  1         2     B     I     W     f     y     �     �     �     �     �     �     �  $        ,     1     5     H     Y     y     �     �     �     �     �  	   �     �     �  +   �     !	     &	     ,	  $   <	     a	     r	     �	     �	  
   �	  2   �	     �	     �	     �	  "   �	     
     
     8
     I
  
   O
     Z
     _
  	   f
     p
     x
  
   
     �
  G   �
  
   �
     �
            .   1  #   `     �     �     �  "   �  '   �  "        (     5     P     V     _     d     t     �     �  �  �  ?   �  3   �     �               #     ;     N  )   a     �     �  $   �     �     �     �  M         N     Z     g     |     �     �     �  	   �     �     �     �       &        C  @   U     �     �     �  +   �     �     �                 d   .     �     �     �  $   �     �     �     
        
   )  
   4     ?     K     Z     b     j     z  D   �     �  
   �  ,   �  &   "  =   I  /   �     �     �     �  0     0   @  )   q     �     �     �     �  
   �     �               #        )   <      D       *      9          
      3   ?   +               I      2   6   "      -       ,      8       O   K   	          L                      :   &   M   =         J                  0          E   /                         1   %   4       #   N                 7         P   '      !   G          F   >         C   A   5   $          ;   H   B   @      .          (               Action to do when launching a program from the desktop Add new drives to the opposite side of the screen Allow Launching Always Arrange Icons Ask what to do Bottom-left corner Bottom-right corner Can not email a Directory Cancel Change Background… Click type for open files Close Command not found Common Properties Compress {0} file Compress {0} files Copy Cut Delete permanently Display Settings Display the content of the file Don't Allow Launching Double click Eject Empty Trash Extract Here Extract To... File name Find Files on Desktop Folder name Highlight the drop place during Drag'n'Drop Home Large Launch the file Launch using Dedicated Graphics Card Local files only Move to Trash Never New Document New Folder New Folder with {0} item New Folder with {0} items New icons alignment OK Open Open All With Other Application... Open All... Open With Other Application Open in Terminal Paste Properties Redo Rename Rename… Scripts Select Select All Select Extract Destination Selection includes a Directory, compress the directory to a file first. Send to... Settings Settings shared with Nautilus Show All in Files Show a context menu item to delete permanently Show external drives in the desktop Show hidden files Show image thumbnails Show in Files Show network drives in the desktop Show the personal folder in the desktop Show the trash icon in the desktop Single click Size for the desktop icons Small Standard Tiny Top-left corner Top-right corner Undo Unmount Project-Id-Version: gtk4-ding
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2023-03-05 15:42+0000
Last-Translator: Anonymous <noreply@weblate.org>
Language-Team: Slovak <https://hosted.weblate.org/projects/gtk4-desktop-icons-ng/gtk4-ding-pot/sk/>
Language: sk
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=3; plural=(n==1) ? 1 : (n>=2 && n<=4) ? 2 : 0;
X-Generator: Weblate 4.16.2-dev
X-Poedit-Basepath: .
X-Poedit-SearchPath-0: .
 Akcia, ktorú vykonať po spustení programu z pracovnej plochy Pridať nové jednotky na opačnú stranu obrazovky Umožniť spúšťanie Vždy Usporiadať ikony Spýtať sa, čo robiť Ľavý spodný roh Pravý spodný roh Nedá sa odoslať adresár pomocou emailu Zrušiť Zmeniť pozadie… Typ kliknutia na otváranie súborov Zavrieť Príkaz sa nenašiel Spoločné vlastnosti Skomprimovať {0} súborov Skomprimovať {0} súbor Skomprimovať {0} súbory Kopírovať Vystrihnúť Odstrániť natrvalo Nastavenia displejov Zobraziť obsah súboru Neumožniť spustenie Dvojité kliknutie Vysunúť Vyprázdniť Kôš Rozbaliť sem Rozbaliť do… Názov súboru Nájdenie súborov na pracovnej ploche Názov priečinku Zvýrazniť miesto pustenia počas operácie ťahania a pustenia Domov Veľké Spustiť súbor Spustiť pomocou vyhradenej grafickej karty Iba miestne súbory Presunúť do Koša Nikdy Nový dokument Nový priečinok Nový priečinok s {0} položkami Nový priečinok s {0} položkou Nový priečinok s {0} položkami Zarovnanie nových ikon OK Otvoriť Otvoriť všetko inou aplikáciou... Otvoriť všetko... Otvoriť inou aplikáciou Otvoriť v termináli Vložiť Vlastnosti Zopakovať Premenovať Premenovať… Skripty Vybrať Vybrať všetko Výber cieľa rozbaľovania Výber obsahuje adresár. Najskôr skomprimujte adresár do súboru. Odoslať do… Nastavenia Nastavenia zdieľané s aplikáciou Nautilus Zobraziť všetko v aplikácii Súbory Zobraziť kontextovú ponuku položky na trvalé odstránenie Zobraziť externé jednotky na pracovnej ploche Zobraziť skryté súbory Zobraziť miniatúry obrázkov Zobraziť v aplikácii Súbory Zobraziť sieťové jednotky na pracovnej ploche Zobraziť osobný priečinok na pracovnej ploche Zobraziť ikonu Koša na pracovnej ploche Jednoduché kliknutie Veľkosť ikon pracovnej plochy Malé Štandardné Najmenšie Ľavý horný roh Pravý horný roh Vrátiť späť Odpojiť 