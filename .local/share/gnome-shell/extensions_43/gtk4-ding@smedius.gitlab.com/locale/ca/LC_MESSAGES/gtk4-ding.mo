��    J      l  e   �      P  6   Q  1   �     �     �     �     �     �          !     (     =     W     i  $   {     �     �     �     �     �     �                    "     /  	   =     G  +   S          �     �  $   �     �     �     �     �  
   �  2   �     /	     C	     F	  "   K	     n	     z	     �	     �	  
   �	     �	     �	  	   �	     �	  G   �	  
   
     )
     2
     P
  .   b
  #   �
     �
     �
     �
  "   �
  '     "   6     Y     f     �     �     �     �     �     �     �  �  �  ;   g  5   �     �     �     �          $  4   =  
   r  !   }  %   �     �     �  ,   �          "     *     B     ]     |  
   �     �     �     �     �     �     �  0   �     &     ,     1  '   C     k     �     �     �     �  :   �     �                     9      H     i     z  
   �     �     �     �     �  J   �  
          &        D  E   V  *   �     �  $   �       *     )   C  -   m     �     �     �  
   �     �     �     �                         9   ;   G   +   (              5   "   F            $      7       ?   !   .   2   )         A      8   '   -      /   E                 1                      	           %         
      H       <                            &      >   J   4   6   :   3                   *      D   =   B      C                  I                   0           @   ,       #    Action to do when launching a program from the desktop Add new drives to the opposite side of the screen Allow Launching Always Ask what to do Bottom-left corner Bottom-right corner Can not email a Directory Cancel Change Background… Click type for open files Command not found Common Properties Compress {0} file Compress {0} files Copy Cut Delete permanently Display Settings Display the content of the file Don't Allow Launching Double click Eject Empty Trash Extract Here Extract To... File name Folder name Highlight the drop place during Drag'n'Drop Home Large Launch the file Launch using Dedicated Graphics Card Local files only Move to Trash Never New Document New Folder New Folder with {0} item New Folder with {0} items New icons alignment OK Open Open All With Other Application... Open All... Open With Other Application Open in Terminal Paste Properties Redo Rename Rename… Scripts Selection includes a Directory, compress the directory to a file first. Send to... Settings Settings shared with Nautilus Show All in Files Show a context menu item to delete permanently Show external drives in the desktop Show hidden files Show image thumbnails Show in Files Show network drives in the desktop Show the personal folder in the desktop Show the trash icon in the desktop Single click Size for the desktop icons Small Standard Tiny Top-left corner Top-right corner Undo Unmount Project-Id-Version: gtk4-ding
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2023-03-05 15:42+0000
Last-Translator: Anonymous <noreply@weblate.org>
Language-Team: Catalan <https://hosted.weblate.org/projects/gtk4-desktop-icons-ng/gtk4-ding-pot/ca/>
Language: ca
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: Weblate 4.16.2-dev
 Acció a fer quan s'iniciï un programa des de l'escriptori Afegeix noves unitats al costat oposat de la pantalla Permet que s'iniciï Sempre Preguntar què fer Cantonada inferior esquerra Cantonada inferior dreta No es pot enviar un directori per correu electrònic Cancel·la Canvia el fons de l'escriptori… Feu clic al tipus per a obrir fitxers No s'ha trobat l'ordre Propietats comunes Comprimeix {0} fitxer Comprimeix {0} fitxers Copia Retalla Suprimeix permanentment Paràmetres de la pantalla Mostra el contingut del fitxer No permetis que s'iniciï Doble clic Expulsa Buida la paperera Extreu aquí Extreu a... Nom del fitxer Nom de la carpeta Ressalta el lloc desplegable durant l'arrossegat Inici Gran Executa el fitxer Llança usant targeta gràfica dedicada Només fitxers locals Mou a la paperera Mai Document nou Carpeta nova Carpeta nova amb {0} element Carpeta nova amb {0} elements Alineació de les icones noves D'acord Obre Obre amb una altra aplicació... Obre-ho tot... Obre amb una altra aplicació... Obre al Terminal Enganxa Propietats Refés Canvia el nom Canvia el nom… Scripts La selecció inclou un directori, comprimeix-hi el directori en un fitxer. Envia a... Paràmetres Paràmetres compartits amb el Nautilus Mostra al Fitxers Mostra un element de menú contextual per a suprimir-lo permanentment Mostra les unitats externes a l'escriptori Mostra els fitxers ocults Mostra les miniatures de les imatges Mostra al Fitxers Mostra les unitats de xarxa a l'escriptori Mostra la carpeta personal a l'escriptori Mostra la icona de la paperera a l'escriptori Un sol clic Mida de les icones d'escriptori Petita Estàndard Diminuta Cantonada superior esquerra Cantonada superior dreta Desfés Desmunta 