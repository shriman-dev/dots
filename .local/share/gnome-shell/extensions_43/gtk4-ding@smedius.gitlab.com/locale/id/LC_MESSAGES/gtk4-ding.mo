��    &      L  5   |      P     Q     a     h     }     �     �     �     �     �     �     �     �     �     �     �     �     	  
        !     $     )     E     V  
   \     g  	   l     v  
   }     �     �  '   �  "   �     �                      �  !     �     �     �     �                         -     A     Q     g     w     �     �     �     �     �     �     �     �     �     �        	   	          !     '  
   3     >  "   T  $   w     �     �     �     �     �     #      &                                       
             $         %                                                                                      "          	   !       Allow Launching Cancel Change Background… Command not found Copy Create Cut Delete permanently Display Settings Eject Empty Trash Extract Here Extract To... Home Large Move to Trash New Document New Folder OK Open Open With Other Application Open in Terminal Paste Properties Redo Rename… Select Send to... Settings Show in Files Show the personal folder in the desktop Show the trash icon in the desktop Size for the desktop icons Small Standard Undo Unmount Project-Id-Version: gtk4-ding
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2023-03-05 15:42+0000
Last-Translator: Anonymous <noreply@weblate.org>
Language-Team: Indonesian <https://hosted.weblate.org/projects/gtk4-desktop-icons-ng/gtk4-ding-pot/id/>
Language: id
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=1; plural=0;
X-Generator: Weblate 4.16.2-dev
 Izinkan Peluncuran Batal Ubah Latar Belakang… Perintah tidak ditemukan Salin Buat Potong Hapus permanen Pengaturan Tampilan Keluarkan Media Kosongkan Tong Sampah Ekstrak Di Sini Ekstrak ke… Rumah Besar Pindahkan ke Tong Sampah Dokumen Baru Folder Baru OK Buka Buka Dengan Aplikasi Lain Buka dalam Terminal Tempel Properti Jadi Lagi Ganti Nama… Pilih Kirim ke… Pengaturan Tampilkan pada Berkas Tampilkan folder pribadi di destop Tampilkan ikon tong sampah di destop Ukuran untuk ikon destop Kecil Standar Tak Jadi Lepas Kaitan 