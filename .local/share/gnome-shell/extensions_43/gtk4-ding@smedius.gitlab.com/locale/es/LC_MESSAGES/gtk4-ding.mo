��    a      $  �   ,      8  :   9  B   t  ,   �  6   �     	  1   7	     i	     y	     �	     �	     �	     �	     �	     �	     �	  5   �	     4
     ;
     P
     j
     p
     �
  $   �
     �
     �
     �
     �
     �
     �
          #     0     6     B     O  	   ]     g     }  b   �  +   �       #        A     G  $   W     |     �     �     �  
   �  2   �     �             "        +     7     S     d  
   j     u     z  	   �     �     �  
   �     �  G   �  
                  :  .   L  #   {     �     �     �  "   �  '   �  "         C     P     k     q     �  T   �  �   �  �                       )     .     6     H     a  �  ~  I   "  O   l  3   �  <   �  ,   -  9   Z     �     �     �     �     �     �                )  7   J     �     �  !   �     �     �     �  ,   �          &     ,     3     J      c     �     �     �     �     �     �     �      �       o   "  0   �     �  +   �             '        C     Z     n     t     �  >   �     �     �     �     �          +     F     \     b     n     v     �     �     �     �  #   �  K   �     %     1  &   >     e  9   {  *   �     �     �       (   *  ,   S  $   �     �  $   �     �     �  	   �  w   �  �   t  �   "     �     �     �       	   
       !   )  4   K     "   A   ,   U   0   B   P             3             Z   \   /   M      $           <   +       .   7      1       *   D                 
   R       %              ?   >   -   G   V      ^   I   =            &   2       4       X       W   	   L   ;                     K                         )         ]   !   T       9   [   8   Y   F   Q          a       C   _   `           J   (              5   O          N   H                         S   '         #              :   @      6   E    
<b>Enable option, "Allow Executing File as a Program"</b> 
<b>Set Permissions, in "Others Access", "Read Only" or "None"</b> "${programName}" is needed for Desktop Icons Action to do when launching a program from the desktop Add an emblem to soft links Add new drives to the opposite side of the screen Allow Launching Always Arrange Icons Ask what to do Bottom-left corner Bottom-right corner Broken Desktop File Broken Link Can not email a Directory Can not open this File because it is a Broken Symlink Cancel Change Background… Click type for open files Close Command not found Common Properties Compress {0} file Compress {0} files Copy Create Cut Delete permanently Display Settings Display the content of the file Don't Allow Launching Double click Eject Empty Trash Extract Here Extract To... File name Find Files on Desktop Folder name For this functionality to work in Desktop Icons, you must install "${programName}" in your system. Highlight the drop place during Drag'n'Drop Home Invalid Permissions on Desktop File Large Launch the file Launch using Dedicated Graphics Card Local files only Move to Trash Never New Document New Folder New Folder with {0} item New Folder with {0} items New icons alignment OK Open Open All With Other Application... Open All... Open With Other Application Open in Terminal Paste Properties Redo Rename Rename… Scripts Select Select All Select Extract Destination Selection includes a Directory, compress the directory to a file first. Send to... Settings Settings shared with Nautilus Show All in Files Show a context menu item to delete permanently Show external drives in the desktop Show hidden files Show image thumbnails Show in Files Show network drives in the desktop Show the personal folder in the desktop Show the trash icon in the desktop Single click Size for the desktop icons Small Stack This Type Standard This .desktop File has incorrect Permissions. Right Click to edit Properties, then:
 This .desktop file has errors or points to a program without permissions. It can not be executed.

	<b>Edit the file to set the correct executable Program.</b> This .desktop file is not trusted, it can not be launched. To enable launching, right-click, then:

<b>Enable "Allow Launching"</b> Tiny Top-left corner Top-right corner Undo Unmount Unstack This Type Use Nemo to open folders Use dark text in icon labels Project-Id-Version: gtk4-ding
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2023-03-05 15:42+0000
Last-Translator: Anonymous <noreply@weblate.org>
Language-Team: Spanish <https://hosted.weblate.org/projects/gtk4-desktop-icons-ng/gtk4-ding-pot/es/>
Language: es
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: Weblate 4.16.2-dev
 
<b>Activa la opción "Permitir ejecutar el archivo como un programa"</b> 
<b>Cambiar los permisos, en "Otros accesos", a "Sólo lectura" o "Ninguno"</b> Desktop Icons necesita el programa "${programName}" Acción a realizar al lanzar un programa desde el escritorio Añadir un emblema a los enlaces simbólicos Añadir nuevas unidades en el lado opuesto de la pantalla Permitir lanzar Siempre Ordenar Iconos Preguntar qué hacer Esquina inferior-izquierda Esquina inferior-derecha Fichero .desktop dañado Enlace roto No se puede enviar un directorio No se puede abrir este fichero porque es un enlace roto Cancelar Cambiar el fondo... Tipo de click para abrir ficheros Cerrar Comando no encontrado Propiedades Comunes Comprimir {0} archivo Comprimir {0} archivos Copiar Crear Cortar Borrar permanentemente Preferencias de pantalla Mostrar el contenido del fichero No permitir lanzar Click doble Expulsar Vaciar la papelera Extraer aquí Extraer a... Nombre de archivo Buscar ficheros en el Escritorio Nombre de la carpeta Para disponer de esta funcionalidad en Desktop Icons, es necesario que instales "${programName}" en tu sistema. Resaltar el lugar de destino durante Drag'n'Drop Carpeta personal Permisos no válidos en el fichero .desktop Grande Ejecutar el fichero Lanzar con la Tarjeta Gráfica Dedicada Sólo ficheros locales Mover a la papelera Nunca Documento nuevo Nueva carpeta Nueva carpeta con {0} elemento Nueva carpeta con {0} elementos Alineamiento de nuevos iconos Aceptar Abrir Abrir todo con otra aplicación Abrir todo... Abrir con otra aplicación Abrir en una terminal Pegar Propiedades Rehacer Renombrar… Renombrar… Scripts Seleccionar Seleccionar Todo Seleccionar el destino para extraer La selección incluye un directorio. Debe comprimirse a un fichero primero. Enviar a... Preferencias Configuración compartida con Nautilus Mostrar todo en Files Permitir borrar permanentemente desde el menú contextual Mostrar unidades externas en el escritorio Mostrar archivos ocultos Mostrar miniaturas de imágenes Mostrar en Files Mostrar unidades de red en el escritorio Mostrar la carpeta personal en el escritorio Mostrar la papelera en el escritorio Click simple Tamaño de los iconos del escritorio Pequeño Agrupar este tipo Estándar Este fichero .desktop tiene permisos incorrectos. Haz click con el botón derecho para editar la Propiedades, y luego:
 Este fichero .desktop tiene errores o apunta a un programa sin los permisos necesarios. No se puede lanzar.

	<b>Edita el fichero para que apunte al ejecutable correcto.</b> Este fichero .desktop no es confiable, por lo que no se puede lanzar. Para permitirlo, haz click con el botón derecho, y luego:

<b>Escoge "Permitir lanzar"</b> Diminuto Esquina superior-izquierda Esquina superior-derecha Deshacer Desmontar Desagrupar este tipo Usar Nemo para abrir las carpetas Utilizar texto oscuro en las etiquetas de los iconos 