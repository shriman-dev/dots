��    P      �  k         �  6   �  1         2     B     I     W     f     y     �     �     �     �     �     �     �  $        ,     1     5     H     Y     y     �     �     �     �     �  	   �     �     �  +   �     !	     &	     ,	  $   <	     a	     r	     �	     �	  
   �	  2   �	     �	     �	     �	  "   �	     
     
     8
     I
  
   O
     Z
     _
  	   f
     p
     x
  
   
     �
  G   �
  
   �
     �
            .   1  #   `     �     �     �  "   �  '   �  "        (     5     P     V     _     d     t     �     �  �  �  -   5  8   c     �     �     �     �     �     �  -     	   9     C     [  
   j     u     �  -   �     �     �     �     �               %  	   1     ;     M     \  	   o  #   y  
   �  (   �     �     �  
   �  #   �          :     X     \     k  7   x  &   �     �     �  #   �               5  	   I     S     a     m     |     �  
   �     �  #   �  U   �     )     7  %   E      k  (   �  1   �     �           $  2   @  8   s  1   �     �  "   �                     '     7     H  
   U        )   <      D       *      9          
      3   ?   +               I      2   6   "      -       ,      8       O   K   	          L                      :   &   M   =         J                  0          E   /                         1   %   4       #   N                 7         P   '      !   G          F   >         C   A   5   $          ;   H   B   @      .          (               Action to do when launching a program from the desktop Add new drives to the opposite side of the screen Allow Launching Always Arrange Icons Ask what to do Bottom-left corner Bottom-right corner Can not email a Directory Cancel Change Background… Click type for open files Close Command not found Common Properties Compress {0} file Compress {0} files Copy Cut Delete permanently Display Settings Display the content of the file Don't Allow Launching Double click Eject Empty Trash Extract Here Extract To... File name Find Files on Desktop Folder name Highlight the drop place during Drag'n'Drop Home Large Launch the file Launch using Dedicated Graphics Card Local files only Move to Trash Never New Document New Folder New Folder with {0} item New Folder with {0} items New icons alignment OK Open Open All With Other Application... Open All... Open With Other Application Open in Terminal Paste Properties Redo Rename Rename… Scripts Select Select All Select Extract Destination Selection includes a Directory, compress the directory to a file first. Send to... Settings Settings shared with Nautilus Show All in Files Show a context menu item to delete permanently Show external drives in the desktop Show hidden files Show image thumbnails Show in Files Show network drives in the desktop Show the personal folder in the desktop Show the trash icon in the desktop Single click Size for the desktop icons Small Standard Tiny Top-left corner Top-right corner Undo Unmount Project-Id-Version: gtk4-ding
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2023-03-05 15:42+0000
Last-Translator: Anonymous <noreply@weblate.org>
Language-Team: German <https://hosted.weblate.org/projects/gtk4-desktop-icons-ng/gtk4-ding-pot/de/>
Language: de
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: Weblate 4.16.2-dev
 Ausführbare Textdateien auf dem Schreibtisch Neue Laufwerke gegenüber auf dem Bildschirm hinzufügen Start erlauben Alle Dateien Symbole anordnen Nachfragen, was geschehen soll Ecke unten links Ecke unten rechts Kann Verzeichnis nicht per E-Mail verschicken Abbrechen Hintergrund ändern … Öffnen-Aktion Schließen Kommando nicht gefunden Eigenschaften Komprimiere {0} Datei Komprimiere {0} Dateien Kopieren Ausschneiden Dauerhaft löschen Anzeigeeinstellungen Anzeigen Start nicht erlauben Doppelklick Auswerfen Papierkorb leeren Hier entpacken Entpacken nach ... Dateiname Dateien auf dem Schreibtisch finden Ordnername Zielposition bei Drag'n'Drop hervorheben Persönlicher Ordner Groß Ausführen Mit dedizierter Grafikkarte starten Nur Dateien auf diesem Rechner In den Papierkorb verschieben Nie Neues Dokument Neuer Ordner Neuer Ordner mit {0} Datei Neuer Ordner mit {0} Dateien Ausrichtung der Arbeitsflächensymbole OK Öffnen Alle mit anderer Anwendung öffnen. Alle öffnen ... Mit anderer Anwendung öffnen Im Terminal öffnen Einfügen Eigenschaften Wiederholen Umbenennen ... Umbenennen … Skripte Auswählen Alles auswählen Ziel für die Entpackung auswählen Auswahl beinhaltet ein Verzeichnis, komprimiere zunächst Verzeichnis zu einer Datei. Senden an ... Einstellungen Anpassung an Nautilus-Dateiverwaltung Alle in Dateiverwaltung anzeigen Aktion zum dauerhaften Löschen anzeigen Externe Laufwerke auf der Arbeitsfläche anzeigen Verborgene Dateien anzeigen Miniatur-Vorschaubilder anzeigen In Dateiverwaltung anzeigen Netzwerk-Laufwerke auf der Arbeitsfläche anzeigen Den persönlichen Ordner auf der Arbeitsfläche anzeigen Papierkorb-Symbol auf der Arbeitsfläche anzeigen Einfacher Klick Größe der Arbeitsflächensymbole Klein Standard Winzig Ecke oben links Ecke oben rechts Rückgängig Aushängen 