��    J      l  e   �      P  6   Q  1   �     �     �     �     �     �          !     (     =     W     i  $   {     �     �     �     �     �     �                    "     /  	   =     G  +   S          �     �     �     �     �     �  
   �     �     �     �  "   �     	     "	     >	     O	  
   U	     `	     e	  	   l	     v	     ~	     �	  G   �	  
   �	     �	     �	     
  .   ,
  #   [
     
     �
     �
  "   �
  '   �
  "         #     0     K     Q     Z     _     o     �     �  �  �  '   B  *   j     �     �     �  	   �  	   �     �     �     �          "     5     B     T     [     b     o     |     �     �     �     �     �     �     �     �  $   �  	     	   &     0     =     P     c     j     w     �     �     �  '   �     �     �                    &     -     :  	   J     T     [  B   t     �     �     �     �  !         "     >     N     a     z     �     �     �     �  	   �     �       	     	   "     ,     3               9   ;   G   )   &              5   !   F            #      7       ?   <   ,   0   '         A      4   @   +      -   E                 /              3       	       8   $         
      H       J                            %      >       2   6   :   1                   (      D   =   B      C                  I                   .               *       "    Action to do when launching a program from the desktop Add new drives to the opposite side of the screen Allow Launching Always Ask what to do Bottom-left corner Bottom-right corner Can not email a Directory Cancel Change Background… Click type for open files Command not found Common Properties Compress {0} file Compress {0} files Copy Cut Delete permanently Display Settings Display the content of the file Don't Allow Launching Double click Eject Empty Trash Extract Here Extract To... File name Folder name Highlight the drop place during Drag'n'Drop Home Large Launch the file Local files only Move to Trash Never New Document New Folder New icons alignment OK Open Open All With Other Application... Open All... Open With Other Application Open in Terminal Paste Properties Redo Rename Rename… Scripts Select Select Extract Destination Selection includes a Directory, compress the directory to a file first. Send to... Settings Settings shared with Nautilus Show All in Files Show a context menu item to delete permanently Show external drives in the desktop Show hidden files Show image thumbnails Show in Files Show network drives in the desktop Show the personal folder in the desktop Show the trash icon in the desktop Single click Size for the desktop icons Small Standard Tiny Top-left corner Top-right corner Undo Unmount Project-Id-Version: gtk4-ding
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2023-03-05 15:42+0000
Last-Translator: Anonymous <noreply@weblate.org>
Language-Team: Chinese (Traditional) <https://hosted.weblate.org/projects/gtk4-desktop-icons-ng/gtk4-ding-pot/zh_Hant/>
Language: zh_TW
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=1; plural=0;
X-Generator: Weblate 4.16.2-dev
 在桌面執行應用程式時的動作 將新掛載硬碟置於螢幕的另一邊 允許執行 總是 詢問要做什麼 左下角 右下角 無法寄送資料夾 取消 變更背景圖片… 開啟檔案時的點擊方式 無法找到指令 共同屬性 壓縮 {0} 檔案 複製 剪下 永久刪除 顯示設定 顯示檔案內容 不允許執行 雙擊 退出 清空垃圾桶 在這裡解開 解開到… 檔案名稱 資料夾名稱 拖曳時強調顯示拖曳後位置 家目錄 大圖示 執行檔案 顯示本機檔案 移動到垃圾桶 從不 新增文件 新增資料夾 新圖示排序位置 確定 開啟 用其他應用程式開啟所有…… 開啟所有項目…… 用其他應用程式開啟 開啟終端器 貼上 屬性 重做 重新命名 重新命名… 命令稿 選擇 選擇解壓縮的位置 已選項目含資料夾，先把資料夾壓縮成一個檔案。 傳送到… 設定 與《檔案》共用設定 在《檔案》中顯示 在快速選單顯示永久刪除 在桌面顯示外接硬碟 顯示隱藏檔 顯示影像縮圖 在《檔案》中顯示 在桌面顯示網路資料夾 在桌面顯示個人資料夾 在桌面顯示垃圾桶圖示 單擊 桌面圖示的大小 小圖示 適中圖示 更小圖示 左上角 右上角 復原 卸載 