��    O      �  k         �  6   �  1   �     "     2     9     G     V     i     }     �     �     �     �     �     �  $   �          !     %     8     I     i          �     �     �     �  	   �     �     �  +   �     	     	     	  $   ,	     Q	     b	     p	     v	  
   �	  2   �	     �	     �	     �	  "   �	      
     
     (
     9
  
   ?
     J
     O
  	   V
     `
     h
     o
  G   �
  
   �
     �
     �
       .     #   E     i     {     �  "   �  '   �  "   �               5     ;     D     I     Y     j     o  �  w  C   g  e   �          1     >     W     v      �  T   �            <   7     t  $   �  %   �  _   �     .     A  #   R     v  &   �  $   �     �     �  #        1     Q     p  /   �     �  [   �     &     5     D  N   `  (   �  (   �       !        0  �   J  =   �            9   &     `  /   {  &   �     �     �     �          (     F     U  7   f  `   �     �       -   /  '   ]  ^   �  D   �  2   )  8   \      �  H   �  F   �  R   F     �  7   �  
   �     �           #      D     e     x        )   ;      C       *      A          
      3   >   +               H      2   6   "      -       ,      8       N   J   	          K                      9   &   L   <         I                  0          D   /                         1   %   4       #   M                 7         O   '      !   F          E   =         B   @   5   $          :   G       ?      .          (               Action to do when launching a program from the desktop Add new drives to the opposite side of the screen Allow Launching Always Arrange Icons Ask what to do Bottom-left corner Bottom-right corner Can not email a Directory Cancel Change Background… Click type for open files Close Command not found Common Properties Compress {0} file Compress {0} files Copy Cut Delete permanently Display Settings Display the content of the file Don't Allow Launching Double click Eject Empty Trash Extract Here Extract To... File name Find Files on Desktop Folder name Highlight the drop place during Drag'n'Drop Home Large Launch the file Launch using Dedicated Graphics Card Local files only Move to Trash Never New Document New Folder New Folder with {0} item New Folder with {0} items New icons alignment OK Open Open All With Other Application... Open All... Open With Other Application Open in Terminal Paste Properties Redo Rename Rename… Scripts Select Select Extract Destination Selection includes a Directory, compress the directory to a file first. Send to... Settings Settings shared with Nautilus Show All in Files Show a context menu item to delete permanently Show external drives in the desktop Show hidden files Show image thumbnails Show in Files Show network drives in the desktop Show the personal folder in the desktop Show the trash icon in the desktop Single click Size for the desktop icons Small Standard Tiny Top-left corner Top-right corner Undo Unmount Project-Id-Version: gtk4-ding
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2023-03-05 15:42+0000
Last-Translator: Anonymous <noreply@weblate.org>
Language-Team: Ukrainian <https://hosted.weblate.org/projects/gtk4-desktop-icons-ng/gtk4-ding-pot/uk/>
Language: uk
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=3; plural=n%10==1 && n%100!=11 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2;
X-Generator: Weblate 4.16.2-dev
 Дія при запуску програми з стільниці Додавати нові змінні носії до протилежного боку екрану Дозволяти запуск Завжди Впорядкувати Питати що робити Нижній лівий кут Нижній правий кут Неможливо відправити теку електронною поштою Скасувати Змінити тло… Тип натиску для відкриття файлів Закрити Команда не знайдена Спільні властивості Стиснути {0} файл Стиснути {0} файли Стиснути {0} файлів Копіювати Вирізати Вилучити остаточно Параметри екрану Показати вміст файлу Не дозволяти запуск Подвійний клік Витягти Спорожнити смітник Розпакувати сюди Розпакувати до... Назва файлу Пошук файлів на стільниці Назва теки Підсвічувати місце падіння під час перетягування Домівка Великий Запустити файл Запустити через відповідну графічну плату Тільки локальні файли Перемістити у смітник Ніколи Створити документ Створити теку Нова тека з {0} елементом Нова тека з {0} елементами Нова тека з {0} елементами Вирівнювання для нових піктограм Ок Відкрити Відкрити все в іншій програмі... Відкрити все... Відкрити в іншій програмі Відкрити в терміналі Вставити Властивості Повторити Перейменувати Перейменувати… Скрипти Виділити Вибрати шлях для розпакування Вибір включає теку, для початку стисніть теку в файл. Відправити до... Параметри Спільні параметри з Nautilus Показати все у Файлах Додати пункт "Вилучити остаточно" у контекстне меню Показувати змінні носії на стільниці Показувати приховані файли Показати мініатюру зображення Показати у Файлах Показувати мережеві носії на стільниці Показувати особисту теку на стільниці Показувати піктограму смітника на стільниці Одинарний клік Розмір піктограм на стільниці Малий Стандартний Дуже малий Верхній лівий кут Верхній лівий кут Відмінити Демонтувати 