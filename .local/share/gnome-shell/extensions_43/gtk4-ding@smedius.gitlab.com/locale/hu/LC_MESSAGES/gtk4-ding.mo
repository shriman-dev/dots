��    J      l  e   �      P  6   Q  1   �     �     �     �     �     �          !     (     =     W     i  $   {     �     �     �     �     �     �                    "     /  	   =     G  +   S          �     �  $   �     �     �     �     �  
   �  2   �     /	     C	     F	  "   K	     n	     z	     �	     �	  
   �	     �	     �	  	   �	     �	  G   �	  
   
     )
     2
     P
  .   b
  #   �
     �
     �
     �
  "   �
  '     "   6     Y     f     �     �     �     �     �     �     �  �  �  D   i  =   �     �               )     9  &   J     q     x  :   �     �     �  1     	   4  	   >     H     [  $   y     �     �     �     �     �     �  	     	     8        N     [     `  &   s     �     �     �     �  	   �  +   �          .  
   6  ,   A     n      �     �     �     �     �     �     �     �  F   
     Q     ]  '   l  $   �  8   �  0   �     #     C     a  4   }  .   �  '   �     	          8  	   >     H     N     _     q     ~               9   ;   G   +   (              5   "   F            $      7       ?   !   .   2   )         A      8   '   -      /   E                 1                      	           %         
      H       <                            &      >   J   4   6   :   3                   *      D   =   B      C                  I                   0           @   ,       #    Action to do when launching a program from the desktop Add new drives to the opposite side of the screen Allow Launching Always Ask what to do Bottom-left corner Bottom-right corner Can not email a Directory Cancel Change Background… Click type for open files Command not found Common Properties Compress {0} file Compress {0} files Copy Cut Delete permanently Display Settings Display the content of the file Don't Allow Launching Double click Eject Empty Trash Extract Here Extract To... File name Folder name Highlight the drop place during Drag'n'Drop Home Large Launch the file Launch using Dedicated Graphics Card Local files only Move to Trash Never New Document New Folder New Folder with {0} item New Folder with {0} items New icons alignment OK Open Open All With Other Application... Open All... Open With Other Application Open in Terminal Paste Properties Redo Rename Rename… Scripts Selection includes a Directory, compress the directory to a file first. Send to... Settings Settings shared with Nautilus Show All in Files Show a context menu item to delete permanently Show external drives in the desktop Show hidden files Show image thumbnails Show in Files Show network drives in the desktop Show the personal folder in the desktop Show the trash icon in the desktop Single click Size for the desktop icons Small Standard Tiny Top-left corner Top-right corner Undo Unmount Project-Id-Version: gtk4-ding
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2023-03-05 15:42+0000
Last-Translator: Anonymous <noreply@weblate.org>
Language-Team: Hungarian <https://hosted.weblate.org/projects/gtk4-desktop-icons-ng/gtk4-ding-pot/hu/>
Language: hu
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: Weblate 4.16.2-dev
 Program asztalról történő indításakor végrehajtandó művelet Új meghajtók hozzáadása a képernyő ellentétes oldalán Indítás engedélyezése Mindig Kérdezze meg, mi a teendő Bal alsó sarok Jobb alsó sarok Könyvtár nem küldhető el e-mailben Mégse Háttér megváltoztatása… A fájlok megnyitásához használandó kattintás típusa A parancs nem található Gyakori tulajdonságok {0} fájl tömörítése {0} fájl tömörítése Másolás Kivágás Végleges törlés Megjelenítés beállításai A fájl tartalmának megjelenítése Ne engedélyezze az indítást Dupla kattintás Kiadás Kuka ürítése Kibontás ide Kibontás… Fájlnév Mappanév Az ejtés helyének kiemelése fogd és vidd műveletkor Saját mappa Nagy A fájl futtatása Futtatás a dedikált videokártyával Csak helyi fájlok esetén Áthelyezés a Kukába Soha Új dokumentum Új mappa Új mappa {0} elemmel Új mappa {0} elemmel Új ikonok elrendezése Rendben Megnyitás Összes megnyitása egyéb alkalmazással… Összes megnyitása… Megnyitás egyéb alkalmazással Megnyitás terminálban Beillesztés Tulajdonságok Újra Átnevezés Átnevezés… Parancsfájlok A kijelölés könyvtárat tartalmaz, előbb tömörítse egy fájlba. Küldés… Beállítások A Nautilussal megosztott beállítások Összes megjelenítése a Fájlokban Helyi menü-elem megjelenítése a végleges törléshez A külső meghajtók megjelenítése az asztalon Rejtett fájlok megjelenítése Bélyegképek megjelenítése Megjelenítés a Fájlokban A hálózati meghajtók megjelenítése az asztalon. A személyes mappa megjelenítése az asztalon A kuka ikon megjelenítése az asztalon Egyszeres kattintás Az asztali ikonok mérete Kicsi Szokásos Apró Bal felső sarok Jobb felső sarok Visszavonás Leválasztás 