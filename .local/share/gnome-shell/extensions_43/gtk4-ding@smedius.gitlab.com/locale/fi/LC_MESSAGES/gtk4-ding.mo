��    `        �         (  :   )  B   d  ,   �  6   �     	  1   '	     Y	     i	     p	     ~	     �	     �	     �	     �	     �	  5   �	     $
     +
     @
     Z
     `
     r
  $   �
     �
     �
     �
     �
     �
     �
                    +     8  	   F     P     f  b   r  +   �       #        *     0  $   @     e     v     �     �  
   �  2   �     �     �     �  "   �                <     M  
   S     ^     c  	   j     t     |  
   �     �  G   �  
   �     �          #  .   5  #   d     �     �     �  "   �  '   �  "   	     ,     9     T     Z     j  T   s  �   �  �   h     �     �                         1     J  �  g  C   :  S   ~  2   �  5     '   ;  ;   c     �     �     �     �     �     �           $  +   8  :   d     �     �  %   �     �     �     �  (        8     ?     G     Y     l     �     �     �     �     �     �     �     �       k   "  4   �     �  )   �     �     �  .        <     W  
   k     v     �  :   �     �     �     �  %   �          %     A     T     [     h     v     �     �     �     �     �  =   �       	      0   *  &   [  -   �  4   �     �     �       $   .  "   S  )   v     �     �     �     �  
   �  v   �  �   j  �   ,     �     �                    (  #   F  0   j     !   @   +   T   /   A   O             `             Y   [   .   L      #          ;   *       -   6      0       )   C                 
   Q       $              >   =   ,   F   U      ]   H   <            %   1       3       W       V   	   K   :                     J                         (         \       S       8   Z   7   X   E   P                  B   ^   _           I   '       2       4   N          M   G                         R   &         "              9   ?      5   D    
<b>Enable option, "Allow Executing File as a Program"</b> 
<b>Set Permissions, in "Others Access", "Read Only" or "None"</b> "${programName}" is needed for Desktop Icons Action to do when launching a program from the desktop Add an emblem to soft links Add new drives to the opposite side of the screen Allow Launching Always Arrange Icons Ask what to do Bottom-left corner Bottom-right corner Broken Desktop File Broken Link Can not email a Directory Can not open this File because it is a Broken Symlink Cancel Change Background… Click type for open files Close Command not found Common Properties Compress {0} file Compress {0} files Copy Cut Delete permanently Display Settings Display the content of the file Don't Allow Launching Double click Eject Empty Trash Extract Here Extract To... File name Find Files on Desktop Folder name For this functionality to work in Desktop Icons, you must install "${programName}" in your system. Highlight the drop place during Drag'n'Drop Home Invalid Permissions on Desktop File Large Launch the file Launch using Dedicated Graphics Card Local files only Move to Trash Never New Document New Folder New Folder with {0} item New Folder with {0} items New icons alignment OK Open Open All With Other Application... Open All... Open With Other Application Open in Terminal Paste Properties Redo Rename Rename… Scripts Select Select All Select Extract Destination Selection includes a Directory, compress the directory to a file first. Send to... Settings Settings shared with Nautilus Show All in Files Show a context menu item to delete permanently Show external drives in the desktop Show hidden files Show image thumbnails Show in Files Show network drives in the desktop Show the personal folder in the desktop Show the trash icon in the desktop Single click Size for the desktop icons Small Stack This Type Standard This .desktop File has incorrect Permissions. Right Click to edit Properties, then:
 This .desktop file has errors or points to a program without permissions. It can not be executed.

	<b>Edit the file to set the correct executable Program.</b> This .desktop file is not trusted, it can not be launched. To enable launching, right-click, then:

<b>Enable "Allow Launching"</b> Tiny Top-left corner Top-right corner Undo Unmount Unstack This Type Use Nemo to open folders Use dark text in icon labels Project-Id-Version: gtk4-ding
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2023-03-05 15:42+0000
Last-Translator: Anonymous <noreply@weblate.org>
Language-Team: Finnish <https://hosted.weblate.org/projects/gtk4-desktop-icons-ng/gtk4-ding-pot/fi/>
Language: fi
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: Weblate 4.16.2-dev
X-Launchpad-Export-Date: 2021-04-08 06:16+0000
 
<b>Kytke päälle valinta "Salli tiedoston suoritus ohjelmana"</b> 
<b>Aseta oikeudet kohdassa "Muut oikeudet", joko "Vain luku" tai "Ei mikään"</b> "${programName}" tarvitaan työpöytäkuvakkeisiin Toiminta käynnistettäessä ohjelmaa työpöydältä Merkitse linkit tiedostoihin symbolilla Lisää uudet tallennusvälineet näytön toiselle puolelle Salli käynnistäminen Aina Järjestä kuvakkeet Kysy mitä tehdään Vasen alakulma Oikea alakulma Rikkinäinen työpöytätiedosto Rikkinäinen linkki Kansiota ei voi lähettää sähköpostitse Tiedostoa ei voitu avata, sillä se on rikkinäinen linkki Peru Vaihda taustakuvaa… Napsautustapa tiedostojen avaamiselle Sulje Komentoa ei löydy Yhteiset ominaisuudet Pakkaa {0} tiedosto Pakkaa {0} tiedostoa Kopioi Leikkaa Poista pysyvästi Näytön asetukset Näytä tiedoston sisältö Älä salli käynnistämistä Kaksoisnapsautus Irrota Tyhjennä roskakori Pura tänne Pura… Tiedoston nimi Etsi tiedostoja työpöydällä Kansion nimi Tämä työpöytäkuvakkeiden toiminnallisuus vaatii ohjelman"${programName}" asentamisen järjestelmään. Korosta pudotuspaikka vedettäessä ja pudotettaessa Koti Vialliset oikeudet työpöytätiedostossa Suuri Käynnistä tiedosto Käynnistä erillisnäytönohjainta käyttäen Vain paikalliset tiedostot Siirrä roskakoriin Ei koskaan Uusi asiakirja Uusi kansio Uusi kansio jossa {0} kohde Uusi kansio jossa {0} kohdetta Uusien kuvakkeiden asettelu OK Avaa Avaa kaikki toisella sovelluksella... Avaa kaikki... Avaa toisella sovelluksella Avaa päätteessä Liitä Ominaisuudet Tee uudelleen Nimeä uudelleen Nimeä uudelleen… Komentosarjat Valitse Valitse kaikki Valitse purkamiskohde Valinta sisältää kansion, pakkaa kansio tiedostoksi ensin. Lähetä… Asetukset Jaa asetukset Nautilus-tiedostonhallinnan kanssa Näytä kaikki Tiedostot-sovelluksessa Näytä valikkokohde pysyvästi poistamiselle Näytä erilliset tallennusvälineet työpöydällä Näytä piilotiedostot Näytä pienoiskuvat Näytä tiedostonhallinnassa Näytä verkkoasemat työpöydällä Näytä kotikansio työpöydällä Näytä roskakorin kuvake työpöydällä Yksi napsautus Työpöytäkuvakkeiden koko Pieni Pinoa tämäntyyppiset Tavallinen Tällä .desktop-tiedostolla on väärät oikeudet. Naksauta oikealla hiiren napilla ja valitse Ominaisuudet, sitten:
 Tässä .desktop-tiedostossa on virheitä, tai se osoittaa ohjelmaan, jolla on puutteelliset oikeudet. Sitä ei voi suorittaa.

	<b>Muokkaa tiedostoa asettaaksesi oikea suoritettava ohjelma.<b> Tämä .desktop-tiedosto ei ole luotettu, ja sitä ei voi käynnistää. Salliakseksi käynnistämisen naksauta hiiren oikealla napilla, sitten:

<b>Valitse "Salli käynnistäminen"</b> Erittäin pieni Vasen yläkulma Oikea yläkulma Kumoa Irrota osio Älä pinoa tämäntyyppisiä Käytä Nemoa kansioiden avaamiseen Käytä tummaa tekstiä kuvakkeiden selitteissä 