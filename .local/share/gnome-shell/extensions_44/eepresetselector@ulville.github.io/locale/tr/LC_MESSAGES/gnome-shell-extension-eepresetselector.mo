��          �      �       H  7   I  1   �     �     �     �  )   �     "     8  0   K  1   |     �  1   �     �  �     E   �  ;   E     �  !   �     �  "   �            =   ,  @   j     �  9   �  $   �                           
                        	              An error occurred while trying to get available presets An error occurred while trying to load the preset Cycle Input Presets Cycle Output Presets EasyEffects Preset Selector EasyEffects isn't available on the system Enter new shortcut… Keyboard Shortcuts Keyboard shortcut to cycle through input presets Keyboard shortcut to cycle through output presets New shortcut… This extension depends on EasyEffects to function Use Backspace to clear Project-Id-Version: 
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2023-06-05 04:40+0300
Last-Translator: 
Language-Team: 
Language: tr
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Poedit 3.3.1
 Mevcut ön ayarlar alınmaya çalışılırken bir hata meydana geldi Ön ayarı yüklemeye çalışırken bir hata meydana geldi Giriş Ön Ayarlarını Çevir Çıkış Ön Ayarlarını Çevir EasyEffects Ön Ayar Seçici EasyEffects sistemde mevcut değil Yeni kısayol gir… Klavye Kısayolları Giriş ön ayarları arasında dönmek için klavye kısayolu Çıkış ön ayarları arasında dönmek için klavye kısayolu Yeni kısayol… Bu uzantı çalışmak için EasyEffects'e bağımlıdır Temizlemek için Geri Silmeyi kullan 