��          �      ,      �     �  7   �  W   �           3     F     W  
   `     k     �     �     �     �     �     �  ;   �  $  1     V  p   c  �   �     \     y  #   �     �     �  ?   �  7   (  Q   `  #   �  (   �     �  A     c   T                  
                                                     	       Camera Force the icon to move to right side of the status area Found this useful?
<a href="https://paypal.me/stuartahayhurst">Consider donating</a> :) GNOME 43+ settings GNOME 44+ settings General settings Location Microphone Move status icon right Privacy Settings Privacy Settings Menu Indicator Reset settings Reset to defaults Settings Use quick settings menu Use the system quick settings area, instead of an indicator Project-Id-Version: privacy-menu-extension
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2022-10-09 02:20+0700
Last-Translator: Vyacheslav Kostromin <ikibastusloh1337@gmail.com>
Language-Team: 
Language: ru
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
 Камера Принудительно переместить значок вправо к области состояния Нашли это полезным?
<a href="https://paypal.me/stuartahayhurst">Подумайте о пожертвовании</a> :) Настройки GNOME 43+ Настройки GNOME 44+ Основные настройки Местоположение Микрофон Переместить значок статуса вправо Настройки конфиденциальности Индикатор меню настроек конфиденциальности Сбросить настройки Сбросить по умолчанию Настройки Использовать меню быстрых настроек Использовать меню быстрых настроек вместо индикатора 