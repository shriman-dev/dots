��          �      \      �     �  7   �  W        h     {     �     �  *   �     �  
   �     �     
          #     C     R     d     m  ;   �  -  �  
   �  B   �  _   =     �     �     �     �  +     	   1  	   ;      E     f     n  4   �     �  &   �     �  %     P   *                                                                  	             
                    Camera Force the icon to move to right side of the status area Found this useful?
<a href="https://paypal.me/stuartahayhurst">Consider donating</a> :) GNOME 43+ settings GNOME 44+ settings General settings Group quick settings Group quick settings together, into a menu Location Microphone Move status icon right Privacy Privacy Settings Privacy Settings Menu Indicator Reset settings Reset to defaults Settings Use quick settings menu Use the system quick settings area, instead of an indicator Project-Id-Version: privacy-menu-extension
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2023-06-26 19:52+0100
Last-Translator: Albano Battistella <albanobattistella@>
Language-Team: Italian <LL@li.org>
Language: it
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
 Fotocamera Forza lo spostamento dell'icona sul lato destro dell'area di stato L'hai trovato utile?
<a href="https://paypal.me/stuartahayhurst">Considera una donazione</a> :) Impostazioni di GNOME 43+ Impostazioni di GNOME 44+ Impostazioni generali Impostazioni rapide di gruppo Raggruppa le impostazioni rapide in un menu Posizione Microfono Sposta l'icona di stato a destra Privacy Impostazioni Privacy Indicatore del menu delle impostazioni sulla privacy Ripristina impostazioni Ripristina le impostazioni predefinite Impostazioni Usa il menu delle impostazioni rapide Utilizzare l'area delle impostazioni rapide del sistema, invece di un indicatore 