��    _                   
   	  
             $     A  T   C     �     �     �     �  &   �     	     	     	     ,	     A	  
   N	     Y	     l	  '   �	     �	     �	  ;   �	     
     
     0
     P
     U
     d
     w
     �
     �
     �
     �
     �
     �
     �
     �
     �
     �
     �
     �
          (     D     X     h     �     �     �  
   �     �     �     �     �     �     �     �     
     $     =     P     l     {     �  	   �  4   �     �  �   �  ,   �     �     �     �     �          +     :  ]   B  ~   �  �     R   �  �   K  �     &   �     �       	   #     -     4     @     F     N     V     i  @  �  
   �     �     �  *   �       _        n     �     �     �  5   �     �       !        4     N     [     i     �  2   �     �     �  =   �     +  "   K  $   n     �     �     �     �     �     �     �                    )  	   9  	   C     M     Q  -   X     �  "   �     �     �     �               )     1     =  	   C     M     Y     k     �     �     �     �     �  /   �                @     O  9   ]  "   �  �   �  +   N  9   z  
   �     �     �     �            s   %  �   �  �   '  R   �  �     �   �  '   �     �       
   )     4     ;  	   G     Q     Y     a  "   t     ,   _   F       ^   @      X           W      P   ]   /      D   $       M   
      ?       E   [   +      )   1       -              O       K                 A   R   \   Q   3      5      =       T       ;      2      &   (   4      >   6      #   <       .   J   "   G           L   Y          0   U   Z           '                            %   I   B              9   8   	       7   V   C   S          !       :               *             N         H                    
 - Name:  
 - Port:   Sec  sec. delay before recording 0 <span foreground="red">No Caps selected, please select one from the caps list</span> Add code Alpha channel Both  [ESC + Default] Command post-recording Could not load the preferences UI file Credits Custom Custom GStreamer Pipeline Default audio source Default only Delay Time Destination folder Draw cursor on screencast ERROR RECORDER - See logs for more info ESC only EasyScreenCast EasyScreenCast -> Recording in progress / Seconds passed :  Enable keyboard shortcut Enable verbose debug Execute command after recording File File container File name template File resolution Frames Per Second GStreamer Pipeline Height How to contribute? Info Left-Bottom Left-Top Margin X Margin Y N/A Native Native area resolution No GSP description
 No Gstreamer pipeline found No WebCam recording No audio source No webcam device selected Not any Official doc Options Percentage Pixel Position Preset Preset helper Put the webcam in the corner Quality Record a selected area Record a selected monitor Record a selected window Record all desktop Recording status indicators Reporting bugs Restore default options Right-Bottom Right-Top Select an area for recording or press [ESC] to abort Select the destination folder Select the folder where the file is saved, if not specific a folder  the file will be saved in the $XDG_VIDEOS_DIR if it exists, or the home directory otherwise. Show a border around the area being recorded Show alerts and notifications Size Start Recording Start recording Start recording immediately Stop recording Support The extension does NOT handle the webcam and audio when you use a custom gstreamer pipeline.
 The filename which may contain some escape sequences - %d and %t will be replaced by the start date and time of the recording. These words will be replaced
 _fpath = the absolute path of the screencast video file.
_dirpath = the absolute path of destination folder for the screencast video file.
_fname = the name of the screencast video file. This extension simplifies the use of the 
screen recorder  included in gnome shell This option enable more debug message, to view these run this command into a terminal:
$ journalctl --since=today --no-pager | grep js
$ dbus-monitor "interface=org.gnome.Shell.Screencast" This option enable more debug message, to view these run this command into a terminal:
$ journalctl /usr/bin/gnome-session --since=today | grep js
$ dbus-monitor "interface=org.gnome.Shell.Screencast" This software is licensed under GPL v3 Type of unit of measure Unspecified webcam Version:  WebCam WebCam Caps Width Wiki #1 Wiki #2 With a translation keep original aspect ratio Project-Id-Version: 
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2017-10-05 09:29+0200
Last-Translator: idn <iacopodeenosee@gmail.com>
Language-Team: 
Language: it
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Poedit 2.0.4
Plural-Forms: nplurals=2; plural=(n != 1);
 
 - Nome:  
 - Porta:   Sec  sec. di ritardo prima della registrazione 0 <span foreground="red">Nessuna Caps selezionata, per favore selezionarne una dalla lista</span> Migliorando il codice Canale Alpha Entrambi [ESC + Default] Comando post-registrazione Non è possibile caricare il file UI delle preferenze Crediti Personalizzata GStreamer Pipeline personalizzata Sorgente Audio di default Solo Default Tempo ritardo Cartella di destinazione Registra cursore nel screencast ERRORE RECORDER - visiona i logs per maggiori info Solo ESC EasyScreenCast EasyScreenCast -> Registrazione in corso / Secondi passati :  Abilita scorciatoia da tastiera Abilita maggiori messaggi di debug Esegui comando dopo la registrazione File Tipo di file Modello del nome del file Risoluzione del file Frames Per Second GStreamer Pipeline Altezza Come contribuire? Info Basso a sinistra Alto a sinistra Margine X Margine Y N/A Nativa Risoluzione nativa dell'area di registrazione Nessuna descrizione della GSP
 Nessuna GStreamer Pipeline trovata Non registrare la webcam Nessuna sorgente Audio Nessuna webcam selezionata Nessuno Doc ufficiale Opzioni Percentuale Pixel Posizione Predefiniti Lista predefiniti Metti la webcam nell'angolo in Qualità Area di Registrazione Seleziona monitor Seleziona finestra Tutto il desktop Stato degli indicatori durante la registrazione Segnalando bugs Ripristina le opzioni di default Basso a destra Alto a destra Seleziona l'area da registrare o premi [ESC] per annulare Seleziona cartella di destinazione Seleziona la cartella in cui verrà salavto il file, se non specificata il file verrà salvato in $XDG_VIDEOS_DIR se esiste, altrimenti nella home. Mostra perimetro dell'area di registrazione Mostra tempo di registrazione nella barra delle notifiche Dimensioni Inizia Registrazione Inizia Registrazione Inizia subito la registrazione Ferma Registrazione Supporto Questa estensione NON gestisce il flusso Audio e della Webcam quando si usa una gstreamer pipeline personalizzata.
 Il nome del file può contenere dei caratteri speciali, %d e %t questi verranno sostituiti con la data e l'ora di inizio della registrazione. Queste parole verrano sostituite
 _fpath = percorso assoluto del file.
_dirpath = percorso assoluto della cartella contenente il file.
_fname = nome del file. This extension simplifies the use of the 
screen recorder  included in gnome shell Questa opzione abilità maggiori messaggi di debug, per visionarli esegui questi commandi nel terminale:
$ journalctl /usr/bin/gnome-session --since=today | grep js 
$ dbus-monitor "interface=org.gnome.Shell.Screencast" Questa opzione abilità maggiori messaggi di debug, per visionarli esegui questi commandi nel terminale:
$ journalctl /usr/bin/gnome-session --since=today | grep js 
$ dbus-monitor "interface=org.gnome.Shell.Screencast" Questo software è sotto licenza GPL v3 Tipo di unità di misura Webcam non specificata Versione:  WebCam WebCam Caps Larghezza Wiki #1 Wiki #2 Con una traduzione mantieni l' aspect-ratio originale 