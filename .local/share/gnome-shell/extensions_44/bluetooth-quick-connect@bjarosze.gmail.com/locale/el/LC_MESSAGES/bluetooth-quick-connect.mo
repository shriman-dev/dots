��          �      �       0     1     I      \     }     �  !   �  &   �  C   �  $   B  %   g  (   �     �  "  �  '   �       S   #  l   w  C   �  J   (  Z   s  �   �  h   k  ^   �  l   3     �                        	                   
               Bluetooth Quick Connect Bluetooth Settings Checking idle interval (seconds) Debug mode (restart required) Disable bluetooth if idle Enable bluetooth when menu opened Error trying to execute "bluetoothctl" Keep the menu open after toggling the connection (restart required) Show battery icon (restart required) Show battery value (restart required) Show reconnect button (restart required) Wait Project-Id-Version: 
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2023-03-23 15:46+0200
Last-Translator: 
Language-Team: 
Language: el
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
X-Generator: Poedit 3.1.1
 Γρήγορη σύνδεση Bluetooth Ρυθμίσεις Bluetooth Έλεγχος διαστήματος αδράνειας (δευτερόλεπτα) Λειτουργία εντοπισμού σφαλμάτων (απαιτείται επανεκκίνηση) Απενεργοποίηση του bluetooth σε αδράνεια Ενεργοποίηση bluetooth όταν ανοίγει το μενού Σφάλμα κατά την προσπάθεια εκτέλεσης του "bluetoothctl" Διατηρήστε το μενού ανοιχτό μετά την εναλλαγή της σύνδεσης (απαιτείται επανεκκίνηση) Εμφάνιση εικονιδίου μπαταρίας (απαιτείται επανεκκίνηση) Εμφάνιση τιμής μπαταρίας (απαιτείται επανεκκίνηση) Εμφάνιση κουμπιού επανασύνδεσης (απαιτείται επανεκκίνηση) Αναμονή 