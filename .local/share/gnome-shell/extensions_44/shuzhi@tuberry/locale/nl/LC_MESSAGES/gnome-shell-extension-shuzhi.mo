��    "      ,  /   <      �     �     �  <        H     N     S  #   Z     ~     �     �     �     �     �  
   �     �     �     �     �     �     �  ,   �     ,     ;     D     T     [     d  	   q     {     �     �     �     �  �  �     -     9  G   P     �     �     �  :   �  	   �     �     �               6     =     I     O     a     g     n  
   �  *   �     �  
   �     �     �  
   �     �  
             !  	   '     1     8                                           	      !   "                                
                                                                          Auto Auto refresh Background color, “Auto” means sync with the Night Light Blobs Both Clouds Command to generate the center text Copy Dark Dark sketches Default style Enable systray Height Horizontal Light Light sketches Motto Ovals Picture location Refresh Required only if the resolution is incorrect Set resolution Settings Show color name Sketch Sketch:  Text command Text font Text orientation Trees Vertical Waves Width Project-Id-Version: gnome-shell-extension-shuzhi 15
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2021-09-14 14:32+0200
Last-Translator: Heimen Stoffels <vistausss@fastmail.com>
Language-Team: Dutch <vistausss@fastmail.com>
Language: nl
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
X-Generator: Poedit 3.0
 Automatisch Automatisch vernieuwen De achtergrondkleur - ‘Automatisch’ = synchroniseren met Nachtlicht Klodders Beide Wolken Opdracht waarmee de gecentreerde tekst moet worden gemaakt Kopiëren Donker Donkere tekeningen Standaardstijl Systeemvakpictogram tonen Hoogte Horizontaal Licht Lichte tekeningen Motto Ovalen Afbeeldingslocatie Vernieuwen Alleen vereist als de resolutie onjuist is Resolutie instellen Voorkeuren Kleurnaam tonen Tekening Tekening:  Tekstopdracht Lettertype Tekstrichting Bomen Verticaal Golven Breedte 