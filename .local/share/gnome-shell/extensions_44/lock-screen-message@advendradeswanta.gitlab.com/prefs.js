'use strict';

const Gio = imports.gi.Gio;
const Gtk = imports.gi.Gtk;

const ExtensionUtils = imports.misc.extensionUtils;
const Me = ExtensionUtils.getCurrentExtension();

const shellVersion = imports.misc.config.PACKAGE_VERSION;


var Prefs = class {
    constructor(window = null) {
        this.settings = ExtensionUtils.getSettings();

        // Create a parent widget that we'll return from this function
        this.prefsWidget = new Gtk.Box({
            margin_start: 24,
            margin_end: 24,
            margin_top: 24,
            margin_bottom: 24,
            spacing: 24,
            orientation: Gtk.Orientation.VERTICAL,
            hexpand: true,
            valign: Gtk.Align.CENTER,
            visible: true
        });

        // Title
        let title = new Gtk.Label({
            label: `<span size="16384"><b>${Me.metadata.name} Preferences</b></span>`,
            halign: Gtk.Align.FILL,
            justify: Gtk.Justification.CENTER,
            use_markup: true,
            wrap: true,
            visible: true
        });
        this.prefsWidget.append(title);

        // About Extension
        let infoLabel = new Gtk.Label({
            label: "<b>This extension let's you add your message to the lock screen (unlockDialog)</b>",
            halign: Gtk.Align.FILL,
            justify: Gtk.Justification.CENTER,
            use_markup: true,
            wrap: true,
            visible: true
        });
        this.prefsWidget.append(infoLabel);

        // Message
        let messageEntry = new Gtk.Entry({
            text: this.settings.get_string('message'),
            placeholder_text: "Enter a Message",
            max_length: 480,
            hexpand: true,
            visible: true
        });
        this.prefsWidget.append(messageEntry);

        // About Extension
        let aboutLabel = new Gtk.Label({
            label:
                `<b>Version ${Me.metadata.version}</b>\n` +
                `Maintained by: AdvendraDeswanta`,
            halign: Gtk.Align.CENTER,
            justify: Gtk.Justification.CENTER,
            use_markup: true,
            visible: true
        });
        this.prefsWidget.append(aboutLabel);

        // Bind the messageWidget to the `message` key
        this.settings.bind(
            'message',
            messageEntry,
            'text',
            Gio.SettingsBindFlags.DEFAULT
        );

        if (shellVersion >= 42) {
            const Adw = imports.gi.Adw;

            let mainPage = Adw.PreferencesPage.new();
            mainPage.set_name('main-page');

            let mainGroup = Adw.PreferencesGroup.new();
            mainGroup.add(this.prefsWidget);
            mainPage.add(mainGroup);

            let clamp = mainGroup.get_parent().get_parent();
            clamp.set_valign(Gtk.Align.CENTER);

            // Set default window size
            window.default_width = 480;
            window.default_height = 320;

            window.add(mainPage);
        } else {
            // Adaptive content
            this.prefsWidget.connect('realize', () => {
                let root = this.prefsWidget.get_root();
                let margin = 0.2;

                // Set default window size
                root.default_width = 480;
                root.default_height = 320;

                this.prefsWidget.set({
                    margin_start: root.default_width * margin,
                    margin_end: root.default_width * margin,
                });
                root.connect('notify::default-width', () => {
                    this.prefsWidget.set({
                        margin_start: root.default_width * margin,
                        margin_end: root.default_width * margin,
                    })
                    // Avoid spawning more warn messages by calling measure()
                    root.measure(Gtk.Orientation.HORIZONTAL, -1)
                });
            });
        }
    }

    getWidget() {
        return this.prefsWidget;
    }
}

function init() { }

function fillPreferencesWindow(window) {
    return new Prefs(window)
}

function buildPrefsWidget() {
    return new Prefs().getWidget();
}
