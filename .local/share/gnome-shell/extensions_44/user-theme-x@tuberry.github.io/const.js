// vim:fdm=syntax
// by tuberry
/* exported System Field */
'use strict';

var System = {
    SHELL:  'name',
    GTK:    'gtk-theme',
    ICONS:  'icon-theme',
    LPIC:   'picture-uri',
    COLOR:  'color-scheme',
    CURSOR: 'cursor-theme',
    DPIC:   'picture-uri-dark',
};

var Field = {
    GTK:     'x-gtk',
    COLOR:   'x-color',
    ICONS:   'x-icons',
    NIGHT:   'x-night',
    SHELL:   'x-shell',
    CURSOR:  'x-cursor',
    NGTK:    'x-gtk-night',
    PAPER:   'x-wallpaper',
    STYLE:   'x-stylesheet',
    NCOLOR:  'x-color-night',
    NICONS:  'x-icons-night',
    NSHELL:  'x-shell-night',
    NCURSOR: 'x-cursor-night',
};
