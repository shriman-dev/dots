��          �   %   �      P     Q     X     w     }     �     �     �     �     �     �     �  
   �     �     
               '     7     ?     H     ^     d     p     v     |  �  �  
   *  6   5     l     u     �  [   �  _   �  ]   ^     �     �  	   �  (   �               8  ,   E     r     �  
   �  *   �     �  *   �       
   (     3        
                                               	                                                                      Center Click on the eye to turn it on Color Default Enable Enable left click coloring Enable middle click coloring Enable right click coloring Eye Eyelid Left Left click Line Thickness Location Margin Middle click Mouse indicator Opacity Position Refresh interval (ms) Right Right click Round Shape Size Project-Id-Version: EyeExtendedExtension
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2022-11-28 10:27-0500
Last-Translator: Philippe-André Akue <pakueamb@gmail.com>
Language-Team: 
Language: ru
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=3; plural=(n%10==1 && n%100!=11 ? 0 : n%10>=2 && n%10<=4 && (n%100<12 || n%100>14) ? 1 : 2);
X-Generator: Poedit 3.1.1
 Центр Для активации нажмите на глаз Цвет По умолчанию Отображать Включить раскрашивание при нажатии левой кнопкой Включить раскрашивание при нажатии средней кнопкой Включить раскрашивание при нажатии правой кнопкой Глаз Глаз Слевa Нажатии левой кнопкой Толщина линии Местоположение Отступ Нажатии средней кнопкой Индикатор мыши Непрозрачность Место Интервал рисования (мс) Справа Нажатии правой кнопкой Круглый Форма Размер 