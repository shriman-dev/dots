��    Z      �     �      �     �     �  G   �  	             +     D     _     y     �     �     �     �     �     �     �     �  
   �     	     	  3   ,	     `	  	   w	  
   �	  6   �	  	   �	     �	     �	     �	     
     
  
   
     (
     -
     ;
     H
     Q
     ^
  	   {
     �
     �
  1   �
  3   �
                  (   /     X     h     z          �     �     �     �     �     �     �     �     �     �          #     1     F     W     f     �     �  1   �     �     �     �  %   
  %   0  !   V     x     �     �  	   �     �     �     �     �     �          "     9     I  �  Z     �     �  �     !   �     �     �  $   �       
   0     ;  /   K     {     �  I   �  *   �  \        v  
   �  a   �  *   �  y   !  @   �     �     �  �        �  -   �  '   �  >     0   @  
   q  /   |     �  %   �  3   �          *  :   H     �  4   �     �  h   �  |   O     �     �  %   �  T     #   n  *   �     �     �     �     �       I        b  %   u  #   �  #   �     �  9   �  0   0  0   a  ?   �  :   �  <     B   J  >   �  a   �  k   .     �  $   �  *   �  e   �  m   e  U   �  *   )  =   T  +   �     �     �  #   �  -     (   0  Q   Y  N   �  O   �  <   J      �                 C   &      %             @   B   	          6          A   S      0   V      5         #   D   G       )       -       ,          Y       F   R           U                  3   P      9             ?   !               T   +       *   O           >   W         :         Q         8          ;   
   (       7   =           1   I            N   K       <   Z           /               H   M          L          X   J   "   E   .           '       4   2       $       About Actions Adds a badge counter to the App Icon based on GNOME shell notifications App Icons App Icons Taskbar App Icons Taskbar GitLab App Icons Taskbar Settings App Icons Taskbar Version Bottom Center Click Actions Cycle Cycle Windows Dance Urgent App Icons Donate via PayPal Focused Indicator Color GNOME Version Git Commit Hide Window Previews Delay Hover Actions Hovering a window preview will focus desired window Icon Desaturate Factor Icon Size Icon Style Icon themes may not have a symbolic icon for every app Indicator Indicator Location Isolate Monitors Isolate Workspaces Launch New Instance Left Left Click Load Load Settings Middle Click Minimize Multi-Dashes Multi-Window Indicator Style No Action Notification Badges Count OS Name Offset the position within the above selected box Opacity of non-focused windows during a window peek Panel Panel Height Panel Location Panel menu button that shows focused app Position Offset Position in Panel Quit Raise Regular Requires Unity API Right Running Indicator Color Save Save Settings Scroll Action Scroll Actions Settings Shift + Middle Click Show Activities Button Show All Apps Show App Menu Button Show Apps Button Show Favorites Show Panels on All Monitors Show Running Apps Show Window Previews Delay Show running apps and favorites on the main panel Symbolic Taskbar Badges Taskbar Behavior Time in ms to hide the window preview Time in ms to show the window preview Time in ms to trigger window peek Toggle / Cycle Toggle / Cycle + Minimize Toggle / Preview Tool-Tips Top Unity Badges Count Unity Progress Bars Window Peeking Window Peeking Delay Window Peeking Opacity Window Preview Options Window Previews Windowing System Project-Id-Version: 
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2023-05-13 10:06+1000
Last-Translator: Ser82-png <asvmail.as@gmail.com>
Language-Team: 
Language: ru
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=3; plural=(n%10==1 && n%100!=11 ? 0 : n%10>=2 && n%10<=4 && (n%100<12 || n%100>14) ? 1 : 2);
X-Generator: Poedit 3.0.1
 О расширении Действия Добавляет счетчик значков к значку приложения на основе уведомлений GNOME shell Значки приложений App Icons Taskbar App Icons Taskbar на GitLab Настройки App Icons Taskbar Версия App Icons Taskbar Снизу В центре Действия при щелчке мышью По кругу Окна по кругу Танцующие значки актуальных приложений Пожертвовать через PayPal Цвет индикатора приложения, находящегося в фокусе Версия GNOME Git Commit Задержка до скрытия окна предварительного просмотра Действия при наведении Наведение на окно предварительного просмотра выведет нужное окно Коэффициент обесцвечивания значка Размер значка Стиль значков Темы значков могут содержать символические значки не для всех приложений Индикатор Расположение индикатора Изолировать мониторы Изолировать рабочие пространства Запустить новый экземпляр Слева Щелчок левой кнопкой мыши Загрузить Загрузить настройки Щелчок средней кнопкой мыши Свернуть Мульти-чёрточки Стиль многооконного индикатора Нет действий Счётчик значков уведомлений Название ОС Сместить положение в пределах диапазона выбранного выше Непрозрачность несфокусированных окон во время быстрого просмотра Панель Высота панели Расположение панели Кнопка меню приложения, находящегося в фокусе Смещение положения Расположение на панели Завершить Поднять Обычные Требуется Unity API Справа Цвет индикатора запущенного приложения Сохранить Сохранить настройки Действие прокрутки Действия прокрутки Настройки Shift + Щелчок средняя кнопка мыши Показывать кнопку «Обзор» Показывать все приложения Показывать кнопку меню приложения Показывать кнопку «Приложения» Показывать избранные приложения Показывать панель на всех мониторах Показывать запущенные приложения Задержка при показе окна предварительного просмотра Показ запущенных и избранных приложений на главной панели Символьные Значки панели задач Поведение панели задач Время в мс для скрытия окна предварительного просмотра Время в мс для отображения окна предварительного просмотра Время в мс для запуска быстрого просмотра окна Переключение / По кругу Переключение / По кругу + Свернуть Переключение / Просмотр Подсказки Сверху Счётчик значков Unity Индикатор выполнения Unity Быстрый просмотр окна Задержка при выводе быстрого просмотра окна Непрозрачность при быстром просмотре окна Параметры окна предварительного просмотра Окна предварительного просмотра Оконная система 