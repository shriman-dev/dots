��    )      d  ;   �      �     �     �  >   �     �               &     3     ?     N     Z     h          �     �     �     �     �     �  
   �     �            '        7     L     b          �     �     �     �     �     �     �  
   �  
                  (  F  /     v  -   {  L   �     �               "     /     <     Q     ^     l     �     �     �     �     �     �     �  
   	     	     -	     5	  9   D	     ~	     �	  )   �	      �	  !   
     *
     6
     D
     R
     f
     z
  	   �
     �
     �
     �
  
   �
           )          $          	   #          !            '                                                             (                        
      "                        &                    %        About Add a clock to the desktop! All Desktop Clock settings will be reset to the default value. Background Color Border Color Border Radius Border Width Date - Time Date Font Size Date Format Desktop Clock Desktop Clock Settings Desktop Clock Version Display In-Line Enable Background Enable Border Format Guide GNOME Version General Options Git Commit Label Order Lock OS Please confirm reset of clock position. Reset Clock Position Reset Clock Position? Reset Desktop Clock Settings Reset all Settings Reset all settings? Session Type Settings Shadow Color Shadow Spread Shadow X Offset Shadow Y Offset Text Color Text Style Time - Date Time Font Size Unlock Project-Id-Version: 
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2022-06-18 23:31+0200
Last-Translator: Onno Giesmann <nutzer3105@gmail.com>
Language-Team: 
Language: de
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
X-Generator: Poedit 3.0.1
 Info Fügen Sie Ihrem Schreibtisch eine Uhr hinzu! Alle Desktop Clock-Einstellungen werden auf die Vorgabewerte zurückgesetzt. Hintergrundfarbe Rahmenfarbe Eckenabrundung Rahmenbreite Datum - Zeit Schriftgröße Datum Datumsformat Desktop Clock Desktop Clock-Einstellungen Desktop Clock-Version In einer Reihe anzeigen Hintergrund einschalten Rahmen einschalten Formatleitfaden GNOME-Version Allgemeine Optionen Git-Commit Feldsortierung Sperren Betriebssystem Bitte bestätigen Sie das Zurücksetzen der Uhr-Position. Position der Uhr zurücksetzen Position der Uhr zurücksetzen? Desktop Clock-Einstellungen zurücksetzen Alle Einstellungen zurücksetzen Alle Einstellungen zurücksetzen? Sitzungsart Einstellungen Schattenfarbe Schattenausbreitung Schattenversatz (X) Schattenversatz (Y) Textfarbe Textdarstellung Zeit - Datum Schriftgröße Uhrzeit Entsperren 