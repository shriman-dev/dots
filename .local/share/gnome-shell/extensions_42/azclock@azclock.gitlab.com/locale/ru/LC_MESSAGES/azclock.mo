��    )      d  ;   �      �     �     �  >   �     �               &     3     ?     N     Z     h          �     �     �     �     �     �  
   �     �            '        7     L     b          �     �     �     �     �     �     �  
   �  
                  (  �  /     �  7   �  r   	     |     �     �     �     �  @   �     )	     ?	      M	     n	  "   �	     �	     �	  *   �	     
  #   
  
   B
  ,   M
     z
     �
  \   �
  4   �
  M   ,  )   z  *   �  A   �          %     8     J  '   d  '   �     �     �     �  F   �     ?           )          $          	   #          !            '                                                             (                        
      "                        &                    %        About Add a clock to the desktop! All Desktop Clock settings will be reset to the default value. Background Color Border Color Border Radius Border Width Date - Time Date Font Size Date Format Desktop Clock Desktop Clock Settings Desktop Clock Version Display In-Line Enable Background Enable Border Format Guide GNOME Version General Options Git Commit Label Order Lock OS Please confirm reset of clock position. Reset Clock Position Reset Clock Position? Reset Desktop Clock Settings Reset all Settings Reset all settings? Session Type Settings Shadow Color Shadow Spread Shadow X Offset Shadow Y Offset Text Color Text Style Time - Date Time Font Size Unlock Project-Id-Version: 
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2022-07-06 22:54+1000
Last-Translator: Ser82-png <asvmail.as@gmail.com>
Language-Team: 
Language: ru
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=3; plural=(n%10==1 && n%100!=11 ? 0 : n%10>=2 && n%10<=4 && (n%100<12 || n%100>14) ? 1 : 2);
X-Generator: Poedit 2.3
 О приложении Добавьте часы на рабочий стол! Все настройки Desktop Clock будут сброшены на значения по умолчанию. Цвет фона Цвет рамки Радиус рамки Ширина рамки Дата - Время Размер шрифта для отображения даты Формат даты Desktop Clock Настройки Desktop Clock Версия Desktop Clock Разместить в линию Включить фон Включить рамку Руководство по формату Версия GNOME Основные параметры Git Commit Порядок показа надписей Заблокировать ОС Пожалуйста, подтвердите сброс расположения часов. Сбросить расположение часов Сбросить расположение часов до исходного? Сброс настроек Desktop Clock Сбросить все настройки Сбросить все настройки до исходных? Тип сессии Настройки Цвет тени Размытие тени Смещение тени по оси X Смещение тени по оси Y Цвет текста Стиль текста Время - Дата Размер шрифта для отображения времени Разблокировать 