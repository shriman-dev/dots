��    )      d  ;   �      �     �     �  >   �     �               &     3     ?     N     Z     h          �     �     �     �     �     �  
   �     �            '        7     L     b          �     �     �     �     �     �     �  
   �  
                  (  S  /     �  $   �  9   �     �  
   �                    )     @     L     [     t     �     �     �  
   �     �     �  
   �     �     	     	  +    	     L	  2   c	     �	      �	  9   �	  
   
  
   
     #
     0
     D
     ]
  
   v
  
   �
     �
     �
     �
           )          $          	   #          !            '                                                             (                        
      "                        &                    %        About Add a clock to the desktop! All Desktop Clock settings will be reset to the default value. Background Color Border Color Border Radius Border Width Date - Time Date Font Size Date Format Desktop Clock Desktop Clock Settings Desktop Clock Version Display In-Line Enable Background Enable Border Format Guide GNOME Version General Options Git Commit Label Order Lock OS Please confirm reset of clock position. Reset Clock Position Reset Clock Position? Reset Desktop Clock Settings Reset all Settings Reset all settings? Session Type Settings Shadow Color Shadow Spread Shadow X Offset Shadow Y Offset Text Color Text Style Time - Date Time Font Size Unlock Project-Id-Version: 
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2022-09-23 20:25+0200
Last-Translator: Heimen Stoffels <vistausss@fastmail.com>
Language-Team: Dutch
Language: nl
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
X-Generator: Lokalize 22.08.1
 Over Voeg een klok toe aan uw bureaublad! Alle voorkeuren worden teruggezet op de standaardwaarden. Achtergrondkleur Kaderkleur Kaderstraal Kaderbreedte Datum - Tijd Tekstgrootte van datum Datumopmaak Bureaubladklok Bureaubladklokvoorkeuren Bureaubladklokversie Op één regel tonen Achtergrond tonen Kader tonen Zelfstudie GNOME-versie Algemene voorkeuren Git-commit Labelvolgorde Vergrendelen Besturingssysteem Bevestig het herstellen van de klokpositie. Kloklocatie herstellen Weet u zeker dat u de kloklocatie wilt herstellen? Standaardwaarden herstellen Alle standaardwaarden herstellen Weet u zeker dat u alle standaardwaarden wilt herstellen? Sessietype Voorkeuren Schaduwkleur Schaduwverspreiding Schaduw - X-verschuiving Schaduw - Y-verschuiving Tekstkleur Tekststijl Tijd - Datum Tekstgrootte van tijd Ontgrendelen 