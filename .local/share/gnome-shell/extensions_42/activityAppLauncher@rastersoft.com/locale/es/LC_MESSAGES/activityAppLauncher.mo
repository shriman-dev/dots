��          �      �       H     I  .   f  -   �  	   �     �     �     �  7   �  ;   4     p  )        �     �  y  �     A  M   ^  N   �  	   �  
             "  <   A  G   ~     �  .   �               
                                     	                       Add a border to the tooltip. Add a cathegory button to show Favorites apps. Add a cathegory button to show Frequent apps. Favorites Frequent Show tooltips. Show virtual desktops. Shows a tooltip with the full name and the description. Shows the virtual desktops when a category has been chosen. Size for icons Specify the size for the icons in pixels. Tooltips with border. Windows Project-Id-Version: 12
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2021-07-24 18:33+0200
Last-Translator: Sergio Costas <rastersoft@gmail.com>
Language-Team: Español; Castellano <rastersoft@gmail.com>
Language: es
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
X-Generator: Gtranslator 3.38.0
 Añadir un borde al tooltip. Añadir un botón en las categorías para mostrar las aplicaciones Favoritas. Añadir un botón en las categorías para mostrar las aplicaciones Frecuentes. Favoritas Frecuentes Mostrar tooltips. Mostrar escritorios virtuales. Mostrar un tooltip con el nombre completo y la descripción. Mostrar los escritorios virtuales cuando se ha escogido una categoría. Tamaño de los iconos Especifica el tamaño de los iconos en pixels. Tooltips con borde. Ventanas 