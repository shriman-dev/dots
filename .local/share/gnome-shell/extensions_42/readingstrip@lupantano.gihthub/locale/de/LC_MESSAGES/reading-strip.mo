��          |      �             !     5     I     \     l     |     �     �  
   �     �  N   �  H       X     k          �     �     �     �     �     �       s   
                                          	          
       <b>Color Focus</b>: <b>Focus strip</b>: <b>Opacity</b> (%) <b>Profile</b>: <b>Size</b> (%) <b>Strip Color</b>: <b>Vertical Strip</b>: Default Focus Mode Rules You can activate/deactive with <b>SUPER+CTRL+SPACE</b> 
or click on icon panel Project-Id-Version: 1.0
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2022-07-31 20:51+0200
Last-Translator: Luigi Pantano <nil>
Language-Team: Deutsch <nil>
Language: de
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Gtranslator 42.0
Plural-Forms: nplurals=2; plural=(n != 1)
 <b>Fokusfarbe</b>: <b>Fokusleiste</b>: <b>Trasparenz</b> (%) <b>Profile</b>: <b>Dicke</b> (%) <b>Farbeleiste</b>: <b>Senkrechter Leiste</b>: Ursprünglich Ohne Ablenkung Lineale Sie können aktivieren / deaktivieren, indem Sie <b>SUPER+CTRL+SPACE</b>
drücken oder auf das Panel-Symbol klicken 