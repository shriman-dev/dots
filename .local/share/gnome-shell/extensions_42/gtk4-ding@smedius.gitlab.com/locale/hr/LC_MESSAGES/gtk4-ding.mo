��    ,      |  ;   �      �     �     �     �     �                         *     ;     A     M     Z     h  	   m     w     }     �  
   �     �     �     �     �     �  
   �     �  	   �     �  
   �  #   
     .     7     E  '   Z  (   �  "   �  #   �     �               #     ,     1  ^  9     �     �     �     �     �     �     �     �                     0     A     Q     ]     m     t     �  	   �     �     �     �     �     �     �     �     �     	     
	  (   	     A	     J	     `	  (   u	  )   �	  (   �	  *   �	     
     2
     R
  
   W
     b
  
   k
         &   !       )   *   "          
                                 $   %                    #                  (                                +       	                ,                   '             Allow Launching Cancel Change Background… Command not found Copy Create Cut Delete permanently Display Settings Eject Empty Trash Extract Here Extract To... Home Icon size Large Move to Trash New Document New Folder OK Open Open With Other Application Open in Terminal Paste Properties Redo Rename… Select Send to... Set the size for the desktop icons. Settings Show in Files Show personal folder Show the personal folder in the desktop Show the personal folder in the desktop. Show the trash icon in the desktop Show the trash icon in the desktop. Show trash icon Size for the desktop icons Small Standard Undo Unmount Project-Id-Version: gtk4-ding
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2019-06-22 18:52+0200
Last-Translator: gogo <trebelnik2@gmail.com>
Language-Team: Croatian <hr@li.org>
Language: hr
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2019-03-27 09:36+0000
X-Generator: Poedit 2.2.1
 Dopusti pokretanje Odustani Promijeni pozadinu… Naredba nije pronađena Kopiraj Stvori Izreži Obriši trajno Postavke zaslona Izbaci Isprazni smeće Raspakiraj ovdje Raspakiraj u… Osobna mapa Veličina ikona Velike Premjesti u smeće Novi dokument Nova mapa U redu Otvori Otvori s drugom aplikacijom Otvori u Terminalu Zalijepi Svojstva Ponovi Preimenuj… Odaberi Pošalji u… Postavi veličinu ikona radne površine. Postavke Prikaži u Datotekama Prikaži osobnu mapu Prikaži osobnu mapu na radnoj površini Prikaži osobnu mapu na radnoj površini. Prikaži mapu smeća na radnoj površini Prikaži ikonu smeća na radnoj površini. Prikaži ikonu smeća Veličina ikona radne površine Male Standardne Poništi Odmontiraj 