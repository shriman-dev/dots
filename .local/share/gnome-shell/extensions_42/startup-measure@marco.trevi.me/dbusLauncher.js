/* dbusLauncher.js
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Author: Marco Trevisan <marco@ubuntu.com>
 *
 * SPDX-License-Identifier: GPL-2.0-or-later
 */

/* exported DBusLauncher */

const { GLib, Gio, Shell } = imports.gi;

const Config = imports.misc.config;

if (!String.prototype.replaceAll) {
    String.prototype.replaceAll = function(str, newStr) {
        return this.split(str).join(newStr);
    };
}

const ShellStartupMeasureIface = 'me.trevi.ShellAppStartupMeasure';
const ShellStartupMeasurePath = `/${ShellStartupMeasureIface.replaceAll('.', '/')}`
const ShellStartupMeasure = loadInterfaceXml(`${ShellStartupMeasureIface}.xml`);

// loads a xml file into an in-memory string
function loadInterfaceXml(filename) {
    const extension = imports.misc.extensionUtils.getCurrentExtension();
    const interfacesDir = extension.dir.get_child('interfaces-xml');
    const file = interfacesDir.get_child(filename);
    let [result, contents] = imports.gi.GLib.file_get_contents(file.get_path());

    if (result) {
        if (contents instanceof Uint8Array)
            contents = imports.byteArray.toString(contents);
        return `<node>${contents}</node>`;
    } else {
        throw new Error(`AppIndicatorSupport: Could not load file: ${filename}`);
    }
}

var DBusLauncher = class DBusLauncher {
    constructor() {
        this._dbusObject = Gio.DBusExportedObject.wrapJSObject(
            ShellStartupMeasure, this);
        try {
            this._dbusObject.export(Gio.DBus.session, ShellStartupMeasurePath);
        } catch (e) {
            logError(e, `Failed to export ${ShellStartupMeasurePath}`);
        }

        this._pendingInvocations = new Map();
        this._ownName = Gio.DBus.session.own_name(ShellStartupMeasureIface,
            Gio.BusNameOwnerFlags.NONE,
            null,
            () => this._lostName());
    }

    destroy() {
        if (this._ownName)
            Gio.DBus.session.unown_name(this._ownName);

        try {
            this._dbusObject.unexport();
        } catch (e) {
            logError(e, `Failed to unexport ${ShellStartupMeasurePath}`);
        }

        this._pendingInvocations.forEach((i, app) =>
            i.return_error_literal(Gio.DBusError,
                Gio.DBusError.FAILED,
                `${app.get_id()} not launched`));
        this._pendingInvocations.clear();

        this._dbusObject.run_dispose();
        delete this._dbusObject;
    }

    _lostName() {
        delete this._ownName;
    }

    _launchApp(id, timestamp=0, workspace=-1, gpuPref="app-pref") {
        const app = Shell.AppSystem.get_default().lookup_app(id);

        if (!app) {
            throw new GLib.Error(Gio.DBusError,
                Gio.DBusError.FILE_NOT_FOUND,
                `No application with ID ${id}`);
        }

        if (app.state !== Shell.AppState.STOPPED) {
            throw new GLib.Error(Gio.DBusError,
                Gio.DBusError.FAILED,
                `Application ${id} is already running`);
        }

        const [majorVersion, minorVersion] = Config.PACKAGE_VERSION.split('.');

        if (majorVersion >= 40 || (majorVersion >= 3 && minorVersion >= 38)) {
            app.launch(timestamp, workspace,
                Shell.AppLaunchGpu[gpuPref.toUpperCase().replaceAll('-', '_')]);
        } else {
            app.launch(timestamp, workspace,
                gpuPref.toLowerCase() === 'discrete');
        }

        return app;
    }

    LaunchAppFullAsync([id, timestamp, workspace, gpuPref], invocation) {
        try {
            this._launchApp(id, timestamp, workspace, gpuPref);
            invocation.return_value(null);
        } catch (e) {
            invocation.return_gerror(e);
        }
    }

    LaunchAppAsync([id], invocation) {
        try {
            this._launchApp(id);
            invocation.return_value(null);
        } catch (e) {
            invocation.return_gerror(e);
        }
    }

    _launchAppWaiting(invocation, ...args) {
        const app = this._launchApp(...args);
        if (this._pendingInvocations.has(app)) {
            throw new GLib.Error(Gio.DBusError,
                Gio.DBusError.FAILED,
                `Application ${id} has been already launched`);
        }
        this._pendingInvocations.set(app, invocation);
    }

    LaunchAppWaitingAsync([id], invocation) {
        try {
            this._launchAppWaiting(invocation, id);
        } catch (e) {
            invocation.return_gerror(e);
        }
    }

    LaunchAppWaitingFullAsync([id, timestamp, workspace, gpuPref], invocation) {
        try {
            this._launchAppWaiting(invocation, id, timestamp, workspace, gpuPref);
        } catch (e) {
            invocation.return_gerror(e);
        }
    }

    cancelLaunch(app) {
        const invocation = this._pendingInvocations.get(app);
        if (!invocation)
            return;

        this._pendingInvocations.delete(app);
        invocation.return_error_literal(Gio.DBusError,
            Gio.DBusError.FAILED,
            `${app.get_id()} not launched`);
    }

    emitAppStarted(app, appStartupTime) {
        const invocation = this._pendingInvocations.get(app);

        if (invocation) {
            this._pendingInvocations.delete(app);
            invocation.return_value(new GLib.Variant('(u)', [appStartupTime]));
        }

        this._dbusObject.emit_signal('AppStarted',
            new GLib.Variant('(su)', [app.get_id(), appStartupTime]));
    }
}
