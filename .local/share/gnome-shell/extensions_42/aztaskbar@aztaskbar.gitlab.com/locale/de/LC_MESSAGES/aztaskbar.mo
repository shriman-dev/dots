��    C      4  Y   L      �     �     �  	   �     �     �     �     �     �     �  	             -     ;  
   C     N     i  3   w     �  	   �  
   �  6   �  	             +     <     O  
   T  -   _     �     �  	   �     �  1   �  3   �     *     0     =     M     _     g     m     �     �     �     �     �     �     �     �     	  1   ,	     ^	  %   g	  %   �	  !   �	     �	     �	     �	  	   
     
     
     %
     4
     I
     `
     w
  $  �
     �     �     �     �     �     �     �             	   $  *   .     Y  	   g  
   q  -   |     �  K   �          "  	   0  I   :  	   �     �     �     �     �  
   �  5   �       #   (     L     Y  ;   \  G   �     �     �     �               !  '   (     P     ]     t     �     �     �  (   �  &   �  -     ;   <  
   x  :   �  4   �  -   �     !  '   <     d  
   z     �     �     �  "   �     �     �             =          (   :   '   ,       )   	   !   #                                         &                       ?         <                              /       5   8   ;   6   -   0          C             1   +   >      9      $   *   4   2   "               A   @          7      %          
          .           B   3    About Actions App Icons App Icons Taskbar Bottom Center Click Actions Cycle Cycle Windows Favorites Focused Indicator Color GNOME Version General Git Commit Hide Window Previews Delay Hover Actions Hovering a window preview will focus desired window Icon Desaturate Factor Icon Size Icon Style Icon themes may not have a symbolic icon for every app Indicator Indicator Location Isolate Monitors Isolate Workspaces Left Left Click Modify Left Click Action of Running App Icons Multi-Dashes Multi-Window Indicator Style No Action OS Offset the position within the above selected box Opacity of non-focused windows during a window peek Panel Panel Height Position Offset Position in Panel Regular Right Running Indicator Color Scroll Action Scroll Actions Session Type Settings Show Applications Show Apps Button Show Focused Window Title Show Panels on All Monitors Show Window Previews Delay Show running apps and favorites on the main panel Symbolic Time in ms to hide the window preview Time in ms to show the window preview Time in ms to trigger window peek Toggle / Cycle Toggle / Cycle + Minimize Toggle / Preview Tool-Tips Top Version Window Peeking Window Peeking Delay Window Peeking Opacity Window Preview Options Window Previews Project-Id-Version: aztaskbar
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2022-09-22 10:33+0200
Last-Translator: Onno Giesmann <nutzer3105@gmail.com>
Language-Team: 
Language: de
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Poedit 3.1.1
 Info Aktionen Anwendungssymbole App Icons Taskbar Unten Mitte Aktionen beim Klicken Durchwechseln Fenster durchwechseln Favoriten Indikator-Farbe für fokussierte Anwendung GNOME-Version Allgemein Git-Commit Ausblende-Verzögerung der Fenster-Vorschauen Aktionen beim Berühren Beim Berühren der Fenster-Vorschau wird das fokussierte Fenster angedeutet Symbolentsättigung Symbolgröße Symbolart Nicht alle Symbolthemen enthalten stilisierte Symbole für jede Anwendung Indikator Indikator-Position Bildschirme isolieren Arbeitsflächen isolieren Links Linksklick Aktion bei Linksklick auf laufende Anwendung anpassen Mehrere Striche Indikatorstil bei mehreren Fenstern Keine Aktion OS Position innerhalb des oben gewählten Bereiches verrücken Deckkraft der nicht-fokussierten Fenster während der Fenster-Andeutung Panel Höhe des Panels Positionsversatz Position im Panel Normal Rechts Indikator-Farbe für laufende Anwendung Sroll-Aktion Aktionen beim Scrollen Sitzungstyp Einstellungen Anwendungen anzeigen Knopf Anwendungen anzeigen Titel des fokussierten Fensters anzeigen Panels auf allen Bildschirmen anzeigen Einblende-Verzögerung der Fenster-Vorschauen Laufende Anwendungen und Favoriten im oberen Panel anzeigen Stilisiert Zeit in ms, nach der die Fenstervorschau ausgeblendet wird Zeit in ms, bevor die Fenstervorschau angezeigt wird Zeit in ms, bevor das Fenster angedeutet wird Umschalten / Durchwechseln Umschalten / Durchwechseln + Minimieren Umschalten / Vorschau Minihilfen Oben Version Fenster-Andeutung Verzögerung der Fenster-Andeutung Deckkraft der Fenster-Andeutung Optionen für Fenster-Vorschau Fenster-Vorschauen 