��    <      �  S   �      (     )     /     7     I     c     j     q       	   �     �     �     �  
   �     �     �  3   �     -  	   D  
   N  6   Y  	   �     �  
   �     �     �     �  
   �  -   �       	   1     ;  1   >  3   p     �     �     �     �     �     �     �               #     ,  1   G     y  %   �  %   �  !   �     �     �     	  	   *	     4	     8	     G	     \	     s	     �	  i  �	          	          %     A     J  
   Q     \  
   v     �     �     �  
   �     �     �  L   �     '     <     M  O   \  	   �     �  
   �  $   �  !   �            &   .     U  
   f     q  3   �  @   �     �            	   &     0     7  
   S     ^  
   j  
   u     �  5   �  
   �  3   �  3         B  	   c     m     �     �     �     �     �  $   �     �          <      8       #             9   "                           +      %      &   !            -   0                     5   
   .      '   :          1      )   3      2   4                            $   ,          /   (   ;               	           7             *   6                                About Actions App Icons Taskbar App Icons Taskbar Version Bottom Center Click Actions Cycle Windows Favorites Focused Indicator Color GNOME Version General Git Commit Hide Window Previews Delay Hover Actions Hovering a window preview will focus desired window Icon Desaturate Factor Icon Size Icon Style Icon themes may not have a symbolic icon for every app Indicator Indicator Location Indicators Isolate Monitors Isolate Workspaces Left Left Click Modify Left Click Action of Running App Icons Multi-Window Indicator No Action OS Offset the position within the above selected box Opacity of non-focused windows during a window peek Panel Height Position Offset Position in Panel Regular Right Running Indicator Color Scroll Action Scroll Actions Session Type Settings Show Window Previews Delay Show running apps and favorites on the main panel Symbolic Time in ms to hide the window preview Time in ms to show the window preview Time in ms to trigger window peek Toggle / Cycle Toggle / Cycle + Minimize Toggle / Preview Tool-Tips Top Window Peeking Window Peeking Delay Window Peeking Opacity Window Preview Options Window Previews Project-Id-Version: 
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2022-05-09 13:33+0200
Last-Translator: Heimen Stoffels <vistausss@fastmail.com>
Language-Team: Dutch <kde-i18n-nl@kde.org>
Language: nl
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
X-Generator: Lokalize 22.04.0
 Over Acties Pictogrammentaakbalk Pictogrammentaakbalk-versie Onderaan Midden Klikacties Schakelen tussen vensters Favorieten Kleur van focusindicator GNOME-versie Algemeen Git-commit Verbergvertraging Aanwijsactie Houd de cursor boven een voorvertoning om het venster in kwestie te focussen Pictogramverzadiging Pictogramgrootte Pictogramstijl Let op: pictogramthema's bevatten mogelijk niet altijd een symbolisch pictogram Indicator Indicatorlocatie Indicators Beeldschermen onafhankelijk bedienen Werkbladen onafhankelijk bedienen Links Linkermuisknop Pas de actie van de linkermuisknop aan Vensterindicator Geen actie Besturingssyteem Verschuif de positie met behulp van bovenstaand vak De doorzichtigheid van ongefocuste vensters tijdens het begluren Bovenbalkhoogte Verschuiving Locatie op paneel Standaard Rechts Kleur van actieve indicator Scrolactie Scrolacties Sessietype Voorkeuren Voorvertoningsvertraging Actieve toepassingen en favorieten tonen op hoofdbalk Symbolisch De tijd in ms alvorens een voorvertoning te sluiten De tijd in ms alvorens een venster voor te vertonen De tijd in ms alvorens te gluren Schakelen Schakelen en minimaliseren Schakelen/Voorvertonen Hulpballonnen Bovenaan Vensterbegluring Gluurvertraging Doorzichtigheid van vensterbegluring Voorvertoningsopties Vensters voorvertonen 