const Main = imports.ui.main;
const Config = imports.misc.config;
const Me = imports.misc.extensionUtils.getCurrentExtension();
const Log = Me.imports.modules.log;
const Panel = Me.imports.modules.panel;
var powerProfileIndicatorExtension = class powerProfileIndicatorExtension {
    constructor() {
        this.PowerProfileIndicatorInstance = null;
        this.systemMenu = null;
    }
    enable() {
        this.systemMenu = this.getSystemMenu();
        if (!this.systemMenu) {
            Log.raw("init", "system menu is not defined");
            return false;
        }
        if (this.systemMenu._powerProfiles) {
            this.PowerProfileIndicatorInstance = new Panel.powerProfileIndicator();
        }
    }
    disable() {
        if (this.PowerProfileIndicatorInstance)
            this.PowerProfileIndicatorInstance.stop();
        if (this.PowerProfileIndicatorInstance._indicator)
            this.PowerProfileIndicatorInstance._indicator.destroy();
        this.PowerProfileIndicatorInstance._indicator = null;
        if (this.PowerProfileIndicatorInstance)
            this.PowerProfileIndicatorInstance.destroy();
        this.PowerProfileIndicatorInstance = null;
    }
    populate() {
        if (this.PowerProfileIndicatorInstance !== null) {
            if (parseInt(Config.PACKAGE_VERSION) < 43)
                if (this.systemMenu._power)
                    this.systemMenu._indicators.remove_child(this.systemMenu._power);
            if (parseInt(Config.PACKAGE_VERSION) >= 43)
                if (this.systemMenu._system)
                    this.systemMenu._indicators.remove_child(this.systemMenu._system);
            this.systemMenu._indicators.add_child(this.PowerProfileIndicatorInstance);
            if (parseInt(Config.PACKAGE_VERSION) < 43)
                if (this.systemMenu._power)
                    this.systemMenu._indicators.add_child(this.systemMenu._power);
            if (parseInt(Config.PACKAGE_VERSION) >= 43)
                if (this.systemMenu._system)
                    this.systemMenu._indicators.add_child(this.systemMenu._system);
        }
    }
    getSystemMenu() {
        if (parseInt(Config.PACKAGE_VERSION) < 43) {
            return Main.panel.statusArea.aggregateMenu;
        }
        else {
            return Main.panel.statusArea.quickSettings;
        }
    }
}
function init() {
    powerProfileIndicatorExtensionInstance = new powerProfileIndicatorExtension();
    return powerProfileIndicatorExtensionInstance;
}
//# sourceMappingURL=extension.js.map