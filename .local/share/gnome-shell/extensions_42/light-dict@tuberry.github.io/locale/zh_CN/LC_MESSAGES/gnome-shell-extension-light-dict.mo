��    >        S   �      H     I  	   O     Y     j     o     �  	   �     �  &   �     �     �     �     �  /        2  	   :     D     S     b  
   g  	   r     |  6   �     �     �  u   �     U  6   Z     �     �     �  	   �     �  	   �  
   �     �  *   �     �       	             $  !   3     U     c     o     }     �     �  B   �  =   �     	     "	     *	     =	     B	  	   P	     Z	  
   k	     v	  	   {	    �	     �
  	   �
     �
     �
     �
     �
  	   �
     �
     �
          &     3     @  .   M     |     �     �     �     �     �     �     �  6   �            <   )     f  0   m     �  	   �     �     �     �     �     �     �     �     �                          -     I     V     c     p  	   w     �  6   �  -   �     �  	   �                 	   *     4  	   A     K     R                 1                           '      "   8      (             7                         #                  
      3   -       0   $      5   ,   !         %      2          .   4          +   >   &          <   *   /      :       =                       	   )      ;      9          6       About Allowlist Application list Area Autohide interval Basic Blocklist Clear Click or Press ENTER to commit changes Click the app icon to remove Command type Commit result Copy result Depends on python-opencv and python-pytesseract Disable Dwell OCR Enable systray Enable tooltip Help Hide title Icon name Icon tooltip Leave RegExp/application list blank for no restriction Left click to run Left command Lightweight extension for on-the-fly manipulation to primary selections, especially optimized for Dictionary lookups. Line Middle click the panel to copy the result to clipboard OCR OCR:  Other Page size Panel Paragraph Parameters Passive Passive means that pressing Alt to trigger Passive mode Popup Proactive RegExp filter RegExp matcher Right click to run and hide panel Right command Run command Select result Settings Shortcut Show result Simulate keyboard input in JS statement: <i>key("Control_L+c")</i> Substitute <b>LDWORD</b> for the selected text in the command Swift Swift:  Switch to %s style Tips Trigger style Trigger:  Trim blank lines Version %d Word Work mode Project-Id-Version: gnome-shell-extension-light-dict 55
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2021-08-18 13:20+0800
Last-Translator: Automatically generated
Language-Team: none
Language: zh_CN
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
 关于 白名单 应用名单 区域 隐藏延迟 基本 黑名单 清除 单击或按回车提交更改 点击应用图标移除 命令类型 提交结果 复制结果 依赖于 python-opencv 和 python-pytesseract 禁用 悬停取词 托盘图标 启用提示 帮助 隐藏标题 图标名称 图标提示 正则表达式或应用列表留空则无相应限制 左键运行 左键命令 即时操作所选文本的轻量扩展，为查词而生。 单行 中键点击气泡复制结果到系统剪切板 OCR 取词： 其它 页面容量 气泡 段落 参数 被动 被动意为按住Alt触发 被动模式 弹出 主动 正则过滤 正则匹配 右键运行并关闭气泡 右键命令 运行命令 选取结果 设置 快捷键 显示结果 JS 语句模拟键盘输入: <i>key('Control_L+c')</i> 以 <b>LDWORD</b> 代替命令中所选文本 即时 即时： 切换至%s方式 提示 触发方式 触发： 去除空行 版本 %d 单词 工作模式 