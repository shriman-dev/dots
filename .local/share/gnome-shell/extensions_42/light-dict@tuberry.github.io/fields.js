// vim:fdm=syntax
// by tuberry
/* exported Fields */
'use strict';

var Fields = {
    APPLIST:     'app-list',
    DWELLOCR:    'dwell-ocr',
    SHORTOCR:    'short-ocr',
    LISTTYPE:    'list-type',
    ENABLEOCR:   'enable-ocr',
    HIDETITLE:   'hide-title',
    TXTFILTER:   'text-filter',
    LCOMMAND:    'left-command',
    PASSIVE:     'passive-mode',
    TEXTSTRIP:   'enable-strip',
    OCRMODE:     'ocr-work-mode',
    PAGESIZE:    'icon-pagesize',
    RCOMMAND:    'right-command',
    SCOMMAND:    'swift-command',
    TRIGGER:     'trigger-style',
    OCRPARAMS:   'ocr-parameters',
    PCOMMANDS:   'popup-commands',
    SCOMMANDS:   'swift-commands',
    SYSTRAY:     'enable-systray',
    TOOLTIP:     'enable-tooltip',
    AUTOHIDE:    'autohide-timeout',
    OCRSHORTCUT: 'light-dict-ocr-shortcut',
};
