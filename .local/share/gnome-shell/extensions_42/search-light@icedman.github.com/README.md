# Search Light 

This is a Gnome Shell extension that takes the apps search widget out of Overview. Like the macOS spotlight, or Alfred.

```sh
git clone http://github.com/icedman/search-light
make
```

# Keybinding

ctrl+cmd+space (change at the preference page)

# Screenshots
![First Release](https://raw.githubusercontent.com/icedman/search-light/main/screenshots/Screenshot%20from%202022-11-03%2011-53-28.png)

![First Release](https://raw.githubusercontent.com/icedman/search-light/main/screenshots/Screenshot%20from%202022-11-03%2011-53-01.png)

![First Prototype](https://raw.githubusercontent.com/icedman/search-light/main/screenshots/screenshot.png)

# Credits

Blur-My-Shell for background blurring code.
