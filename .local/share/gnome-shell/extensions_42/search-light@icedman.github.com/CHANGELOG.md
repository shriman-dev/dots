# release 6

* add custom text color
* fix scroll issue
* fix fullscreen issue

# release 5

* add window borders
* add background blur
* add font-size settings

# release 4

* takes gnome's search outside of overview
* custom search window size
* bind to super+custom keys
* bind to preferred monitor or to cursor

