��    #      4  /   L        r   	  \   |  g   �  |   A  b   �  N   !  U   p  T   �  X        t     {     �     �     �     �     �     �     �  	   �     �     �            	   1     ;     A     M  
   d     o     {     �     �     �     �  q  �  �   Q	  N   �	  y   %
  �   �
  X   1  R   �  M   �  L   +  D   x     �     �     �     �     �     �     
  
   *     5  	   >     H  '   N     v     |  
   �     �  
   �     �  	   �     �  	   �     �     �          8                               !          #                    	                   
                                           "                                  <small>Add a button to Top Bar or System Menu. The button can be used to manually toggle the Bedtime Mode.</small> <small>Change the button appearance in Top Bar when Bedtime Mode is active/inactive.</small> <small>Choose to always show or hide the button or display it only when the schedule is active.</small> <small>Enable Color Intensity change by scrolling up or down over the button in Top Bar when Bedtime Mode is active.</small> <small>Manually change the button position in Top Bar. Zero is the start of the icons row.</small> <small>Select the color preset to be used when Bedtime Mode is active.</small> <small>Specifies the time when Bedtime Mode will be automatically turned off.</small> <small>Specifies the time when Bedtime Mode will be automatically turned on.</small> <small>The percentage of how much of the selected color preset is to be applied.</small> Always Amber Automatic Schedule Button Location Color Intensity Color Preset Color Preset and Intensity Cyan End Time Grayscale Green Intensity Change by Scrolling Never On Active Schedule On-Demand Sepia Show Button Start Bedtime Mode Now Start Time System Menu Top Bar Top Bar Manual Position Top Bar On/Off Indicator Turn Off Bedtime Mode Turn On Bedtime Mode Project-Id-Version: gnome-bedtime-mode 6
Report-Msgid-Bugs-To: ionutbortis@gmail.com
PO-Revision-Date: 2021-10-25 18:20+0200
Last-Translator: Heimen Stoffels <vistausss@fastmail.com>
Language-Team: 
Language: nl
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
X-Generator: Poedit 3.0
 <small>Toon een knop op de bovenbalk of in het systeemmenu. Met deze knop kan de nachtkastmodus worden in- of uitgeschakeld.</small> <small>Bepaal hoe de knop eruitziet als de knop actief of inactief is.</small> <small>Geef aan of de knop altijd moet worden getoond of worden verborgen, of alleen als er een schema actief is.</small> <small>Verander de kleurintensiteit door omhoog of omlaag te scrollen over de knop op de bovenbalk als de nachtkastmodus is ingeschakeld.</small> <small>Kies een eigen locatie op de bovenbalk. 0 = begin van de pictogrammenrij.</small> <small>Kies de te gebruiken kleuren als de nachtkastmodus is ingeschakeld.</small> <small>Geef aan hoe laat de nachtkastmodus moet worden uitgeschakeld.</small> <small>Geef aan hoe laat de nachtkastmodus moet worden ingeschakeld.</small> <small>Hoeveel (in procenten) kleur er moet worden gebruikt.</small> Altijd Amber Automatisch schema Knoplocatie Kleurintensiteit Kleurinstelling Voorinstellingen en intensiteit Groenblauw Eindtijd Zwart-wit Groen Intensiteit veranderen door te scrollen Nooit Tijdens schema Op verzoek Sepia Knop tonen Nachtkastmodus inschakelen Starttijd Systeemmenu Bovenbalk Eigen locatie op bovenbalk Aan-/Uitindicator op bovenbalk Nachtkastmodus uitschakelen Nachtkastmodus inschakelen 