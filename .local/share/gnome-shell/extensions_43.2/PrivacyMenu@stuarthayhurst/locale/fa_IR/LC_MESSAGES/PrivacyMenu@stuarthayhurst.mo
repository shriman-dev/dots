��          �   %   �      0     1     :     G  7   N  W   �     �     �            *   *     U  
   ^     i     �     �     �     �     �     �  6   �          2  ;   N  i  �      �  (        >  O   K  t   �          .     L  +   f  @   �     �     �  .   �          )  7   I     �  )   �     �  R   �  6   -	  :   d	  Q   �	                     	                                   
                                                                        enabled All disabled Camera Force the icon to move to right side of the status area Found this useful?
<a href="https://paypal.me/stuartahayhurst">Consider donating</a> :) GNOME 43+ settings GNOME 44+ settings General settings Group quick settings Group quick settings together, into a menu Location Microphone Move status icon right Privacy Privacy Settings Privacy Settings Menu Indicator Reset settings Reset to defaults Settings Show the privacy status in the quick settings subtitle Use quick settings menu Use quick settings subtitle Use the system quick settings area, instead of an indicator Project-Id-Version: privacy-menu-extension
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2023-07-14 13:08+0330
Last-Translator: MohammadSaleh Kamyab <mskf1383@envs.net>
Language-Team: 
Language: fa_IR
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n==0 || n==1);
X-Generator: Poedit 3.2.2
  به کار افتاده است همه از کار افتاده‌اند دوربین انتقال اجباری نقشک به سمت راست ناحیهٔ وضعیت برایتان مفید بود؟
<a href="https://paypal.me/stuartahayhurst">اعانه فراموش نشود</a> :) تنظیمات گنوم ۴۳+ تنظیمات گنوم ۴۴+ تنظیمات عمومی گروه‌بندی تنظیمات سریع گروه‌بندی تنظیمات سریع در یک فهرست مکان صدابَر انتقال نقشک وضعیت به راست محرمانگی تنظیمات محرمانگی نشانگر فهرست تنظیمات محرمانگی بازنشانی تنظیمات بازنشانی به پیش‌گزیده تنظیمات نمایش وضعیت محرمانگی در زیرنویس تنظیمات سریع استفاده از فهرست تنظیمات سریع استفاده از زیرنویس تنظیمات سریع استفاده از ناحیهٔ تنظیمات سریع به جای نشانگر 