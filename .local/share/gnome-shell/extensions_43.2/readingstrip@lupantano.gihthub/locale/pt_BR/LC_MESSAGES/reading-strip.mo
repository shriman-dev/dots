��          |      �             !     5     I     \     l     |     �     �  
   �     �  N   �  �       �     �     �     �     �               3  	   ;     E  X   L                                          	          
       <b>Color Focus</b>: <b>Focus strip</b>: <b>Opacity</b> (%) <b>Profile</b>: <b>Size</b> (%) <b>Strip Color</b>: <b>Vertical Strip</b>: Default Focus Mode Rules You can activate/deactive with <b>SUPER+CTRL+SPACE</b> 
or click on icon panel Project-Id-Version: readingstrip
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2022-09-10 15:29-0300
Last-Translator: Daniel Henriques <danielh.pamplona@gmail.com>
Language-Team: English - United States <danielh.pamplona@gmail.com>
Language: en_US
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
X-Generator: Gtranslator 41.0
 <b>Cor do foco</b>: <b>Linha de foco</b>: <b>Opacidade</b> (%) <b>Perfil</b>: <b>Tamanho</b> (%) <b>Cor da linha</b>: <b>Linha vertical</b>: Padrão Modo foco Regras Você pode ativar/desativar com <b>SUPER+CTRL+ESPAÇO</b> ou clicar no painel de ícones 