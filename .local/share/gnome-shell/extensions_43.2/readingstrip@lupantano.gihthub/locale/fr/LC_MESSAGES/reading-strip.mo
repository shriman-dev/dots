��          |      �             !     5     I     \     l     |     �     �  
   �     �  N   �  %        5     V     u     �     �     �     �     �     �       b   	                                          	          
       <b>Color Focus</b>: <b>Focus strip</b>: <b>Opacity</b> (%) <b>Profile</b>: <b>Size</b> (%) <b>Strip Color</b>: <b>Vertical Strip</b>: Default Focus Mode Rules You can activate/deactive with <b>SUPER+CTRL+SPACE</b> 
or click on icon panel Project-Id-Version: 1.1
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2022-07-31 14:07+0200
Last-Translator: Albano Battistella <Albano_battistella@hotmail.com>
Language-Team: French <LL@li.org>
Language: fr
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
 <b>Couleur de mise au point</b>: <b>Barre de mise au point</b>: <b>Transparence</b> (%) <b>Profil</b>: <b>Dimension</b> (%) <b>Couleur de la barre</b> <b>Barre verticale</b>: Par défaut Sans distraction Règles Vous pouvez activer/désactiver avec <b>SUPER+CTRL+ESPACE</b> 
ou cliquez sur le panneau d'icônes 