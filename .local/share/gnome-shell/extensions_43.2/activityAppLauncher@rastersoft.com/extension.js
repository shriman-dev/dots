const GLib = imports.gi.GLib;
const St = imports.gi.St;
const GMenu = imports.gi.GMenu;
const GObject = imports.gi.GObject;
const Clutter = imports.gi.Clutter;
const Shell = imports.gi.Shell;

const AppDisplay = imports.ui.appDisplay;
const Main = imports.ui.main;
const Favorites = imports.ui.appFavorites;
const ExtensionUtils = imports.misc.extensionUtils;

const Gettext = imports.gettext;

const SCHEMA = 'org.gnome.shell.extensions.activityAppLauncher';

var _ = Gettext.domain("activityAppLauncher").gettext;
Gettext.bindtextdomain("activityAppLauncher", ExtensionUtils.getCurrentExtension().path + "/locale");

const ButtonType = Object.freeze({
    Windows: 0,
    AllApps: 1,
    Favorites: 2,
    Frequents: 3,
    Group: 4
});

// Version 11

var init = function () {
    return new ActivityAppLauncher();
}

var BaseConstraint = GObject.registerClass(
class BaseConstraint extends Clutter.Constraint {
    _init(props) {
        super._init(props);
        this._element = null;
        this._settings = null;
    }

    set_element(element, settings) {
        this._element = element;
        this._settings = settings;
    }

    vfunc_update_allocation(actor, actorBox) {
        const prefSize = this._element.get_preferred_size();
        let x = 0;
        let y = Main.panel.height;
        const [stageWidth, stageHeight] = Main.layoutManager.overviewGroup.get_transformed_size();
        if (this._settings.get_boolean('show-centered')) {
            y = Math.max(y, Math.floor((stageHeight - prefSize[3]) / 2));
        }
        const dash = Main.overview._overview.dash;
        const [dashX, dashY] = dash.get_transformed_position();
        const [dashWidth, dashHeight] = dash.get_transformed_size();
        if (dashWidth < dashHeight) { // vertical dash
            if (dashX < (stageWidth/2)) { // dash is at the left
                x = dashWidth;
            }
        } else { // horizontal dash
            if (dashY < (stageHeight/2)) { // dash is at the top
                y += dashHeight;
            }
        }
        actorBox.init_rect(x, y, prefSize[2], prefSize[3]);
    }
});

class ActivityAppLauncher {
    constructor() {
        this.isEnabled = false;
        this._startupPreparedId = 0;
        this._appsInnerContainer = null;
        this.selected = null;
    }

    enable() {
        // Wait until startup completed
        if (Main.layoutManager._startingUp) {
            this._startupPreparedId = Main.layoutManager.connect('startup-complete', () => {
                this._doEnable(true);
            });
        } else {
            this._doEnable(false);
        }
    }

    _doEnable(removeStartup) {
        // Does all the enabling work, after the startup process has been completed
        if (removeStartup) {
            Main.layoutManager.disconnect(this._startupPreparedId);
            this._startupPreparedId = 0;
        }
        this._appSys = Shell.AppSystem.get_default();
        this._settings = ExtensionUtils.getSettings(SCHEMA);
        this.isEnabled = true;

        // Add the categories menu in the overview container
        this._appsInnerContainer = new St.BoxLayout({
            vertical: true,
            x_align: Clutter.ActorAlign.START,
            y_align: Clutter.ActorAlign.CENTER,
            x_expand: false,
            y_expand: true
        });
        this._constraint = new BaseConstraint();
        this._constraint.set_element(this._appsInnerContainer, this._settings);
        this._appsInnerContainer.add_constraint(this._constraint);
        Main.layoutManager.overviewGroup.add_child(this._appsInnerContainer);
        this._appsInnerContainer.show();
        this._show();
        let activityAppLauncherObject = this; // to have it available inside the new functions
        this._appDisplay = Main.overview._overview.controls.appDisplay;

        //--------- BEGIN GNOME SHELL MONKEY PATCHING

        // Replace the ordering function in the applications list object
        this._oldCompareItemsFunc = this._appDisplay._compareItems;
        let oldCompareItemsFunc = this._oldCompareItemsFunc;
        this._appDisplay._compareItems = function (a, b) {
            if (activityAppLauncherObject.selected === null) {
                return oldCompareItemsFunc.bind(this)(a, b);
            }
            return a.name.localeCompare(b.name);
        }

        // Replace the application list loading function in the applications list object
        this._oldLoadAppsFunc = this._appDisplay._loadApps;
        let oldLoadAppsFunc = this._oldLoadAppsFunc;
        this._appDisplay._loadApps = function () {
            if (activityAppLauncherObject.selected === null) {
                return oldLoadAppsFunc.bind(this)();
            }
            let appIcons = [];
            this._appInfoList = Shell.AppSystem.get_default().get_installed().filter(appInfo => {
                try {
                    appInfo.get_id(); // catch invalid file encodings
                } catch (e) {
                    return false;
                }
                if (!this._parentalControlsManager.shouldShowApp(appInfo)) {
                    return false;
                }

                // the objects obtained with GMenu.tree don't contain
                // the should_show() method, so it is a must to map the
                // objects obtained with Shell.AppSystem with the ones to show.
                for (let app of activityAppLauncherObject.selected) {
                    if (app.get_id() == appInfo.get_id()) {
                        return true;
                    }
                }
                return false;
            });

            let apps = this._appInfoList.map(app => app.get_id());

            let appSys = Shell.AppSystem.get_default();

            this._folderIcons = [];

            // Allow dragging of the icon only if the Dash would accept a drop to
            // change favorite-apps. There are no other possible drop targets from
            // the app picker, so there's no other need for a drag to start,
            // at least on single-monitor setups.
            // This also disables drag-to-launch on multi-monitor setups,
            // but we hope that is not used much.
            const isDraggable =
                global.settings.is_writable('favorite-apps') ||
                global.settings.is_writable('app-picker-layout');
            apps.forEach(appId => {
                let icon = this._items.get(appId);
                if (!icon) {
                    let app = appSys.lookup_app(appId);
                    icon = new AppDisplay.AppIcon(app, { isDraggable });
                    icon.connect('notify::pressed', () => {
                        if (icon.pressed)
                            this.updateDragFocus(icon);
                    });
                }
                appIcons.push(icon);
            });
            return appIcons;
        }.bind(this._appDisplay);

        //------------ END GNOME SHELL MONKEY PATCHING

        this._favorites = Favorites.getAppFavorites();
        this._usage = Shell.AppUsage.get_default();
        this.showingId = Main.overview.connect('showing', () => { this._show(); });
        this.hidingId = Main.overview.connect('hiding', () => { this._hide(); });
    }

    disable() {
        if (this.isEnabled) {
            // Restore everything inside Gnome Shell
            Main.layoutManager.overviewGroup.remove_child(this._appsInnerContainer);
            this._appDisplay._loadApps = this._oldLoadAppsFunc;
            this._appDisplay._compareItems = this._oldCompareItemsFunc;
            this._appsInnerContainer = null;
            this._constraint = null;

            // Disconnect the signals
            Main.overview.disconnect(this.showingId);
            Main.overview.disconnect(this.hidingId);
        }
        this.isEnabled = false;
    }

    _hide() {
        this.selected = null;
        this._appsInnerContainer.destroy_all_children();
    }

    _show() {
        this.selected = null;
        this._fillCategories();
    }

    _fillCategories() {
        this.selected = null;
        this._appsInnerContainer.destroy_all_children();

        this._appsInnerContainer.buttons = [];
        this._appsInnerContainer.appClass = [];

        let tree = new GMenu.Tree({ menu_basename: 'applications.menu' });
        tree.load_sync();
        let root = tree.get_root_directory();
        let categoryMenuItem = new AALCathegory_Menu_Item(this, ButtonType.Windows, _("Windows"), null);
        this._appsInnerContainer.add_child(categoryMenuItem);
        this._appsInnerContainer.buttons.push(categoryMenuItem);

        let allAppsMenuItem = new AALCathegory_Menu_Item(this, ButtonType.AllApps, _("All apps"), null);
        this._appsInnerContainer.add_child(allAppsMenuItem);
        this._appsInnerContainer.buttons.push(allAppsMenuItem);

        if (this._settings.get_boolean("show-favorites")) {
            let favoritesMenuItem = new AALCathegory_Menu_Item(this, ButtonType.Favorites, _("Favorites"), null);
            this._appsInnerContainer.add_child(favoritesMenuItem);
            this._appsInnerContainer.buttons.push(favoritesMenuItem);
        }

        if (this._settings.get_boolean("show-frequent")) {
            let mostUsedMenuItem = new AALCathegory_Menu_Item(this, ButtonType.Frequents, _("Frequent"), null);
            this._appsInnerContainer.add_child(mostUsedMenuItem);
            this._appsInnerContainer.buttons.push(mostUsedMenuItem);
        }

        let iter = root.iter();
        let nextType;
        while ((nextType = iter.next()) != GMenu.TreeItemType.INVALID) {
            if (nextType == GMenu.TreeItemType.DIRECTORY) {
                let dir = iter.get_directory();
                if (!dir.get_is_nodisplay()) {
                    let childrens = this._fillCategories2(dir, []);
                    if (childrens.length != 0) {
                        let item = { dirItem: dir, dirChilds: childrens };
                        this._appsInnerContainer.appClass.push(item);
                    }
                }
            }
        }
        for (var i = 0; i < this._appsInnerContainer.appClass.length; i++) {
            let categoryMenuItem = new AALCathegory_Menu_Item(this, ButtonType.Group, this._appsInnerContainer.appClass[i].dirItem.get_name(), this._appsInnerContainer.appClass[i].dirChilds);
            this._appsInnerContainer.add_child(categoryMenuItem);
            this._appsInnerContainer.buttons.push(categoryMenuItem);
        }
    }

    _fillCategories2(dir, childrens) {
        let iter = dir.iter();
        let nextType;

        while ((nextType = iter.next()) != GMenu.TreeItemType.INVALID) {
            if (nextType == GMenu.TreeItemType.ENTRY) {
                let entry = iter.get_entry();
                if (!entry.get_app_info().get_nodisplay()) {
                    let app = this._appSys.lookup_app(entry.get_desktop_file_id());
                    childrens.push(app);
                }
            } else if (nextType == GMenu.TreeItemType.DIRECTORY) {
                childrens = this._fillCategories2(iter.get_directory(), childrens);
            }
        }
        return childrens;
    }

    _clickedCathegory(button) {
        for (var i = 0; i < this._appsInnerContainer.buttons.length; i++) {
            var tmpbutton = this._appsInnerContainer.buttons[i];
            if (button == tmpbutton) {
                tmpbutton.checked = true;
            } else {
                tmpbutton.checked = false;
            }
        }

        switch (button.launcherType) {
            case ButtonType.Group:
                this.selected = button.launchers;
                break;
            case ButtonType.Windows:
            case ButtonType.AllApps:
                this.selected = null;
                break;
            case ButtonType.Favorites:
                this.selected = this._favorites.getFavorites();
                break;
            case ButtonType.Frequents:
                this.selected = this._usage.get_most_used();
                break;
        }

        if (button.launcherType == ButtonType.Windows) {
            Main.overview.dash.showAppsButton.checked = false;
        } else {
            // Remove all the icons from the grid and insert them again, alphabetically sorted
            [...this._appDisplay.getAllItems()].forEach(icon => {
                this._appDisplay._removeItem(icon);
                icon.destroy();
            });
            this._appDisplay._loadApps().sort(this._appDisplay._compareItems.bind(this._appDisplay)).forEach(icon => {
                this._appDisplay._addItem(icon, -1, -1);
            });
            Main.overview.dash.showAppsButton.checked = true;
        }
    }
}

const AALCathegory_Menu_Item = GObject.registerClass({
    GTypeName: 'AALCathegory_Menu_Item',
}, class AALCathegory_Menu_Item extends St.Button {
    _init(topClass, type, cathegory, launchers) {
        this.topClass = topClass;
        this.cat = cathegory;
        this.launchers = launchers;
        this.launcherType = type;
        super._init({
            label: GLib.markup_escape_text(cathegory, -1),
            style_class: "world-clocks-button button activityAppLauncherButton",
            toggle_mode: true,
            can_focus: true,
            track_hover: true
        });
        this.connect("clicked", () => {
            this.topClass._clickedCathegory(this);
        });
        this.show();
    }
});
