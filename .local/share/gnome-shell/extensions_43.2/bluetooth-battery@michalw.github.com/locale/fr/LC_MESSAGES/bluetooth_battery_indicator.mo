��          �   %   �      0     1     :     B     ^     k     s     {     �  
   �     �  &   �     �     �     �     �          	               $     ?     H     O  M  V     �     �      �     �     �  	   �  %   �           /     6  -   H     v     }     �     �     �     �     �     �  )   �     �                  
                                                                             	                                          % Source Battery Bluetooth battery Indicator Bluetoothctl Default Devices Force refresh bluetooth Game Controller Headphones Headset Hide indicator if there are no devices Icon Indicator Settings Keyboard Mouse Name Port Python script Refresh Refresh interval (minutes) Settings Status UPower Project-Id-Version: 
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2023-07-11 10:39+0200
Last-Translator: Benjamin Renard <brenard@easter-eggs.com>
Language-Team: 
Language: fr_FR
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n > 1);
X-Generator: Poedit 3.2.2
 Source % Batterie Indicateur de batterie bluetooth Bluetoothctl Par défaut Appareils Rafraîchissement forcé du bluetooth Manette de jeu Casque Casque avec micro Caché l'indicateur s'il n'y a aucun appareil Icône Paramètres de l'indicateur Clavier Souris Nom Port Script python Rafraîchir Intervalle de rafraîchissement (minutes) Paramètres État UPower 