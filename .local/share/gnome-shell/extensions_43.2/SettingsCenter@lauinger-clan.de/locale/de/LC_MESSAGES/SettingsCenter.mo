��             +         �  *   �     �     �                         )     .     E     L     `     m     s  
   y     �  #   �  
   �  
   �     �  8   �          0     D     W     `     p          �     �  ,   �  Q  �  :   5     p     |     �     �     �     �     �     �     �     �               *     7  (   J  #   s     �     �     �  =   �          (     A     W     e     y     �  +   �     �  8   �            	                                                     
                                                                               'Label' and 'Command' must be filled out ! Add Add Menu Apply Command Del Desktop Config Editor Down Extensions Preferences Global Gnome Config Editor Gnome Tweaks Items Label Label Menu Label to show in menu Makes the systemindicator show/hide Menu Items Menu Label NVidia Settings Name of .desktop file (MyApp.desktop) or name of command Passwords and Keys Select desktop file Session Properties Settings Settings Center SettingsCenter Show SystemIndicator Toggle to show systemindicator Up Usually located in '/usr/share/applications' Project-Id-Version: 
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2023-03-28 18:01+0200
Last-Translator: Christian Lauinger <christian@lauinger-clan.de>
Language-Team: 
Language: de
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
X-Generator: Poedit 3.2.2
 'Beschriftung' und 'Kommando' müssen ausgefüllt werden ! Hinzufügen Menü hinzufügen Übernehmen Kommando Entf Desktop Konfigurationseditor Ab Erweiterungen Einstellungen Global Gnome Konfigurationseditor Gnome Optimierungen Menü Einträge Beschriftung Menü Beschriftung Beschriftung die im Menü angezeigt wird Systemindikator anzeigen/verstecken Menü Einträge Menü Beschriftung NVidia Einstellungen Name der .desktop Datei (MyApp.desktop) oder Name des Befehls Passwörter und Schlüssel Desktop Datei auswählen Sitzungseigenschaften Einstellungen Einstellungszentrum Einstellungszentrum Systemindikator anzeigen Umschalten der Anzeige des Systemindikators Auf Befinden sich normalerweise in '/usr/share/applications' 