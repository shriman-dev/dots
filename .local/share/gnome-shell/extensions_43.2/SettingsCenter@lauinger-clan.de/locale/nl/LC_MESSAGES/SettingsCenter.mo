��             +         �  *   �     �     �                         )     .     E     L     `     m     s  
   y     �  #   �  
   �  
   �     �  8   �          0     D     W     `     p          �     �  ,   �  O  �     3  	   O     Y  	   h     r     {          �     �     �     �     �     �     �  	   �           #  
   D  	   O     Y  Q   k      �     �     �  
             "     4  "   K     n  4   u            	                                                     
                                                                               'Label' and 'Command' must be filled out ! Add Add Menu Apply Command Del Desktop Config Editor Down Extensions Preferences Global Gnome Config Editor Gnome Tweaks Items Label Label Menu Label to show in menu Makes the systemindicator show/hide Menu Items Menu Label NVidia Settings Name of .desktop file (MyApp.desktop) or name of command Passwords and Keys Select desktop file Session Properties Settings Settings Center SettingsCenter Show SystemIndicator Toggle to show systemindicator Up Usually located in '/usr/share/applications' Project-Id-Version: 
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2023-04-23 20:13+0200
Last-Translator: Heimen Stoffels <vistausss@fastmail.com>
Language-Team: Dutch
Language: nl
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
X-Generator: Poedit 3.2.2
 Kies een label en opdracht. Toevoegen Menu toevoegen Toepassen Opdracht Del Bureaublad-voorkeurenbewerker Omlaag Uitbreidingsvoorkeuren Algemeen GNOME-voorkeurenbewerker GNOME Afstelhulp Items Tekst Menutekst Het te tonen label in het menu Toon/Verberg de systeemindicator Menu-items Menutekst NVIDIA-voorkeuren De naam van een .desktopbestand (MijnToepassing.desktop) of naam van een opdracht Wachtwoorden en toegangssleutels Kies een .desktopbestand Sessievoorkeuren Voorkeuren Voorkeurenmenu Voorkeurencentrum Systeemindicator tonen Systeemindicator toon-/verbergknop Omhoog Doorgaans te vinden in ‘/usr/share/applications’ 