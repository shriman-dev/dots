/* extension.js
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Author: Marco Trevisan <marco@ubuntu.com>
 *
 * SPDX-License-Identifier: GPL-2.0-or-later
 */

/* exported init */

const { GLib, Gio, GObject, Shell } = imports.gi;

const Config = imports.misc.config;
const ExtensionUtils = imports.misc.extensionUtils;
const Extension = ExtensionUtils.getCurrentExtension();

const Main = imports.ui.main;
const MessageTray = imports.ui.messageTray;
const OsdWindow = imports.ui.osdWindow;

const DBusLauncher = Extension.imports.dbusLauncher;

const AppNotificationSource = GObject.registerClass(
class AppNotificationSource extends MessageTray.Source {
    _init(app) {
        super._init(app.get_name());
        this.app = app;
    }

    open() {
        this.destroy();
    }

    getIcon() {
        let icon;

        if (this.app.get_icon)
            icon = this.app.get_icon();

        if (this.app.appInfo)
            icon = this.app.appInfo.get_icon();

        return icon || Gio.ThemedIcon.new('application-x-executable');
    }
});

class StartupTime {
    enable() {
        this._settings = ExtensionUtils.getSettings(
            'org.gnome.shell.extensions.startup-measure');
        this._launching = new Map();
        this._defaultLaunch = Shell.App.prototype.launch;
        this._defaultActivate = Shell.App.prototype.activate;
        this._updateDBusLauncher();
        this._settings.connect('changed::disable-dbus-launcher',
            () => this._updateDBusLauncher());

        this._appSystem = Shell.AppSystem.get_default();
        this._appStateChangedId = this._appSystem.connect('app-state-changed',
            (_, app) => {
                if (app.state === Shell.AppState.STARTING)
                    this._monitorApp(app);
            });

        const self = this;
        Shell.App.prototype.launch = function (...args) {
            self._monitorApp(this);
            return self._defaultLaunch.call(this, ...args);
        }

        Shell.App.prototype.activate = function (...args) {
            self._monitorApp(this);
            return self._defaultActivate.call(this, ...args);
        }
    }

    _updateDBusLauncher() {
        if (!this._settings.get_boolean('disable-dbus-launcher')) {
            if (this._dbusLauncher)
                return;

            this._dbusLauncher = new DBusLauncher.DBusLauncher();
        } else if (this._dbusLauncher) {
            this._dbusLauncher.destroy();
            this._dbusLauncher = null;
        }
    }

    _notifyUser(app, appStartupTime) {
        const [majorVersion] = Config.PACKAGE_VERSION.split('.');

        const source = new AppNotificationSource(app);
        const msg = `${app.get_name()} was launched in ${appStartupTime}ms`;
        const hideTimeout = this._settings.get_uint('hide-timeout');

        log(`${app.get_name()} (${app.get_id()}) was launched in ${appStartupTime}ms`);

        if (this._settings.get_boolean('use-osd') && majorVersion >= 42) {
            const defaultTimeout = OsdWindow.HIDE_TIMEOUT;
            OsdWindow.HIDE_TIMEOUT = hideTimeout;
            Main.osdWindowManager.show(-1, source.getIcon(), msg);
            OsdWindow.HIDE_TIMEOUT = defaultTimeout;
        }

        if (this._settings.get_boolean('use-notification') || majorVersion < 42) {
            const defaultNotificationCompleted =
                MessageTray.MessageTray.prototype._showNotificationCompleted;
            MessageTray.MessageTray.prototype._showNotificationCompleted = function(...args) {
                MessageTray.MessageTray.prototype._showNotificationCompleted =
                    defaultNotificationCompleted;
                const defaultTimeout = MessageTray.NOTIFICATION_TIMEOUT;
                MessageTray.NOTIFICATION_TIMEOUT = hideTimeout;
                defaultNotificationCompleted.call(this, ...args)
                MessageTray.NOTIFICATION_TIMEOUT = defaultTimeout;
            };
            Main.messageTray.add(source);
            const notification = new MessageTray.Notification(source, msg);
            notification.setTransient(true);
            source.showNotification(notification);
        }
    }

    _onAppChanged(app) {
        const now = new Date();
        const info = this._launching.get(app);

        if (app.state === Shell.AppState.STOPPED) {
            // We can't just forgetApp here as old Shell may be emit this wrongly
            info.waitTimeout = GLib.timeout_add_seconds(GLib.PRIORITY_DEFAULT, 5, () => {
                if (this._launching.has(app))
                    this._forgetApp(app);
                return GLib.SOURCE_REMOVE;
            });
            return;
        }

        if (app.state !== Shell.AppState.RUNNING || !app.get_n_windows())
            return;

        const windows = app.get_windows();
        if (!windows.some(w => w.get_compositor_private() ?
            w.get_compositor_private().is_mapped() : false)) {
            info.windowsConnections.forEach(([actor, id]) => actor.disconnect(id));
            info.windowsConnections.clear();

            windows.forEach(w => {
                const actor = w.get_compositor_private();
                let id = w.connect('shown', () => this._onAppChanged(app));
                info.windowsConnections.add([w, id]);
                if (!actor)
                    return;
                id = actor.connect('notify::mapped', () => this._onAppChanged(app));
                info.windowsConnections.add([actor, id]);
            });

            return;
        }

        const appStartupTime = now.getTime() - info.startupTime.getTime();
        this._notifyUser(app, appStartupTime);

        if (this._dbusLauncher)
            this._dbusLauncher.emitAppStarted(app, appStartupTime);

        this._forgetApp(app);
    }

    _forgetApp(app) {
        const info = this._launching.get(app);
        info.appConnections.forEach(id => app.disconnect(id));
        info.windowsConnections.forEach(([actor, id]) => actor.disconnect(id));
        if (info.waitTimeout)
            GLib.source_remove(info.waitTimeout);
        this._launching.delete(app);

        if (this._dbusLauncher)
            this._dbusLauncher.cancelLaunch(app);
    }

    _monitorApp(app) {
        const startupTime = new Date();

        if (app.state === Shell.AppState.RUNNING)
            return;

        if (this._launching.has(app))
            return;

        this._launching.set(app, {
            startupTime,
            appConnections: new Set([
                app.connect('notify::state', () => this._onAppChanged(app)),
                app.connect('windows-changed', () => this._onAppChanged(app)),
            ]),
            windowsConnections: new Set(),
        });

        log(`Monitoring startup time of ${app.get_name()} (${app.get_id()})`);
    }

    disable() {
        this._appSystem.disconnect(this._appStateChangedId);
        this._appSystem = null;
        if (this._dbusLauncher) {
            this._dbusLauncher.destroy();
            this._dbusLauncher = null;
        }
        this._settings.run_dispose();
        this._settings = null;
        this._launching.forEach((_, app) => this._forgetApp(app));

        Shell.App.prototype.launch = this._defaultLaunch;
        delete this._defaultLaunch;
        Shell.App.prototype.activate = this._defaultActivate;
        delete this._defaultActivate;
    }
}

function init() {
    return new StartupTime();
}
