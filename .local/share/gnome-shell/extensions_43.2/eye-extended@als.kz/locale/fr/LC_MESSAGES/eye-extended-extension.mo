��          �   %   �      P     Q     X     w     }     �     �     �     �     �     �     �  
   �     �     
               '     7     ?     H     ^     d     p     v     |  ^  �     �  !   �     	            $   !  '   F  #   n     �  	   �     �     �     �     �     �     �     �           	  $        7  
   =     H     O     U        
                                               	                                                                      Center Click on the eye to turn it on Color Default Enable Enable left click coloring Enable middle click coloring Enable right click coloring Eye Eyelid Left Left click Line Thickness Location Margin Middle click Mouse indicator Opacity Position Refresh interval (ms) Right Right click Round Shape Size Project-Id-Version: EyeExtendedExtension
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2022-11-28 14:43-0500
Last-Translator: Philippe-André Akue <pakueamb@gmail.com>
Language-Team: 
Language: fr
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n > 1);
X-Generator: Poedit 3.1.1
 Centre Cliquez sur l'œil pour l'allumer Couleur Défaut Activer Activer la coloration du clic gauche Activer la coloration du clic du milieu Activer la coloration du clic droit Œil Paupière Gauche Clic gauche Épaisseur des lignes Emplacement Marge Clic du milieu Indicateur de souris Opacité Position Intervalle de rafraîchissement (ms) Droit Clic droit Cercle Forme Taille 