��    
      l      �       �       �           0  !   J  C   l  $   �  %   �  (   �     $  ?  )  C   i  G   �  B   �  ;   8  �   t  ^     m   g  n   �     D                   
         	                 Checking idle interval (seconds) Debug mode (restart required) Disable bluetooth if idle Enable bluetooth when menu opened Keep the menu open after toggling the connection (restart required) Show battery icon (restart required) Show battery value (restart required) Show reconnect button (restart required) Wait Project-Id-Version: 1
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2023-11-07 14:31+0530
Last-Translator: a1exak <a1.kamaev@yandex.ru>
Language-Team: 
Language: ru
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
X-Generator: Poedit 3.3.2
 Проверка интервала простоя (секунды) Режим отладки (требуется перезагрузка) Отключить bluetooth, если он простаивает Включить bluetooth при открытии меню Оставлять меню открытым после переключения соединения (требуется перезагрузка) Показывать значок батареи (требуется перезагрузка) Показывать уровень заряда батареи (требуется перезагрузка) Показывать кнопку переподключения (требуется перезагрузка) Ожидайте 