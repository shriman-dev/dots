��    "      ,  /   <      �  *   �     $     (     1     7     ?     C     Y     ^     p     �     �     �     �     �  
   �     �     �  #   �  
     
        '  8   7     p     �     �     �     �     �     �     �       ,   	  Q  6  :   �     �     �     �     �     �     �               0     >     E     `     t     �     �  (   �     �  #   �               *  =   ?     }     �     �     �     �     �     �  +   	     B	  8   F	                                                     
                !                     	                                                   "                 'Label' and 'Command' must be filled out ! Add Add Menu Apply Command Del Desktop Config Editor Down Extension Manager Extensions Preferences Global Gnome Config Editor Gnome Tweaks Items Label Label Menu Label to show in menu Last chosen file Makes the systemindicator show/hide Menu Items Menu Label NVidia Settings Name of .desktop file (MyApp.desktop) or name of command Passwords and Keys Select desktop file Session Properties Settings Settings Center SettingsCenter Show SystemIndicator Toggle to show systemindicator Up Usually located in '/usr/share/applications' Project-Id-Version: 
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2023-10-08 19:21+0200
Last-Translator: Christian Lauinger <christian@lauinger-clan.de>
Language-Team: 
Language: de
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
X-Generator: Poedit 3.3.2
 'Beschriftung' und 'Kommando' müssen ausgefüllt werden ! Hinzufügen Menü hinzufügen Übernehmen Kommando Entf Desktop Konfigurationseditor Ab Erweiterungs-Manager Erweiterungen Global Gnome Konfigurationseditor Gnome Optimierungen Menü Einträge Beschriftung Menü Beschriftung Beschriftung die im Menü angezeigt wird Letzt gewählte Datei Systemindikator anzeigen/verstecken Menü Einträge Menü Beschriftung NVidia Einstellungen Name der .desktop Datei (MyApp.desktop) oder Name des Befehls Passwörter und Schlüssel Desktop Datei auswählen Sitzungseigenschaften Einstellungen Einstellungszentrum Einstellungszentrum Systemindikator anzeigen Umschalten der Anzeige des Systemindikators Auf Befinden sich normalerweise in '/usr/share/applications' 