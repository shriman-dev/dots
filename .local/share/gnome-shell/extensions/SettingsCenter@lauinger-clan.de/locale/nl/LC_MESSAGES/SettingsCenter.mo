��    "      ,  /   <      �  *   �     $     (     1     7     ?     C     Y     ^     p     �     �     �     �     �  
   �     �     �  #   �  
     
        '  8   7     p     �     �     �     �     �     �     �       ,   	  V  6     �  	   �     �  	   �     �     �     �     �     �          )     2     K     \     b  	   h     r     �      �  
   �  	   �     �  Q   �      B     c     |  
   �     �     �     �  "   �     �  4   �                                                     
                !                     	                                                   "                 'Label' and 'Command' must be filled out ! Add Add Menu Apply Command Del Desktop Config Editor Down Extension Manager Extensions Preferences Global Gnome Config Editor Gnome Tweaks Items Label Label Menu Label to show in menu Last chosen file Makes the systemindicator show/hide Menu Items Menu Label NVidia Settings Name of .desktop file (MyApp.desktop) or name of command Passwords and Keys Select desktop file Session Properties Settings Settings Center SettingsCenter Show SystemIndicator Toggle to show systemindicator Up Usually located in '/usr/share/applications' Project-Id-Version: 
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2023-10-08 19:26+0200
Last-Translator: Christian Lauinger <christian@lauinger-clan.de>
Language-Team: Dutch
Language: nl
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
X-Generator: Poedit 3.3.2
 Kies een label en opdracht ! Toevoegen Menu toevoegen Toepassen Opdracht Del Bureaublad-voorkeurenbewerker Omlaag Uitbreidingsbeheer Uitbreidingsvoorkeuren Algemeen GNOME-voorkeurenbewerker GNOME Afstelhulp Items Tekst Menutekst Het te tonen label in het menu Laatst gekozen bestand Toon/Verberg de systeemindicator Menu-items Menutekst NVIDIA-voorkeuren De naam van een .desktopbestand (MijnToepassing.desktop) of naam van een opdracht Wachtwoorden en toegangssleutels Kies een .desktopbestand Sessievoorkeuren Voorkeuren Voorkeurenmenu Voorkeurencentrum Systeemindicator tonen Systeemindicator toon-/verbergknop Omhoog Doorgaans te vinden in ‘/usr/share/applications’ 