import Adw from 'gi://Adw';
import GObject from 'gi://GObject';
import Gtk from 'gi://Gtk';

import {SubPage} from './SubPage.js';

import {gettext as _} from 'resource:///org/gnome/Shell/Extensions/js/extensions/prefs.js';

export const WidgetSubPage = GObject.registerClass(
class AzClockWidgetClockSubPage extends SubPage {
    _init(settings, params) {
        super._init(settings, params);

        const generalGroup = new Adw.PreferencesGroup();
        this.add(generalGroup);

        const xLocationRow = this.createSpinRow(_('Location X'), 'Location_X', 0, 10000);
        generalGroup.add(xLocationRow);

        const yLocationRow = this.createSpinRow(_('Location Y'), 'Location_Y', 0, 10000);
        generalGroup.add(yLocationRow);

        const boxAnchorPoint = this.createComboRow(_('Anchor Point'), 'Box_AnchorPoint');
        generalGroup.add(boxAnchorPoint);

        const boxSpacingRow = this.createSpinRow(_('Widget Spacing'), 'Box_Spacing', 0, 500);
        generalGroup.add(boxSpacingRow);

        const paddingExpanderRow = new Adw.ExpanderRow({
            title: _('Widget Padding'),
        });
        generalGroup.add(paddingExpanderRow);

        const paddingTopRow = this.createSpinRow(_('Top'), 'Box_Padding_Top', 0, 500);
        paddingExpanderRow.add_row(paddingTopRow);
        const paddingRightRow = this.createSpinRow(_('Right'), 'Box_Padding_Right', 0, 500);
        paddingExpanderRow.add_row(paddingRightRow);
        const paddingBottomRow = this.createSpinRow(_('Bottom'), 'Box_Padding_Bottom', 0, 500);
        paddingExpanderRow.add_row(paddingBottomRow);
        const paddingLeftRow = this.createSpinRow(_('Left'), 'Box_Padding_Left', 0, 500);
        paddingExpanderRow.add_row(paddingLeftRow);

        const verticalLayoutSwitch = new Gtk.Switch({
            valign: Gtk.Align.CENTER,
            active: this.getClockElementData('Box_VerticalLayout', 'bool'),
        });
        const verticalLayoutRow = new Adw.ActionRow({
            title: _('Vertical Layout'),
            activatable_widget: verticalLayoutSwitch,
        });
        verticalLayoutSwitch.connect('notify::active', widget => {
            this.setClockElementData('Box_VerticalLayout', widget.get_active());
        });
        verticalLayoutRow.add_suffix(verticalLayoutSwitch);
        generalGroup.add(verticalLayoutRow);

        const borderEnabled = this.getClockElementData('Box_BorderEnabled', 'bool');

        const borderOptionsRow = new Adw.ExpanderRow({
            title: _('Enable Border'),
            show_enable_switch: true,
            enable_expansion: borderEnabled,
        });
        generalGroup.add(borderOptionsRow);
        borderOptionsRow.connect('notify::enable-expansion', widget => {
            this.setClockElementData('Box_BorderEnabled', widget.enable_expansion);
        });

        const borderWidthRow = this.createSpinRow(_('Border Width'), 'Box_BorderWidth', 0, 15);
        borderOptionsRow.add_row(borderWidthRow);
        const borderColorRow = this.createColorRow(_('Border Color'), 'Box_BorderColor');
        borderOptionsRow.add_row(borderColorRow);

        const backgroundEnabled = this.getClockElementData('Box_BackgroundEnabled', 'bool');

        const widgetBackgroundRow = new Adw.ExpanderRow({
            title: _('Enable Background'),
            show_enable_switch: true,
            enable_expansion: backgroundEnabled,
        });
        generalGroup.add(widgetBackgroundRow);

        widgetBackgroundRow.connect('notify::enable-expansion', widget => {
            this.setClockElementData('Box_BackgroundEnabled', widget.enable_expansion);
        });

        const widgetBackgroundColorRow = this.createColorRow(_('Background Color'), 'Box_BackgroundColor');
        widgetBackgroundRow.add_row(widgetBackgroundColorRow);
        const borderRadiusRow = this.createSpinRow(_('Background Radius'), 'Box_BorderRadius', 0, 999);
        widgetBackgroundRow.add_row(borderRadiusRow);
    }

    createComboRow(title, setting) {
        const value = this.getClockElementData(setting);
        const stringList = new Gtk.StringList();
        stringList.append(_('Top Left'));
        stringList.append(_('Bottom Left'));
        stringList.append(_('Top Right'));
        stringList.append(_('Bottom Right'));
        stringList.append(_('Center'));

        const comboRow = new Adw.ComboRow({
            title: _(title),
            model: stringList,
            selected: value,
        });
        comboRow.connect('notify::selected', widget => {
            this.setClockElementData(setting, widget.selected);
        });
        return comboRow;
    }
});
