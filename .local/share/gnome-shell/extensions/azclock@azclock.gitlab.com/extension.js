import Clutter from 'gi://Clutter';
import Gio from 'gi://Gio';
import GLib from 'gi://GLib';
import GObject from 'gi://GObject';
import Graphene from 'gi://Graphene';
import Pango from 'gi://Pango';
import St from 'gi://St';

import * as DND from 'resource:///org/gnome/shell/ui/dnd.js';
import {formatDateWithCFormatString} from 'resource:///org/gnome/shell/misc/dateUtils.js';
import * as Main from 'resource:///org/gnome/shell/ui/main.js';
import * as PopupMenu from 'resource:///org/gnome/shell/ui/popupMenu.js';

import {Extension, gettext as _} from 'resource:///org/gnome/shell/extensions/extension.js';

import * as Utils from './utils.js';

Gio._promisify(Gio.Subprocess.prototype, 'communicate_utf8_async');

const DEBUG_LOG = false;

const ANALOG_CLOCK = 'Analog_Clock';
const DIGITAL_CLOCK = 'Digital_Clock';
const TEXT_LABEL = 'Text_Label';
const COMMAND_LABEL = 'Command_Label';
const WIDGET_DATA_INDEX = 0;

let WIDGETDATA, SETTINGS, EXTENSION;

const AnchorPoint = {
    TOP_LEFT: 0,
    BOTTOM_LEFT: 1,
    TOP_RIGHT: 2,
    BOTTOM_RIGHT: 3,
    CENTER: 4,
};

/**
 * @param {string} msg message to log
 */
function debugLog(msg) {
    if (DEBUG_LOG)
        console.log(msg);
}

function getData(widgetIndex, elementIndex, elementName, parseType) {
    return Utils.getData(WIDGETDATA, widgetIndex, elementIndex, elementName, parseType);
}

function setData(widgetIndex, elementIndex, elementName, newValue) {
    Utils.setData(SETTINGS, WIDGETDATA, widgetIndex, elementIndex, elementName, newValue);
}

function getClockDate(widgetIndex, elementIndex, origDate) {
    const overrideTimeZoneEnabled = getData(widgetIndex, elementIndex, 'TimeZoneOverrideEnabled', 'bool');
    const overrideTimeZone = getData(widgetIndex, elementIndex, 'TimeZoneOverride') || 'UTC';

    if (overrideTimeZoneEnabled) {
        const gTimeZone = GLib.TimeZone.new(overrideTimeZone);
        const localDateTime = GLib.DateTime.new_now(gTimeZone);

        const year = localDateTime.get_year();
        const monthIndex = localDateTime.get_month() - 1;
        const day = localDateTime.get_day_of_month();
        const hours = localDateTime.get_hour();
        const minutes = localDateTime.get_minute();
        const seconds = localDateTime.get_second();

        const newDate = new Date(year, monthIndex, day, hours, minutes, seconds);

        return newDate;
    } else {
        return origDate;
    }
}

var DesktopWidget = GObject.registerClass(
class AzClockDesktopWidget extends St.Widget {
    _init(index) {
        super._init({
            layout_manager: new Clutter.BoxLayout(),
            reactive: true,
            track_hover: true,
            can_focus: true,
            x_align: Clutter.ActorAlign.FILL,
            y_align: Clutter.ActorAlign.FILL,
            pivot_point: new Graphene.Point({x: 0.5, y: .5}),
        });

        this._widgetIndex = index;

        const isVertical = getData(this._widgetIndex, WIDGET_DATA_INDEX, 'Box_VerticalLayout', 'bool');
        this.layout_manager.orientation = isVertical ? Clutter.Orientation.VERTICAL : Clutter.Orientation.HORIZONTAL;

        this._menuManager = new PopupMenu.PopupMenuManager(this);

        this.connect('notify::hover', () => this._onHover());
        this.connect('destroy', () => this._onDestroy());

        if (!Utils.getData(WIDGETDATA, index, WIDGET_DATA_INDEX, 'Lock_Widget', 'bool'))
            this.makeDraggable();

        this.createElements();
        this.updateClocks(true);
    }

    vfunc_allocate(box) {
        if (this._isDragging) {
            super.vfunc_allocate(box);
            return;
        }

        const x = getData(this._widgetIndex, WIDGET_DATA_INDEX, 'Location_X', 'int');
        const y = getData(this._widgetIndex, WIDGET_DATA_INDEX, 'Location_Y', 'int');

        const width = box.get_width();
        const height = box.get_height();
        const xAnchorRight = x - width;
        const yAnchorBottom = y - height;
        const xAnchorCenter = x - (width / 2);
        const yAnchorCenter = y - (height / 2);

        const anchorPoint = getData(this._widgetIndex, WIDGET_DATA_INDEX, 'Box_AnchorPoint', 'int');
        if (anchorPoint === AnchorPoint.TOP_LEFT)
            box.set_origin(x, y);
        else if (anchorPoint === AnchorPoint.BOTTOM_LEFT)
            box.set_origin(x, yAnchorBottom);
        else if (anchorPoint === AnchorPoint.TOP_RIGHT)
            box.set_origin(xAnchorRight, y);
        else if (anchorPoint === AnchorPoint.BOTTOM_RIGHT)
            box.set_origin(xAnchorRight, yAnchorBottom);
        else if (anchorPoint === AnchorPoint.CENTER)
            box.set_origin(xAnchorCenter, yAnchorCenter);

        super.vfunc_allocate(box);
    }

    get widgetIndex() {
        return this._widgetIndex;
    }

    set widgetIndex(index) {
        this._widgetIndex = index;
        this._updateIndex(index);
    }

    createElements() {
        this.destroy_all_children();

        const clockData = WIDGETDATA[this._widgetIndex];

        this._analogClocks = [];
        this._digitalClocks = [];
        this._textLabels = [];
        this._commandLabels = [];

        // skip first element as that stores widget layout/style data, not widget elements
        for (let i = 1; i < clockData.length; i++) {
            const clockType = getData(this._widgetIndex, i, 'Element_Type');
            if (clockType === DIGITAL_CLOCK) {
                const digitalClock = new DigitalClock(this._widgetIndex, i);
                this.add_child(digitalClock);
                this._digitalClocks.push(digitalClock);
            } else if (clockType === ANALOG_CLOCK) {
                const analogClock = new AnalogClock(this._widgetIndex, i, clockData[i]);
                this.add_child(analogClock);
                this._analogClocks.push(analogClock);
            } else if (clockType === TEXT_LABEL) {
                const label = new TextLabel(this._widgetIndex, i);
                this.add_child(label);
                this._textLabels.push(label);
            } else if (clockType === COMMAND_LABEL) {
                const commandLabel = new CommandLabel(this._widgetIndex, i);
                this.add_child(commandLabel);
                this._commandLabels.push(commandLabel);
            }
        }

        this.queue_relayout();
    }

    updateClocks(immediate = false) {
        for (const analogClock of this._analogClocks)
            analogClock.updateClock(immediate);
        for (const digitalClock of this._digitalClocks)
            digitalClock.updateClock(immediate);

        this.queue_relayout();
    }

    setElementsStyle() {
        for (const analogClock of this._analogClocks)
            analogClock.setStyle();
        for (const digitalClock of this._digitalClocks)
            digitalClock.setStyle();
        for (const textLabel of this._textLabels)
            textLabel.setStyle();
        for (const commandLabel of this._commandLabels)
            commandLabel.setStyle();

        this.queue_relayout();
    }

    _updateIndex(index) {
        for (const analogClock of this._analogClocks)
            analogClock.widgetIndex = index;
        for (const digitalClock of this._digitalClocks)
            digitalClock.widgetIndex = index;
        for (const textLabel of this._textLabels)
            textLabel.widgetIndex = index;
        for (const commandLabel of this._commandLabels)
            commandLabel.widgetIndex = index;

        this.queue_relayout();
    }

    _refreshCommands() {
        for (const commandLabel of this._commandLabels)
            commandLabel.refreshCommand();

        this.queue_relayout();
    }

    updateComponents(delay = 300) {
        debugLog('update components');
        let priority = GLib.PRIORITY_DEFAULT_IDLE;
        if (delay !== 300)
            priority = GLib.PRIORITY_DEFAULT;

        this._updateClockId = GLib.timeout_add(priority, delay, () => {
            this.setStyle();
            this.setElementsStyle();
            this._refreshCommands();
            this.setPositionFromSettings();
            this.updateClocks(true);
            this.queue_relayout();
            this._updateClockId = null;
            return GLib.SOURCE_REMOVE;
        });
    }

    setPositionFromSettings() {
        const x = getData(this._widgetIndex, WIDGET_DATA_INDEX, 'Location_X', 'int');
        const y = getData(this._widgetIndex, WIDGET_DATA_INDEX, 'Location_Y', 'int');
        debugLog(`set pos from settings - (${x}, ${y})`);
        this.set_position(x, y);
        this.queue_relayout();
    }

    setStyle() {
        const isVertical = getData(this._widgetIndex, WIDGET_DATA_INDEX, 'Box_VerticalLayout', 'bool');
        this.layout_manager.orientation = isVertical ? Clutter.Orientation.VERTICAL : Clutter.Orientation.HORIZONTAL;

        const borderEnabled = getData(this._widgetIndex, WIDGET_DATA_INDEX, 'Box_BorderEnabled', 'bool');
        const borderWidth = getData(this._widgetIndex, WIDGET_DATA_INDEX, 'Box_BorderWidth', 'int');
        const borderRadius = getData(this._widgetIndex, WIDGET_DATA_INDEX, 'Box_BorderRadius', 'int');
        const borderColor = getData(this._widgetIndex, WIDGET_DATA_INDEX, 'Box_BorderColor');
        const backgroundEnabled = getData(this._widgetIndex, WIDGET_DATA_INDEX, 'Box_BackgroundEnabled', 'bool');
        const backgroundColor = getData(this._widgetIndex, WIDGET_DATA_INDEX, 'Box_BackgroundColor');
        const boxSpacing = getData(this._widgetIndex, WIDGET_DATA_INDEX, 'Box_Spacing', 'int');

        const paddingTop = getData(this.widgetIndex, WIDGET_DATA_INDEX, 'Box_Padding_Top', 'int');
        const paddingLeft = getData(this.widgetIndex, WIDGET_DATA_INDEX, 'Box_Padding_Left', 'int');
        const paddingBottom = getData(this.widgetIndex, WIDGET_DATA_INDEX, 'Box_Padding_Bottom', 'int');
        const paddingRight = getData(this.widgetIndex, WIDGET_DATA_INDEX, 'Box_Padding_Right', 'int');

        this.layoutManager.spacing = boxSpacing;

        let style = `padding: ${paddingTop}px ${paddingRight}px ${paddingBottom}px ${paddingLeft}px;`;

        if (backgroundEnabled) {
            style += `background-color: ${backgroundColor};
                      border-radius: ${borderRadius}px;`;
        }

        if (borderEnabled) {
            style += `border-width: ${borderWidth}px;
                      border-color: ${borderColor};`;
        }

        this.style = style;

        this.z_position = WIDGETDATA.length - this._widgetIndex;
        this.queue_relayout();
    }

    vfunc_button_press_event() {
        const event = Clutter.get_current_event();

        if (event.get_button() === 1) {
            this._setPopupTimeout();
        } else if (event.get_button() === 3) {
            this._popupMenu();
            return Clutter.EVENT_STOP;
        }

        return Clutter.EVENT_PROPAGATE;
    }

    vfunc_button_release_event() {
        this._removeMenuTimeout();
        return Clutter.EVENT_PROPAGATE;
    }

    _onDragBegin() {
        if (this._menu)
            this._menu.close(true);
        this._removeMenuTimeout();

        this._isDragging = true;
        this._dragMonitor = {
            dragMotion: this._onDragMotion.bind(this),
        };
        DND.addDragMonitor(this._dragMonitor);

        this._dragX = this.x;
        this._dragY = this.y;
    }

    _onDragMotion(dragEvent) {
        this.deltaX = dragEvent.x - (dragEvent.x - this._dragX);
        this.deltaY = dragEvent.y - (dragEvent.y - this._dragY);

        const [x, y] = this.allocation.get_origin();
        this._dragX = x;
        this._dragY = y;

        return DND.DragMotionResult.CONTINUE;
    }

    _onDragEnd() {
        this._isDragging = false;
        if (this._dragMonitor) {
            DND.removeDragMonitor(this._dragMonitor);
            this._dragMonitor = null;
        }

        let [x, y] = [Math.round(this.deltaX), Math.round(this.deltaY)];
        const width = this.get_width();
        const height = this.get_height();
        const newX = x + width;
        const newY = y + height;
        const centerX = x + (width / 2);
        const centerY = y + (height / 2);

        const anchorPoint = getData(this._widgetIndex, WIDGET_DATA_INDEX, 'Box_AnchorPoint', 'int');
        if (anchorPoint === AnchorPoint.BOTTOM_LEFT) {
            y = newY;
        } else if (anchorPoint === AnchorPoint.TOP_RIGHT) {
            x = newX;
        } else if (anchorPoint === AnchorPoint.BOTTOM_RIGHT) {
            x = newX;
            y = newY;
        } else if (anchorPoint === AnchorPoint.CENTER) {
            x = centerX;
            y = centerY;
        }

        SETTINGS.set_value('changed-data', new GLib.Variant('a{ss}', {
            'WidgetIndex': this._widgetIndex.toString(),
            'WidgetMoved': 'true',
        }));
        setData(this._widgetIndex, WIDGET_DATA_INDEX, 'Location_X', x);
        setData(this._widgetIndex, WIDGET_DATA_INDEX, 'Location_Y', y);
        debugLog(`drag end - (${x}, ${y})`);
    }

    getDragActorSource() {
        return this;
    }

    makeDraggable() {
        this._draggable = DND.makeDraggable(this);
        this._draggable._dragActorDropped = () => {
            this._draggable._animationInProgress = true;
            this._draggable._dragCancellable = false;
            this._draggable._dragState = 0; // DND.DragState.INIT;
            this._draggable._onAnimationComplete(this._draggable._dragActor, Clutter.get_current_event().get_time());
            return true;
        };

        this.dragBeginId = this._draggable.connect('drag-begin', this._onDragBegin.bind(this));
        this.dragEndId = this._draggable.connect('drag-end', this._onDragEnd.bind(this));
    }

    _onHover() {
        if (!this.hover)
            this._removeMenuTimeout();
    }

    _removeMenuTimeout() {
        if (this._menuTimeoutId > 0) {
            GLib.source_remove(this._menuTimeoutId);
            this._menuTimeoutId = 0;
        }
    }

    _setPopupTimeout() {
        this._removeMenuTimeout();
        this._menuTimeoutId = GLib.timeout_add(GLib.PRIORITY_DEFAULT, 600, () => {
            this._menuTimeoutId = 0;
            this._popupMenu();
            return GLib.SOURCE_REMOVE;
        });
        GLib.Source.set_name_by_id(this._menuTimeoutId, '[azclock] this.popupMenu');
    }

    _popupMenu() {
        this._removeMenuTimeout();

        if (!this._menu) {
            this._menu = new PopupMenu.PopupMenu(this, 0.5, St.Side.TOP);

            const lockWidget = getData(this._widgetIndex, WIDGET_DATA_INDEX, 'Lock_Widget', 'bool');
            const lockPositionSwitch = new PopupMenu.PopupSwitchMenuItem(_('Lock Position'), lockWidget);
            this._menu.addMenuItem(lockPositionSwitch);
            lockPositionSwitch.connect('toggled', (_self, toggled) => {
                this._menu.close();

                SETTINGS.set_value('changed-data', new GLib.Variant('a{ss}', {
                    'WidgetIndex': this._widgetIndex.toString(),
                    'ElementIndex': WIDGET_DATA_INDEX.toString(),
                    'ElementType': 'Lock_Widget',
                }));

                setData(this._widgetIndex, WIDGET_DATA_INDEX, 'Lock_Widget', toggled);
            });

            const alwaysOnTop = getData(this._widgetIndex, WIDGET_DATA_INDEX, 'Always_On_Top', 'bool');
            const alwaysOnTopSwitch = new PopupMenu.PopupSwitchMenuItem(_('Always on Top'), alwaysOnTop);
            this._menu.addMenuItem(alwaysOnTopSwitch);
            alwaysOnTopSwitch.connect('toggled', (_self, toggled) => {
                this._menu.close();

                SETTINGS.set_value('changed-data', new GLib.Variant('a{ss}', {
                    'WidgetIndex': this._widgetIndex.toString(),
                    'ElementIndex': WIDGET_DATA_INDEX.toString(),
                    'ElementType': 'Always_On_Top',
                }));

                setData(this._widgetIndex, WIDGET_DATA_INDEX, 'Always_On_Top', toggled);
            });

            this._menu.addMenuItem(new PopupMenu.PopupSeparatorMenuItem());

            this._menu.addAction(_('Desktop Clock Settings'), () => {
                EXTENSION.openPreferences();
            });

            Main.uiGroup.add_actor(this._menu.actor);
            this._menuManager.addMenu(this._menu);
        }

        this._menu.open();
        return false;
    }

    _onDestroy() {
        this._removeMenuTimeout();

        if (this._updateClockId) {
            GLib.source_remove(this._updateClockId);
            this._updateClockId = null;
        }

        if (this._dragMonitor) {
            DND.removeDragMonitor(this._dragMonitor);
            this._dragMonitor = null;
        }
    }
});

var AnalogClock = GObject.registerClass(
class AzClockAnalogClock extends Clutter.Actor {
    _init(widgetIndex, elementIndex, elementData) {
        super._init({
            layout_manager: new Clutter.BinLayout(),
            x_align: Clutter.ActorAlign.FILL,
            y_align: Clutter.ActorAlign.FILL,
        });

        this.widgetIndex = widgetIndex;
        this.elementIndex = elementIndex;

        this._clockFace = new St.Icon({
            x_align: Clutter.ActorAlign.FILL,
            y_align: Clutter.ActorAlign.FILL,
            visible: elementData['ClockFace_Visible'] === 'true',
        });

        this._secondHand = new St.Icon({
            pivot_point: new Graphene.Point({x: 0.5, y: .5}),
            y_expand: false,
            y_align: Clutter.ActorAlign.START,
            visible: elementData['SecondHand_Visible'] === 'true',
        });

        this._minuteHand = new St.Icon({
            pivot_point: new Graphene.Point({x: 0.5, y: .5}),
            y_align: Clutter.ActorAlign.START,
        });

        this._hourHand = new St.Icon({
            pivot_point: new Graphene.Point({x: 0.5, y: .5}),
            y_align: Clutter.ActorAlign.START,
        });

        this._clockButton = new St.Icon({
            pivot_point: new Graphene.Point({x: 0.5, y: .5}),
            x_align: Clutter.ActorAlign.FILL,
            y_align: Clutter.ActorAlign.FILL,
            visible: elementData['ClockButton_Visible'] === 'true',
        });

        this.add_actor(this._clockFace);
        this.add_actor(this._hourHand);
        this.add_actor(this._minuteHand);
        this.add_actor(this._secondHand);
        this.add_actor(this._clockButton);
    }

    setAnalogClockStyle(element, elementName) {
        const clockData = WIDGETDATA[this.widgetIndex];
        const directoryName = elementName.toLowerCase();
        const filePath = `${EXTENSION.path}/media/${directoryName}/${directoryName}`;

        const data = clockData[this.elementIndex];
        const styleType = data[`${elementName}_Style`] || 1;

        element.style = this.getStyle(data, elementName);

        if (elementName === 'ClockFace' || elementName === 'SecondHand' || elementName === 'ClockButton')
            element.visible = getData(this.widgetIndex, this.elementIndex, `${elementName}_Visible`, 'bool');

        element.gicon = Gio.icon_new_for_string(`${filePath}-${styleType}-symbolic.svg`);
        element.icon_size = parseInt(data['Clock_Size']);
    }

    getStyle(data, element) {
        let style = `color: ${data[`${element}_Color`]};`;

        const backgroundColor = data[`${element}_BackgroundColor`];
        const borderRadius = data[`${element}_BorderRadius`];
        const borderEnabled = data[`${element}_BorderEnabled`] === 'true';
        const shadowEnabled = data[`${element}_ShadowEnabled`] === 'true';
        const boxShadowEnabled = data[`${element}_BoxShadowEnabled`] === 'true';

        if (backgroundColor)
            style += `background-color: ${backgroundColor};`;
        if (borderRadius)
            style += `border-radius: ${borderRadius}px;`;

        if (borderEnabled) {
            const borderWidth = data[`${element}_BorderWidth`];
            const borderColor = data[`${element}_BorderColor`];
            if (borderWidth)
                style += `border: ${borderWidth}px;`;
            if (borderColor)
                style += `border-color: ${borderColor};`;
        }

        if (shadowEnabled) {
            const shadow = `${element}_Shadow`;
            style += `icon-shadow: ${data[`${shadow}X`]}px ${data[`${shadow}Y`]}px ${data[`${shadow}Blur`]}px ${data[`${shadow}Spread`]}px ${data[`${shadow}Color`]};`;
        }
        if (boxShadowEnabled) {
            const boxShadow = `${element}_BoxShadow`;
            style += `box-shadow: ${data[`${boxShadow}X`]}px ${data[`${boxShadow}Y`]}px ${data[`${boxShadow}Blur`]}px ${data[`${boxShadow}Spread`]}px ${data[`${boxShadow}Color`]};`;
        }
        return style;
    }

    setStyle() {
        this.setAnalogClockStyle(this._clockFace, 'ClockFace');
        this.setAnalogClockStyle(this._secondHand, 'SecondHand');
        this.setAnalogClockStyle(this._minuteHand, 'MinuteHand');
        this.setAnalogClockStyle(this._hourHand, 'HourHand');
        this.setAnalogClockStyle(this._clockButton, 'ClockButton');
    }

    updateClock(immediate = false) {
        const date = new Date();

        const elementDate = getClockDate(this.widgetIndex, this.elementIndex, date);
        this.tickClock(elementDate, immediate);

        this.queue_relayout();
    }

    tickClock(date, immediate) {
        // Keep hours in 12 hour format for analog clock
        if (date.getHours() >= 12)
            date.setHours(date.getHours() - 12);

        const degrees = 6; // each minute and second tick represents a 6 degree increment.
        const secondsInDegrees = date.getSeconds() * degrees;
        const minutesInDegrees = date.getMinutes() * degrees;
        const hoursInDegrees = date.getHours() * 30;

        if (this._secondHand.visible)
            this.tickClockHand(this._secondHand, secondsInDegrees, immediate);

        const adjustMinutesWithSeconds = getData(this.widgetIndex, this.elementIndex, 'MinuteHand_AdjustWithSeconds', 'bool');
        const minutesRotationDegree = adjustMinutesWithSeconds ? minutesInDegrees + secondsInDegrees / 60 : minutesInDegrees;
        this.tickClockHand(this._minuteHand, minutesRotationDegree, immediate);
        this.tickClockHand(this._hourHand, hoursInDegrees + minutesInDegrees / 12, immediate);
    }

    tickClockHand(hand, rotationDegree, immediate) {
        const smoothTicks = getData(this.widgetIndex, this.elementIndex, 'Clock_SmoothTicks', 'bool');
        // eslint-disable-next-line no-nested-ternary
        const duration = immediate ? 0 : smoothTicks ? 1000 : 300;
        hand.remove_all_transitions();

        // The onComplete() of the hand.ease() might not trigger when removing the transition.
        if (hand.checkRotationDegree) {
            hand.checkRotationDegree = false;
            if (hand.rotation_angle_z !== 0)
                hand.rotation_angle_z = 0;
        }

        if (rotationDegree === hand.rotation_angle_z)
            return;

        // Prevents the clock hand from spinning counter clockwise back to 0.
        if (rotationDegree === 0 && hand.rotation_angle_z !== 0) {
            rotationDegree = 360;
            hand.checkRotationDegree = true;
        }


        hand.ease({
            opacity: 255, // onComplete() seems to trigger instantly without this.
            rotation_angle_z: rotationDegree,
            mode: smoothTicks ? Clutter.AnimationMode.LINEAR : Clutter.AnimationMode.EASE_OUT_QUAD,
            duration,
            onComplete: () => {
                // Prevents the clock hand from spinning counter clockwise back to 0.
                if (rotationDegree === 360)
                    hand.rotation_angle_z = 0;
            },
        });
    }
});

var Label = GObject.registerClass(
class AzClockLabel extends St.Label {
    _init(widgetIndex, elementIndex) {
        super._init({
            y_align: Clutter.ActorAlign.CENTER,
            pivot_point: new Graphene.Point({x: 0.5, y: 0.5}),
        });

        this.widgetIndex = widgetIndex;
        this.elementIndex = elementIndex;

        this.clutter_text.set({
            ellipsize: Pango.EllipsizeMode.NONE,
        });
    }

    setStyle() {
        const index = this.elementIndex;

        const shadowEnabled = getData(this.widgetIndex, index, 'Text_ShadowEnabled', 'bool');
        const shadowX = getData(this.widgetIndex, index, 'Text_ShadowX');
        const shadowY = getData(this.widgetIndex, index, 'Text_ShadowY');
        const shadowSpread = getData(this.widgetIndex, index, 'Text_ShadowSpread');
        const shadowBlur = getData(this.widgetIndex, index, 'Text_ShadowBlur');
        const shadowColor = getData(this.widgetIndex, index, 'Text_ShadowColor');

        const customFontEnabled = getData(this.widgetIndex, index, 'Text_CustomFontEnabled', 'bool');
        const customFontFamily = getData(this.widgetIndex, index, 'Text_CustomFontFamily');

        const textColor = getData(this.widgetIndex, index, 'Text_Color');
        const textSize = getData(this.widgetIndex, index, 'Text_Size');

        const textAlignmentX = getData(this.widgetIndex, index, 'Text_AlignmentX', 'clutter_align');
        const textAlignmentY = getData(this.widgetIndex, index, 'Text_AlignmentY', 'clutter_align');
        const textLineAlignment = getData(this.widgetIndex, index, 'Text_LineAlignment', 'align');

        const marginTop = getData(this.widgetIndex, index, 'Element_Margin_Top', 'int');
        const marginLeft = getData(this.widgetIndex, index, 'Element_Margin_Left', 'int');
        const marginBottom = getData(this.widgetIndex, index, 'Element_Margin_Bottom', 'int');
        const marginRight = getData(this.widgetIndex, index, 'Element_Margin_Right', 'int');

        const paddingTop = getData(this.widgetIndex, index, 'Element_Padding_Top', 'int');
        const paddingLeft = getData(this.widgetIndex, index, 'Element_Padding_Left', 'int');
        const paddingBottom = getData(this.widgetIndex, index, 'Element_Padding_Bottom', 'int');
        const paddingRight = getData(this.widgetIndex, index, 'Element_Padding_Right', 'int');

        const borderEnabled = getData(this.widgetIndex, index, 'Text_BorderEnabled', 'bool');
        const borderWidth = getData(this.widgetIndex, index, 'Text_BorderWidth', 'int');
        const borderRadius = getData(this.widgetIndex, index, 'Text_BorderRadius', 'int');
        const borderColor = getData(this.widgetIndex, index, 'Text_BorderColor');
        const backgroundEnabled = getData(this.widgetIndex, index, 'Text_BackgroundEnabled', 'bool');
        const backgroundColor = getData(this.widgetIndex, index, 'Text_BackgroundColor');

        const margin = `margin: ${marginTop}px ${marginRight}px ${marginBottom}px ${marginLeft}px;`;
        const padding = `padding: ${paddingTop}px ${paddingRight}px ${paddingBottom}px ${paddingLeft}px;`;

        let textStyle = `color: ${textColor}; ${margin} ${padding}`;

        if (backgroundEnabled) {
            textStyle += `background-color: ${backgroundColor};
                            border-radius: ${borderRadius}px;`;
        }

        if (borderEnabled) {
            textStyle += `border-width: ${borderWidth}px;
                            border-color: ${borderColor};`;
        }

        if (shadowEnabled)
            textStyle += `text-shadow: ${shadowX}px ${shadowY}px ${shadowBlur}px ${shadowSpread}px ${shadowColor};`;

        if (customFontEnabled) {
            const fontStyleEnum = getData(this.widgetIndex, index, 'Text_CustomFontStyle', 'int');
            const fontStyle = fontStyleEnumToString(fontStyleEnum);
            const fontWeight = getData(this.widgetIndex, index, 'Text_CustomFontWeight', 'int');

            textStyle += `font-family: "${customFontFamily}";`;

            if (fontWeight)
                textStyle += `font-weight: ${fontWeight};`;
            if (fontStyle)
                textStyle += `font-style: ${fontStyle};`;
        }

        if (textLineAlignment)
            textStyle += `text-align: ${textLineAlignment};`;

        this.style = `${textStyle} font-size: ${textSize}pt; font-feature-settings: "tnum";`;

        this.x_align = textAlignmentX;
        this.y_align = textAlignmentY;
        this.queue_relayout();
    }
});

var DigitalClock = GObject.registerClass(
class AzClockDigitalClock extends Label {
    _init(widgetIndex, elementIndex) {
        super._init(widgetIndex, elementIndex);
    }

    setStyle() {
        super.setStyle();
        const dateFormat = getData(this.widgetIndex, this.elementIndex, 'Text_DateFormat');
        this._dateFormat = dateFormat;
    }

    updateClock() {
        const date = new Date();

        const dateFormat = this._dateFormat;
        const elementDate = getClockDate(this.widgetIndex, this.elementIndex, date);

        if (dateFormat) {
            this.text = formatDateWithCFormatString(elementDate, dateFormat);
            this.clutter_text.set_markup(this.text);
        }

        this.queue_relayout();
    }
});

var TextLabel = GObject.registerClass(
class AzClockTextLabel extends Label {
    _init(widgetIndex, elementIndex) {
        super._init(widgetIndex, elementIndex);
    }

    setStyle() {
        super.setStyle();
        const text = getData(this.widgetIndex, this.elementIndex, 'Text_Text');
        this.text = text;
        this.clutter_text.set_markup(this.text);
        this.queue_relayout();
    }
});

var CommandLabel = GObject.registerClass(
class AzClockCommandLabel extends Label {
    _init(widgetIndex, elementIndex) {
        super._init(widgetIndex, elementIndex);

        this.connect('destroy', () => this._removePollingInterval());

        this._hasError = false;
    }

    _setErrorState() {
        this._removePollingInterval();
        this._hasError = true;
        this.text = _('error');
        this.clutter_text.set_markup(this.text);
    }

    refreshCommand() {
        this._hasError = false;
        this._removePollingInterval();
        this._executeCommand();
        this._startPollingInterval();
    }

    _startPollingInterval() {
        const interval = Math.max(getData(this.widgetIndex, this.elementIndex, 'Text_PollingInterval'), 250);
        this._pollingIntervalId = GLib.timeout_add(GLib.PRIORITY_HIGH, interval, () => {
            this._executeCommand();
            return GLib.SOURCE_CONTINUE;
        });
    }

    _removePollingInterval() {
        if (this._pollingIntervalId) {
            GLib.source_remove(this._pollingIntervalId);
            this._pollingIntervalId = null;
        }
    }

    _executeCommand() {
        const command = getData(this.widgetIndex, this.elementIndex, 'Text_Command');
        this._execCommand(command);
    }

    async _execCommand(command, input = null, cancellable = null) {
        if (!command || command.length === 0) {
            this._setErrorState();
            console.log('Desktop Clock - Error executing command. No command detected.');
            return;
        }
        try {
            const argv = ['bash', '-c', command];

            let flags = Gio.SubprocessFlags.STDOUT_PIPE |
                Gio.SubprocessFlags.STDERR_PIPE;

            if (input !== null)
                flags |= Gio.SubprocessFlags.STDIN_PIPE;

            const proc = Gio.Subprocess.new(argv, flags);

            const [stdout, stderr] = await proc.communicate_utf8_async(input, cancellable);

            if (!proc.get_successful() || stderr) {
                this._setErrorState();
                const status = proc.get_exit_status();
                console.log(`Desktop Clock - Error executing command "${command}": ${stderr ? stderr.trim() : GLib.strerror(status)}`);
                return;
            }

            const response = stdout.trim();

            if (!response) {
                this._setErrorState();
                console.log(`Desktop Clock - Error executing command "${command}": no output.`);
                return;
            }

            this.text = response;
            this.clutter_text.set_markup(this.text);
            this.queue_relayout();
        } catch (err) {
            this._setErrorState();
            console.log(`Desktop Clock - Error executing command "${command}": ${err}`);
        }
    }
});

export default class AzClock extends Extension {
    enable() {
        EXTENSION = this;
        SETTINGS = this.getSettings();
        this._updateWidgetData();
        this._createWidgets();
        this._startClockTimer();
    }

    disable() {
        SETTINGS.set_value('changed-data', new GLib.Variant('a{ss}', {}));

        SETTINGS.disconnectObject(this);
        Main.layoutManager.disconnectObject(this);

        if (this._dataChangedTimeoutId) {
            GLib.source_remove(this._dataChangedTimeoutId);
            this._dataChangedTimeoutId = null;
        }

        this._removeClockTimer();
        this._destroyWidgets();

        WIDGETDATA = null;
        EXTENSION = null;
        SETTINGS = null;
    }

    _startClockTimer() {
        this._updateClockId = GLib.timeout_add(GLib.PRIORITY_HIGH, 1000, () => {
            for (const widget of this.widgets)
                widget.updateClocks();

            return GLib.SOURCE_CONTINUE;
        });
    }

    _removeClockTimer() {
        if (this._updateClockId) {
            GLib.source_remove(this._updateClockId);
            this._updateClockId = null;
        }
    }

    _destroyWidget(index) {
        const widget = this.widgets[index];
        this.widgets.splice(index, 1);
        widget.destroy();
    }

    _destroyWidgets() {
        for (const widget of this.widgets)
            widget.destroy();

        this.widgets = null;
    }

    _createWidget(index, updateDelay = 300) {
        const widget = new DesktopWidget(index);

        const alwaysOnTop = getData(index, WIDGET_DATA_INDEX, 'Always_On_Top', 'bool');

        if (alwaysOnTop)
            Main.layoutManager.addTopChrome(widget);
        else
            Main.layoutManager._backgroundGroup.add_child(widget);

        widget.updateComponents(updateDelay);
        return widget;
    }

    _createWidgets() {
        this.widgets = [];

        for (let i = 0; i < WIDGETDATA.length; i++) {
            const widget = this._createWidget(i);
            this.widgets.push(widget);
        }

        SETTINGS.connectObject('changed::widget-data', () => {
            if (this._dataChangedTimeoutId)
                GLib.source_remove(this._dataChangedTimeoutId);

            this._dataChangedTimeoutId = GLib.timeout_add(GLib.PRIORITY_DEFAULT, 300, () => {
                this._updateWidgetData();
                this._widgetDataChangedEvent();
                this._dataChangedTimeoutId = null;
                return GLib.SOURCE_REMOVE;
            });
        }, this);

        Main.layoutManager.connectObject('monitors-changed', () => {
            if (global.display.get_n_monitors() === 0)
                return;

            debugLog('Monitors-changed event');
            this._updateWidgetData();
            for (const widget of this.widgets) {
                widget.setPositionFromSettings();
                this._updateWidgetData();
            }
        }, this);
    }

    _updateWidgetData() {
        debugLog('update widget data');
        WIDGETDATA = Utils.unpackData(SETTINGS);
    }

    _updateWidgetsIndex() {
        for (let i = 0; i < this.widgets.length; i++) {
            this.widgets[i].widgetIndex = i;
            this.widgets[i].z_position = this.widgets.length - i;
        }
    }

    _widgetDataChangedEvent() {
        const changedData = SETTINGS.get_value('changed-data').deep_unpack();
        const widgetIndex = parseInt(changedData['WidgetIndex']);
        const elementIndex_ = parseInt(changedData['ElementIndex']);
        const elementType = changedData['ElementType'];
        const widgetDeleted = changedData['WidgetDeleted'];
        const widgetAdded = changedData['WidgetAdded'];
        const elementDeleted = changedData['ElementDeleted'];
        const elementAdded = changedData['ElementAdded'];
        const widgetMoved = changedData['WidgetMoved'];
        const widgetIndexChanged = changedData['WidgetIndexChanged'];
        const widgetIndexNew = parseInt(changedData['WidgetIndexNew']);
        const elementIndexChanged = changedData['ElementIndexChanged'];

        if (widgetIndex === undefined) {
            debugLog('update all this.widgets');

            for (const widget of this.widgets)
                widget.updateComponents();
        } else if (widgetIndexChanged) {
            debugLog(`widget index changed from ${widgetIndex} to ${widgetIndexNew}`);

            const movedWidget = this.widgets.splice(widgetIndex, 1)[0];
            this.widgets.splice(widgetIndexNew, 0, movedWidget);

            this._updateWidgetsIndex();
        } else if (widgetMoved) {
            debugLog(`widget ${widgetIndex} moved`);
        } else if (widgetAdded) {
            debugLog(`widget ${widgetIndex} created`);

            const widget = this._createWidget(widgetIndex);
            this.widgets.push(widget);
            this._updateWidgetsIndex();
        } else if (widgetDeleted) {
            debugLog(`widget ${widgetIndex} deleted`);

            this._destroyWidget(widgetIndex);
            this._updateWidgetsIndex();
        } else if (elementType === 'Lock_Widget') {
            debugLog(`widget ${widgetIndex} lock position changed`);

            const oldWidget = this.widgets[widgetIndex];
            this.widgets[widgetIndex] = this._createWidget(widgetIndex, 0);
            oldWidget.destroy();
        } else if (elementType === 'Always_On_Top') {
            debugLog(`widget ${widgetIndex} always on top changed`);

            const oldWidget = this.widgets[widgetIndex];
            this.widgets[widgetIndex] = this._createWidget(widgetIndex, 0);
            oldWidget.destroy();
        } else if (elementDeleted || elementAdded || elementIndexChanged) {
            debugLog(`element added/deleted/index-changed at widget ${widgetIndex}`);

            this.widgets[widgetIndex].createElements();
            this.widgets[widgetIndex].updateComponents(0);
        } else {
            debugLog(`widget ${widgetIndex} element setting changed`);

            this.widgets[widgetIndex].updateComponents();
        }

        SETTINGS.set_value('changed-data', new GLib.Variant('a{ss}', {}));
    }
}

function fontStyleEnumToString(enumValue) {
    switch (enumValue) {
    case Pango.Style.NORMAL:
        return null;
    case Pango.Style.OBLIQUE:
        return 'oblique';
    case Pango.Style.ITALIC:
        return 'italic';
    default:
        return null;
    }
}
