#!/usr/bin/env python

import os
from glob import glob

hidPath = f"{os.getcwd()}/.hidden"

filePtrn = ['*.out']

ff = []
for p in filePtrn:
    ff += glob(p)

filesToHide ='\n' + '\n'.join(ff)

with open(hidPath, 'a' if os.path.exists(hidPath) else 'w') as f:
    f.write(filesToHide)
