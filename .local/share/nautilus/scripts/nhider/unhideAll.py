#!/usr/bin/env python

import os

hidPath = f"{os.getcwd()}/.hidden"
if os.path.exists(hidPath):
    os.remove(hidPath)
