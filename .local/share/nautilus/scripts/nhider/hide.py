#!/usr/bin/env python

import os, sys

hidPath = f"{os.getcwd()}/.hidden"

filesToHide ='\n' + '\n'.join(sys.argv[1:])

with open(hidPath, 'a' if os.path.exists(hidPath) else 'w') as f:
    f.write(filesToHide)
