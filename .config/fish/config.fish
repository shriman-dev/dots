## Set values
# Hide welcome message
set fish_greeting
set VIRTUAL_ENV_DISABLE_PROMPT "1"
set -x MANROFFOPT "-c"
set -x MANPAGER "sh -c 'col -bx | bat -l man -p'"

## Export variable need for qt-theme
if type "qtile" >> /dev/null 2>&1
   set -x QT_QPA_PLATFORMTHEME "qt5ct"
end

# Set settings for https://github.com/franciscolourenco/done
set -U __done_min_cmd_duration 10000
set -U __done_notification_urgency_level low

# python ta-lib
set -x TA_INCLUDE_PATH "$HOME/.nix-profile/include"
set -x TA_LIBRARY_PATH "$HOME/.nix-profile/lib"

## Environment setup
# Apply .profile: use this to put fish compatible .profile stuff in

if test -f ~/.fish_profile
  source ~/.fish_profile
end

# Add PATHs
    if not contains -- ~/.local/bin ~/.local/podman/bin ~/.local/bin/bim $PATH
        set -p PATH ~/.local/bin ~/.local/podman/bin ~/.local/bin/bim
    end
#~/Bkups/Android ~/Bkups/Android/platform-tools ~/Bkups/Android/cmdline-tools ~/Bkups/Android/cmdline-tools/bin

# nix to PATH
#$HOME/.nix-profile/etc/profile.d/nix.fish | source

# Add depot_tools to PATH
if test -d ~/Applications/depot_tools
    if not contains -- ~/Applications/depot_tools $PATH
        set -p PATH ~/Applications/depot_tools
    end
end

# get cool terminal fonts
#source ~/.local/share/icons-in-terminal/icons.fish

## Starship prompt
if status --is-interactive
   source ("starship" init fish --print-full-init | psub)
end


## Advanced command-not-found hook
#source /usr/share/doc/find-the-command/ftc.fish


## Functions
# Functions needed for !! and !$ https://github.com/oh-my-fish/plugin-bang-bang
function __history_previous_command
  switch (commandline -t)
  case "!"
    commandline -t $history[1]; commandline -f repaint
  case "*"
    commandline -i !
  end
end

function __history_previous_command_arguments
  switch (commandline -t)
  case "!"
    commandline -t ""
    commandline -f history-token-search-backward
  case "*"
    commandline -i '$'
  end
end

if [ "$fish_key_bindings" = fish_vi_key_bindings ];
  bind -Minsert ! __history_previous_command
  bind -Minsert '$' __history_previous_command_arguments
else
  bind ! __history_previous_command
  bind '$' __history_previous_command_arguments
end

# Fish command history
function history
    builtin history --show-time='%F %T '
end

function backup --argument filename
    cp $filename $filename.bak
end

# Copy DIR1 DIR2
function copy
    set count (count $argv | tr -d \n)
    if test "$count" = 2; and test -d "$argv[1]"
	set from (echo $argv[1] | trim-right /)
	set to (echo $argv[2])
        command cp -r $from $to
    else
        command cp $argv
    end
end

## Useful aliases
# Common use
alias c='clear'
alias ls='exa -al --color=always --group-directories-first --icons' # preferred listing # Replace ls with exa
alias la='exa -a --color=always --group-directories-first --icons'  # all files and dirs
alias ll='exa -l --color=always --group-directories-first --icons'  # long format
alias lt='exa -aT --color=always --group-directories-first --icons' # tree listing
alias l.="exa -a | egrep '^\.'"                                     # show only dotfiles
alias ip="ip -color"
#alias cat='bat --style header --style snip --style changes --style header' # Replace some more things with better alternatives
thefuck --alias | source
alias f="thefuck"
alias ramcln="sudo $HOME/Honk/Scripts/loonix/ramclean.sh"
alias noannoy="$HOME/Honk/Scripts/loonix/no-annoy.sh"
alias grubup="$HOME/Honk/Scripts/loonix/grubup.sh"
alias diskref="$HOME/Honk/Scripts/loonix/disk-man.sh refresh"
alias btr-care="$HOME/Honk/Scripts/loonix/disk-man.sh butter-care"
alias fcc="fc-cache -fvr --really-force "
alias tarnow='tar -acf '
alias untar='tar -xvf '
alias wget='wget -c '
alias psmem='ps auxf | sort -nr -k 4'
alias psmem10='ps auxf | sort -nr -k 4 | head -10'
alias ..='cd ..'
alias ...='cd ../..'
alias ....='cd ../../..'
alias .....='cd ../../../..'
alias ......='cd ../../../../..'
alias dir='dir --color=auto'
alias vdir='vdir --color=auto'
alias grep='grep --color=auto'
alias fgrep='fgrep --color=auto'
alias egrep='egrep --color=auto'
alias hw='hwinfo --short'                          # Hardware Info
alias bigpac="expac -H M '%m\t%n' | sort -h | nl"     # Sort installed packages according to size in MB
alias jctl="journalctl -p 3 -xb" # Get the error messages from journalctl
alias netproc="sudo $HOME/.nix-profile/bin/netproc  -B -c -v"
alias gedit="/usr/bin/flatpak run --branch=stable --arch=x86_64 --command=gedit --file-forwarding org.gnome.gedit"


# flatpak alias
alias flup="flatpak update --user"
alias flin="flatpak install --user"
alias flre="flatpak install --user --reinstall"
alias flrn="flatpak run"
alias fdgl="flatpak remote-info --log flathub"
alias fldg="echo -e 'put commit hash app id right after alias \nand then mask the app \n \n' && flatpak update --commit="
alias flrm="flatpak uninstall"
alias fcln="flatpak uninstall --user --unused && find $HOME/.var/ -type d -name "cache" -exec rm -rvf {} +"
alias flss="flatpak search"
alias fhis="flatpak history"
alias finf="flatpak info"
alias fist="flatpak list --columns=installation,name,size,application,version,runtime"
alias fmsk="flatpak mask"
alias flrp="flatpak repair --user"

# distrobox alias
alias arch-btw="distrobox enter arch-btw -- fish"
alias fedog="distrobox enter fedora-box-gaming -- fish"
alias uwubun="distrobox enter uwubuntu -- fish"

alias hoex="distrobox-host-exec"
alias disa="distrobox-export -a"

function undisa
    distrobox-export -a $argv --delete
end
function disb
    $HOME/.local/bin/distrobox-export --bin $argv --export-path $HOME/.local/bin
end
function undisb
    $HOME/.local/bin/distrobox-export --bin $argv --export-path $HOME/.local/bin --delete
end

# nix alias
alias niup="$HOME/Honk/Scripts/pkg-install/nixpkg.sh u"
alias niin="nix-env -iA"
alias nire="nix-collect-garbage && nix-env -iA"
alias ndgl="nix-env -qaP"
alias nidg="nix-env -u --always"
alias nirm="nix-env -e"
alias ncln="nix-store --optimise && nix-collect-garbage"
alias niss="nix-env -qas" # use '' to search
alias ninf="nix-env -qas --description"
alias nist="nix-env -q"
alias nigl="nix-env --list-generations"
alias nigs="nix-env -G"
alias nigd="nix-env --delete-generations"
alias nirb="nix-env --rollback"
alias nirp="nix-store --verify --check-contents --repair"

# apt alias
alias apup="sudo apt update && sudo apt upgrade"
alias apin="sudo apt install"
alias apre="sudo apt install --reinstall "
# use version num for downgrade
alias aprm="sudo apt autoremove"
alias apcln="sudo apt autoremove && sudo apt autoclean"
alias apss="apt search"
alias aphis="cat /var/log/apt/history.log"
alias apinf="apt info"
alias apist="apt list --installed"

# dnf alias
alias dnup="sudo dnf upgrade --refresh" #&& sudo dnf distro-sync --refresh
alias dnin="sudo dnf install"
alias dnre="sudo dnf reinstall"
alias dndg="sudo dnf downgrade"
alias dnrm="sudo dnf autoremove"
alias dcln="sudo dnf autoremove && sudo dnf clean all"
alias dnss="dnf search"
alias dhis="dnf history"
alias dinf="dnf info"
alias dist="dnf list"

# arch\garuda alias
alias arup="sudo pacman -Sy && sudo powerpill -Su && paru -Su"
alias arin="sudo pacman -Sy --needed"
alias prin="paru -S"
alias arre="sudo pacman -Sy"
alias ardg="sudo downgrade"
alias arrm="sudo pacman -Rns"
#alias acln="sudo pacman -Scc && sudo pacman -Rns $(pacman -Qqtd)"
alias prss="paru -Ss"
alias arss="pacman -Ss"
alias ahis="expac --timefmt='%Y-%m-%d %T' '%l\t%n %v' | sort | tail -200 | nl"
alias ainf="pacman -Qi"
alias aist="pacman -Ql"
alias fixpacman="sudo rm /var/lib/pacman/db.lck"
alias upd='/usr/bin/garuda-update'
alias gitpkg='pacman -Q | grep -i "\-git" | wc -l' # List amount of -git packages
[ ! -x /usr/bin/yay ] && [ -x /usr/bin/paru ] && alias yay='paru'
# Get fastest mirrors arch\garuda alias
alias mirror="sudo reflector -f 30 -l 30 --number 10 --verbose --save /etc/pacman.d/mirrorlist"
alias mirrord="sudo reflector --latest 50 --number 20 --sort delay --save /etc/pacman.d/mirrorlist"
alias mirrors="sudo reflector --latest 50 --number 20 --sort score --save /etc/pacman.d/mirrorlist"
alias mirrora="sudo reflector --latest 50 --number 20 --sort age --save /etc/pacman.d/mirrorlist"


## Run fastfetch if session is interactive
if status --is-interactive && type -q fastfetch
     fastfetch
end


if test -f "$HOME/.local/anaconda3/etc/fish/conf.d/conda.fish"
    source "$HOME/.local/anaconda3/etc/fish/conf.d/conda.fish"
    conda deactivate
end
# enable mcfly db to easy access history with ctrl+r
#mcfly init fish | source
#set -gx MCFLY_FUZZY 2
#set -gx MCFLY_RESULTS 40
#set -gx MCFLY_INTERFACE_VIEW BOTTOM
